# Secure blockchain

## GitLab

[![Build status](https://gitlab.com/secureblockchain/blockchain/badges/master/build.svg)](https://gitlab.com/secureblockchain/blockchain/commits/master)

## Canonical source

The canonical source is [hosted on GitLab.com](https://gitlab.com/secureblockchain/blockchain).
