package com.partisiablockchain.blockchain.fee;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.serialization.StateSerializable;

/** Class holding latest storage time and the current gas balance for a contract. */
@Immutable
public final class ContractStorage implements StateSerializable {

  private final long latestStorageTime;
  private final long gas;

  public ContractStorage() {
    latestStorageTime = 0L;
    gas = 0L;
  }

  public ContractStorage(long latestStorageTime, long gas) {
    this.gas = gas;
    this.latestStorageTime = latestStorageTime;
  }

  public long getBalance() {
    return gas;
  }

  public long getLatestStorageTime() {
    return latestStorageTime;
  }

  public ContractStorage updateBalance(long diff) {
    return new ContractStorage(latestStorageTime, gas + diff);
  }
}
