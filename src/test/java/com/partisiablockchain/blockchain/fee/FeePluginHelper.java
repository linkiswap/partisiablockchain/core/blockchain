package com.partisiablockchain.blockchain.fee;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.blockchain.AccountPluginState;
import com.partisiablockchain.blockchain.ChainPluginType;
import com.partisiablockchain.blockchain.ChainState;
import com.partisiablockchain.blockchain.MutableChainState;
import com.secata.jarutil.JarBuilder;
import com.secata.stream.SafeDataOutputStream;
import java.util.Objects;

/** Test. */
public final class FeePluginHelper {

  /**
   * Get the balance for an account.
   *
   * @param state the current blockchain state
   * @param account the account to get information for
   * @return the current balance
   */
  public static long balance(ChainState state, BlockchainAddress account) {
    if (account.getType() == BlockchainAddress.Type.ACCOUNT) {
      return getStateForAccount(state, account).getBalance();
    } else {
      AccountPluginState<?, ?, ?> localAccountPluginState = state.getLocalAccountPluginState();
      ContractStorage accountState = (ContractStorage) localAccountPluginState.getContract(account);
      return Objects.requireNonNullElseGet(accountState, ContractStorage::new).getBalance();
    }
  }

  /**
   * Get the state for a contract. If the state does not exist or the supplied address type is not
   * for a contract a null value is returned.
   *
   * @param state the current blockchain state
   * @param address the address for the contract
   * @return the state for contract if it exists, null otherwise
   */
  public static ContractStorage contractStorage(ChainState state, BlockchainAddress address) {
    if (address.getType() == BlockchainAddress.Type.ACCOUNT) {
      return null;
    } else {
      AccountPluginState<?, ?, ?> accountPlugin = state.getLocalAccountPluginState();
      return (ContractStorage) accountPlugin.getContract(address);
    }
  }

  public static long accumulatedRegisteredUsage(ChainState state) {
    return getAccumulatedFees(state).getRegisteredBlockchainUsage();
  }

  public static Long serviceFees(ChainState state, BlockchainAddress target) {
    return getAccumulatedFees(state).serviceFeesDistributedTo(target);
  }

  public static Long infrastructureFeesSum(ChainState state) {
    return getAccumulatedFees(state).getInfrastructureFeesSum();
  }

  private static AccumulatedFees getAccumulatedFees(ChainState state) {
    AccountPluginState<?, ?, ?> localAccountPluginState = state.getLocalAccountPluginState();
    return (AccumulatedFees) localAccountPluginState.getContextFree();
  }

  private static FeeState getStateForAccount(ChainState state, BlockchainAddress account) {
    AccountPluginState<?, ?, ?> localAccountPluginState = state.getLocalAccountPluginState();
    FeeState accountState = (FeeState) localAccountPluginState.getAccount(account);
    return Objects.requireNonNullElseGet(accountState, FeeState::create);
  }

  public static GasAndCoinFeePluginGlobal getFeePluginGlobal(ChainState state) {
    return (GasAndCoinFeePluginGlobal) state.getGlobalPluginState(ChainPluginType.ACCOUNT);
  }

  /**
   * Create a jar containing {@link AccountHolderAccountPlugin} and related classes.
   *
   * @return byte array of jar
   */
  public static byte[] createJar() {
    return JarBuilder.buildJar(AccountHolderAccountPlugin.class);
  }

  /** Mint gas in the supplied mutable state. */
  public static void mintGas(MutableChainState mutable, BlockchainAddress account, long amount) {
    mutable.updateLocalPluginState(
        ChainPluginType.ACCOUNT,
        account,
        SafeDataOutputStream.serialize(
            s -> {
              s.writeBoolean(true);
              s.writeLong(amount);
            }),
        0);
  }

  /**
   * Get gas balance for an account by invoking local plugin.
   *
   * @param mutable state
   * @param account account address
   * @return return value
   */
  public static byte[] getBalance(
      MutableChainState mutable, BlockchainAddress account, long blockProductionTime) {
    return mutable.updateLocalPluginState(
        ChainPluginType.ACCOUNT,
        account,
        SafeDataOutputStream.serialize(s -> s.writeByte(2)),
        blockProductionTime);
  }

  /**
   * Get blockchain usage by invoking local plugin.
   *
   * @param mutable state
   * @return return value
   */
  public static byte[] getBlockchainUsage(MutableChainState mutable, long blockProductionTime) {
    return mutable.updateLocalPluginState(
        ChainPluginType.ACCOUNT,
        SafeDataOutputStream.serialize(rpc -> rpc.writeBoolean(false)),
        blockProductionTime);
  }

  /** Enable storage fees. */
  public static void enableStorageFees(
      MutableChainState mutable, BlockchainAddress account, long productionTime) {
    mutable.updateLocalPluginState(
        ChainPluginType.ACCOUNT,
        account,
        SafeDataOutputStream.serialize(
            s -> {
              s.writeBoolean(false);
              s.writeLong(productionTime);
            }),
        productionTime);
  }

  /** Set fee account. */
  public static void setFeeAccount(MutableChainState mutable, BlockchainAddress account) {
    mutable.updateGlobalPluginState(
        0,
        ChainPluginType.ACCOUNT,
        SafeDataOutputStream.serialize(
            s -> {
              s.writeBoolean(false);
              account.write(s);
            }));
  }

  /** Set zk fee account. */
  public static void setZkFeeAccount(MutableChainState mutable, BlockchainAddress account) {
    mutable.updateGlobalPluginState(
        0,
        ChainPluginType.ACCOUNT,
        SafeDataOutputStream.serialize(
            s -> {
              s.writeBoolean(true);
              account.write(s);
            }));
  }

  /**
   * Get paid fees by invoking local plugin.
   *
   * @param mutable state
   * @param gas the amount of gas
   * @param blockProductionTime production time of block
   * @return plugin invocation return value
   */
  public static byte[] getPaidFees(
      MutableChainState mutable, long gas, long blockProductionTime, BlockchainAddress account) {
    return mutable.updateLocalPluginState(
        ChainPluginType.ACCOUNT,
        account,
        SafeDataOutputStream.serialize(
            s -> {
              s.writeByte(3);
              s.writeLong(gas);
            }),
        blockProductionTime);
  }

  /**
   * Make a dummy invocation to global plugin, that returns dummyValue.
   *
   * @param mutable state
   * @param dummyValue the value to be returned
   * @return plugin invocation return value
   */
  public static byte[] getDummyValueGlobal(MutableChainState mutable, long dummyValue) {
    return mutable.updateGlobalPluginState(
        0,
        ChainPluginType.ACCOUNT,
        SafeDataOutputStream.serialize(
            s -> {
              s.writeByte(2);
              s.writeLong(dummyValue);
            }));
  }
}
