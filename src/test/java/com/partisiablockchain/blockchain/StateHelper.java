package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.blockchain.fee.FeePluginHelper;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.flooding.ObjectCreator;
import com.partisiablockchain.serialization.StateStorage;
import com.partisiablockchain.tree.AvlTree;
import com.secata.tools.coverage.FunctionUtility;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.Map;
import java.util.UUID;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

/** Test. */
public final class StateHelper {

  /** Create a new state. */
  public static ImmutableChainState createState(
      Map<BlockchainAddress, Long> accounts,
      Map<BlockchainAddress, ContractState> contracts,
      BlockchainAddress root) {
    return createMutableState(accounts, AvlTree.create(contracts), root)
        .asImmutable("chainId", null, AvlTree.create(), AvlTree.create());
  }

  /** Create a new state. */
  public static MutableChainState createMutableState(
      Map<BlockchainAddress, Long> accountsWithBalance, BlockchainAddress root) {
    return createMutableState(accountsWithBalance, AvlTree.create(), root);
  }

  /** Create a new state. */
  private static MutableChainState createMutableState(
      Map<BlockchainAddress, Long> accounts,
      AvlTree<BlockchainAddress, ContractState> contracts,
      BlockchainAddress root) {
    ChainStateCache context = new ChainStateCache(ObjectCreator.createMemoryStateStorage());
    MutableChainState mutableChainState = MutableChainState.emptyMaster(context);
    initial(mutableChainState);
    for (BlockchainAddress address : contracts.keySet()) {
      mutableChainState.setContract(address, contracts.getValue(address));
    }
    for (Map.Entry<BlockchainAddress, Long> entry : accounts.entrySet()) {
      BlockchainAddress address = entry.getKey();
      Long balance = entry.getValue();
      if (address.getType() == BlockchainAddress.Type.ACCOUNT) {
        mutableChainState.createAccount(address);
      }
      FeePluginHelper.mintGas(mutableChainState, address, balance);
    }
    FeePluginHelper.setFeeAccount(mutableChainState, root);
    FeePluginHelper.setZkFeeAccount(mutableChainState, root);
    return mutableChainState;
  }

  /** Construct an immutable chain state from a populator function. */
  static ImmutableChainState fromPopulate(Consumer<MutableChainState> populate, String chainId) {
    return fromPopulate(ObjectCreator.createMemoryStateStorage(), populate, chainId);
  }

  /** Construct an immutable chain state from a populator function. */
  static ImmutableChainState fromPopulate(
      StateStorage storage, Consumer<MutableChainState> populate, String chainId) {
    MutableChainState empty = mutableFromPopulate(storage, populate);
    return empty.asImmutable(chainId, null, AvlTree.create(), AvlTree.create());
  }

  /** Add the supplied events to the executed state. */
  public static ImmutableChainState withEvent(
      MutableChainState state, String chainId, String subChainId, Hash... events) {
    AvlTree<Hash, Hash> spawnedEvents =
        AvlTree.create(
            Arrays.stream(events).collect(Collectors.toMap(Function.identity(), Hash::create)));
    return state.asImmutable(chainId, subChainId, spawnedEvents, AvlTree.create());
  }

  private static MutableChainState mutableFromPopulate(Consumer<MutableChainState> populate) {
    return mutableFromPopulate(ObjectCreator.createMemoryStateStorage(), populate);
  }

  /** Create a new mutable chain state with the given storage and populate function. */
  public static MutableChainState mutableFromPopulate(
      StateStorage storage, Consumer<MutableChainState> populate) {
    ChainStateCache context = new ChainStateCache(storage);
    MutableChainState empty = MutableChainState.emptyMaster(context);
    populate.accept(empty);
    return empty;
  }

  public static ImmutableChainState initial() {
    return fromPopulate(StateHelper::initial, UUID.randomUUID().toString());
  }

  /** Create the initial chain state. */
  public static void initial(MutableChainState state) {
    byte[] rpc = new byte[0];
    state.setPlugin(
        FunctionUtility.noOpBiConsumer(),
        null,
        ChainPluginType.ACCOUNT,
        FeePluginHelper.createJar(),
        rpc);
    createAccount(state, BigInteger.ONE);
    BlockchainAddress feeAccount = createAccount(state, BigInteger.valueOf(123456));
    BlockchainAddress zkAccount = createAccount(state, BigInteger.valueOf(654321));
    FeePluginHelper.setFeeAccount(state, feeAccount);
    FeePluginHelper.setZkFeeAccount(state, zkAccount);
  }

  private static BlockchainAddress createAccount(MutableChainState state, BigInteger privateKey) {
    BlockchainAddress initialProducer = new KeyPair(privateKey).getPublic().createAddress();
    state.createAccount(initialProducer);
    FeePluginHelper.mintGas(state, initialProducer, 100_000);
    return initialProducer;
  }

  /**
   * Create initial chain state with some additions.
   *
   * @param additions consumer adding information to initial state
   * @return the created state
   */
  public static MutableChainState initialWithAdditions(Consumer<MutableChainState> additions) {
    return mutableFromPopulate(
        state -> {
          StateHelper.initial(state);
          additions.accept(state);
        });
  }
}
