package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.blockchain.ChainPlugin.LocalPlugin;
import com.partisiablockchain.blockchain.ChainPlugin.PluginIdentifier;
import com.partisiablockchain.blockchain.ChainPlugin.SerializedPlugin;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.flooding.ObjectCreator;
import com.partisiablockchain.serialization.LargeByteArray;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.serialization.StateSerializer;
import com.partisiablockchain.serialization.StateVoid;
import com.secata.jarutil.JarBuilder;
import com.secata.stream.SafeDataInputStream;
import java.util.List;
import java.util.function.LongSupplier;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class ChainPluginTest {

  @Test
  public void localMigration() {
    ChainStateCache chainStateCache = new ChainStateCache(ObjectCreator.createMemoryStateStorage());

    StateSerializer stateSerializer = chainStateCache.createStateSerializer();
    Hash empty = stateSerializer.write(null).hash();
    Hash jarHash = createJarHash(stateSerializer);

    PluginIdentifier first = new PluginIdentifier(null, jarHash);
    Hash firstIdentifier = stateSerializer.write(first).hash();
    PluginIdentifier second = new PluginIdentifier(firstIdentifier, jarHash);
    PluginIdentifier third = new PluginIdentifier(stateSerializer.write(second).hash(), jarHash);

    Hash secondPluginIdentifierHash = stateSerializer.write(second).hash();
    ChainPlugin<?, ?, ?> plugin =
        new SerializedPlugin(null, stateSerializer.write(second).hash(), empty)
            .asPlugin(chainStateCache.createPluginLoader(), null);
    Assertions.assertThat(plugin.getPluginIdentifierHash()).isEqualTo(secondPluginIdentifierHash);
    StateSerializable localState = plugin.getLocalState();
    Assertions.assertThat(localState).isInstanceOf(LongSupplier.class);
    Assertions.assertThat(((LongSupplier) localState).getAsLong()).isEqualTo(1);

    Hash thirdPluginIdentifierHash = stateSerializer.write(third).hash();
    ChainPlugin<?, ?, ?> nextPlugin =
        new SerializedPlugin(null, thirdPluginIdentifierHash, empty)
            .asPlugin(
                chainStateCache.createPluginLoader(),
                plugin.serializeLocal(chainStateCache.createPluginLoader()));
    Assertions.assertThat(nextPlugin.getPluginIdentifierHash())
        .isEqualTo(thirdPluginIdentifierHash);
    StateSerializable nextState = nextPlugin.getLocalState();
    Assertions.assertThat(nextState).isInstanceOf(LongSupplier.class);
    Assertions.assertThat(((LongSupplier) nextState).getAsLong()).isEqualTo(2);
  }

  @Test
  public void localMigrationWithoutNull() {
    ChainStateCache chainStateCache = new ChainStateCache(ObjectCreator.createMemoryStateStorage());
    StateSerializer stateSerializer = chainStateCache.createStateSerializer();
    Hash jarHash = createJarHash(stateSerializer);
    Hash empty = stateSerializer.write(null).hash();

    PluginIdentifier first = new PluginIdentifier(null, jarHash);
    Hash firstIdentifier = stateSerializer.write(first).hash();
    PluginIdentifier second = new PluginIdentifier(firstIdentifier, jarHash);
    Hash secondIdentifier = stateSerializer.write(second).hash();
    PluginIdentifier third = new PluginIdentifier(secondIdentifier, jarHash);
    PluginIdentifier fourth = new PluginIdentifier(stateSerializer.write(third).hash(), jarHash);
    Hash fourthId = stateSerializer.write(fourth).hash();

    ChainPlugin<?, ?, ?> plugin =
        createChainPlugin(chainStateCache, firstIdentifier, first, empty, fourthId);

    StateSerializable localState = plugin.getLocalState();
    Assertions.assertThat(localState).isInstanceOf(LongSupplier.class);
    Assertions.assertThat(((LongSupplier) localState).getAsLong()).isEqualTo(3);
  }

  @Test
  public void localMigrationWithNull() {
    ChainStateCache chainStateCache = new ChainStateCache(ObjectCreator.createMemoryStateStorage());
    StateSerializer stateSerializer = chainStateCache.createStateSerializer();
    Hash empty = stateSerializer.write(null).hash();
    Hash jarHash = createJarHash(stateSerializer);
    Hash noMigrateJar =
        stateSerializer
            .write(
                new LargeByteArray(
                    JarBuilder.buildJar(
                        MigrationPluginDisallowMigrate.class,
                        AbstractMigrationPlugin.class,
                        StateCounter.class)))
            .hash();

    PluginIdentifier first = new PluginIdentifier(null, jarHash);
    Hash firstIdentifier = stateSerializer.write(first).hash();
    PluginIdentifier second = new PluginIdentifier(firstIdentifier, noMigrateJar);
    stateSerializer.write(second);
    PluginIdentifier third = new PluginIdentifier(null, noMigrateJar);
    PluginIdentifier fourth = new PluginIdentifier(stateSerializer.write(third).hash(), jarHash);
    Hash fourthId = stateSerializer.write(fourth).hash();

    ChainPlugin<?, ?, ?> plugin =
        createChainPlugin(chainStateCache, firstIdentifier, first, empty, fourthId);
    StateSerializable localState = plugin.getLocalState();
    Assertions.assertThat(localState).isInstanceOf(LongSupplier.class);
    Assertions.assertThat(((LongSupplier) localState).getAsLong()).isEqualTo(1);
  }

  private Hash createJarHash(StateSerializer stateSerializer) {
    return stateSerializer
        .write(
            new LargeByteArray(
                JarBuilder.buildJar(
                    MigrationPlugin.class, AbstractMigrationPlugin.class, StateCounter.class)))
        .hash();
  }

  private ChainPlugin<?, ?, ?> createChainPlugin(
      ChainStateCache chainStateCache,
      Hash firstIdentifier,
      PluginIdentifier first,
      Hash empty,
      Hash fourthId) {

    StateSerializable migratedToFirst =
        ((ChainPlugin<?, ?, ?>)
                new SerializedPlugin(null, firstIdentifier, empty)
                    .asPlugin(chainStateCache.createPluginLoader(), null))
            .getLocalState();

    return new SerializedPlugin(null, fourthId, empty)
        .asPlugin(
            chainStateCache.createPluginLoader(),
            new LocalPlugin(
                chainStateCache.getPlugin(first.getJarHash()), firstIdentifier, migratedToFirst));
  }

  /** Test. */
  @Immutable
  public static final class MigrationPlugin extends AbstractMigrationPlugin {

    @Override
    public StateCounter migrateLocal(StateAccessor currentLocal) {
      if (currentLocal != null) {
        return new StateCounter(currentLocal.get("counter").longValue() + 1);
      } else {
        return new StateCounter();
      }
    }
  }

  /** Test. */
  @Immutable
  public static final class MigrationPluginDisallowMigrate extends AbstractMigrationPlugin {

    @Override
    public InvokeResult<StateVoid> invokeGlobal(
        PluginContext pluginContext, StateVoid state, byte[] rpc) {
      return null;
    }

    @Override
    public StateCounter migrateLocal(StateAccessor currentLocal) {
      if (currentLocal != null) {
        throw new UnsupportedOperationException();
      }
      return new StateCounter();
    }
  }

  @Immutable
  abstract static class AbstractMigrationPlugin
      implements BlockchainPlugin<StateVoid, StateCounter> {

    @Override
    public Class<StateVoid> getGlobalStateClass() {
      return StateVoid.class;
    }

    @Override
    public Class<StateCounter> getLocalStateClass() {
      return StateCounter.class;
    }

    @Override
    public List<Class<?>> getLocalStateClassTypeParameters() {
      return List.of();
    }

    @Override
    public InvokeResult<StateCounter> invokeLocal(
        PluginContext pluginContext,
        StateVoid globalState,
        StateCounter state,
        BlockchainAddress invocationContext,
        byte[] rpc) {
      throw new UnsupportedOperationException();
    }

    @Override
    public InvokeResult<StateVoid> invokeGlobal(
        PluginContext pluginContext, StateVoid state, byte[] rpc) {
      throw new UnsupportedOperationException();
    }

    @Override
    public StateVoid migrateGlobal(StateAccessor currentGlobal, SafeDataInputStream rpc) {
      return null;
    }
  }

  /** Test. */
  @Immutable
  public static final class StateCounter implements StateSerializable, LongSupplier {

    private final long counter;

    public StateCounter() {
      counter = 0;
    }

    public StateCounter(long counter) {
      this.counter = counter;
    }

    @Override
    public long getAsLong() {
      return counter;
    }
  }
}
