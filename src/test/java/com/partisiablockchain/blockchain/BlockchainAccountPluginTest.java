package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.TestObjects;
import com.partisiablockchain.blockchain.fee.AccumulatedFees;
import com.partisiablockchain.blockchain.fee.ContractStorage;
import com.partisiablockchain.blockchain.fee.FeeState;
import com.partisiablockchain.blockchain.fee.GasAndCoinFeePluginGlobal;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateLong;
import com.partisiablockchain.tree.AvlTree;
import java.util.List;
import java.util.Map;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class BlockchainAccountPluginTest {

  @Test
  public void migrate() {
    AccountPluginState<AccumulatedFees, FeeState, StateLong> old =
        new AccountPluginState<>(
            AccumulatedFees.create().addFees(89),
            AvlTree.create(
                Map.of(
                    TestObjects.ACCOUNT_ONE, FeeState.create().updatedBalance(50),
                    TestObjects.ACCOUNT_TWO, FeeState.create().updatedBalance(0),
                    TestObjects.ACCOUNT_THREE, FeeState.create().updatedBalance(27))),
            AvlTree.create(
                Map.of(
                    TestObjects.CONTRACT_PUB1, new StateLong(704),
                    TestObjects.CONTRACT_PUB2, new StateLong(0),
                    TestObjects.CONTRACT_SYS, new StateLong(999))));

    TestMigrate migratingPlugin = new TestMigrate();

    AccountPluginState<AccumulatedFees, FeeState, ContractStorage> migrated =
        migratingPlugin.migrateLocal(StateAccessor.create(old));
    Assertions.assertThat(migrated.getContextFree().fees()).isEqualTo(89);
    Assertions.assertThat(migrated.getAccounts().keySet())
        .containsExactlyInAnyOrder(TestObjects.ACCOUNT_ONE, TestObjects.ACCOUNT_THREE);
    Assertions.assertThat(migrated.getContracts().keySet())
        .containsExactlyInAnyOrder(TestObjects.CONTRACT_PUB1, TestObjects.CONTRACT_SYS);
  }

  /** Migrate Test. */
  public static final class TestMigrate extends NullBlockchainAccountPlugin {

    @Override
    public List<Class<?>> getLocalStateClassTypeParameters() {
      return List.of(AccumulatedFees.class, FeeState.class, ContractStorage.class);
    }

    @Override
    public Class<GasAndCoinFeePluginGlobal> getGlobalStateClass() {
      return GasAndCoinFeePluginGlobal.class;
    }

    @Override
    protected AccumulatedFees migrateContextFree(StateAccessor current) {
      return current == null ? AccumulatedFees.create() : current.typedValue(AccumulatedFees.class);
    }

    @Override
    protected ContractStorage migrateContract(BlockchainAddress address, StateAccessor current) {
      StateLong current1 = current.typedValue(StateLong.class);
      if (current1.value() == 0) {
        return null;
      } else {
        return new ContractStorage(current1.value(), 0);
      }
    }

    @Override
    protected FeeState migrateAccount(StateAccessor current) {
      FeeState current1 = current.typedValue(FeeState.class);
      if (current1.getBalance() == 0) {
        return null;
      } else {
        return current1;
      }
    }
  }
}
