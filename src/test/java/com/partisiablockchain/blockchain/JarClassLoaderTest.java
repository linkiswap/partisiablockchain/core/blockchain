package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.binder.PublicContractBinder;
import com.partisiablockchain.binder.SysContractBinder;
import com.secata.jarutil.JarBuilder;
import com.secata.tools.coverage.ExceptionConverter;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.jar.JarOutputStream;
import java.util.zip.ZipEntry;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class JarClassLoaderTest {

  @Test
  public void unableToLoadWithoutMainFile() throws Exception {
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    JarOutputStream jarOutputStream = new JarOutputStream(bytes);
    jarOutputStream.putNextEntry(new ZipEntry("not_main"));
    jarOutputStream.putNextEntry(new ZipEntry("totally_not_main"));
    jarOutputStream.closeEntry();
    jarOutputStream.close();

    Assertions.assertThatThrownBy(() -> JarClassLoader.builder(bytes.toByteArray()).build())
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessageContaining(
            "The jar file should contain a \"main\" resource to specify the main class")
        .hasMessageContaining("totally_not_main")
        .hasMessageContaining("not_main");
  }

  @Test
  public void unableToLoadWithoutMainClass() throws Exception {
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    JarOutputStream jarOutputStream = new JarOutputStream(bytes);
    jarOutputStream.putNextEntry(new ZipEntry("main"));
    jarOutputStream.write("com.partisiablockchain.Missing".getBytes(StandardCharsets.UTF_8));
    jarOutputStream.closeEntry();
    jarOutputStream.close();

    Assertions.assertThatThrownBy(() -> JarClassLoader.builder(bytes.toByteArray()).build())
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessageContaining(
            "The jar file should contain the default main class:"
                + " com.partisiablockchain.Missing")
        .hasMessageContaining("Found: [main]");
  }

  @Test
  public void loadingMultipleTimesShouldReturnSameInstance() throws Exception {
    JarClassLoader loader = JarClassLoader.builder(getPublicBinderJar()).build();
    Class<?> loaded = loader.getMainClass();
    String handlerClassName = loaded.getCanonicalName();
    Assertions.assertThat(loader.loadClass(handlerClassName)).isSameAs(loaded);
    InputStream main = loader.getResourceAsStream("main");
    Assertions.assertThat(main).hasContent(loaded.getName());
  }

  @Test
  public void loadingResources() {
    JarClassLoader loader = JarClassLoader.builder(getPublicBinderJar()).build();
    Class<?> loaded = loader.getMainClass();
    Assertions.assertThat(loader.getResourceAsStream("main")).hasContent(loaded.getName());
    Assertions.assertThat(loader.getResourceAsStream("hellobananan")).isNull();
  }

  @Test
  public void loadJavaLangString() {
    JarClassLoader loader = JarClassLoader.builder(getPublicBinderJar()).build();
    Class<?> string = ExceptionConverter.call(() -> loader.loadClass("java.lang.String"), "Error");

    Assertions.assertThat(string).isSameAs(String.class);
  }

  @Test
  public void classesWithingProvidedNameSpacesAreLoadedByDefaultLoader() {
    JarClassLoader loader =
        JarClassLoader.builder(testJar())
            .defaultClassLoader(JarClassLoaderTest.class.getClassLoader())
            .addSystemClassLoadingNamespace("com.partisiablockchain.blockchain")
            .build();

    Assertions.assertThat(loader.getSystemLoadedNamespaces())
        .containsExactly("com.partisiablockchain.blockchain");
    Assertions.assertThat(loader.getMainClass()).isEqualTo(TestLoadingClass.class);
  }

  @Test
  public void whenDefaultClassLoaderThrowsFallbackClassloadingIsUsed() {
    JarClassLoader loader =
        JarClassLoader.builder(testJar())
            .defaultClassLoader(new DummyClassLoader())
            .addSystemClassLoadingNamespace("com.partisiablockchain.blockchain")
            .build();

    Assertions.assertThat(loader.getSystemLoadedNamespaces())
        .containsExactly("com.partisiablockchain.blockchain");
    Class<?> mainClass = loader.getMainClass();
    Assertions.assertThat(mainClass).isNotEqualTo(TestLoadingClass.class);
    Assertions.assertThat(mainClass.getName()).isEqualTo(TestLoadingClass.class.getName());
  }

  public static byte[] getPublicBinderJar() {
    return JarBuilder.buildJar(PublicContractBinder.class);
  }

  public static byte[] getSysBinderJar() {
    return JarBuilder.buildJar(SysContractBinder.class);
  }

  private static byte[] testJar() {
    return JarBuilder.buildJar(TestLoadingClass.class, JarClassLoaderTest.class);
  }

  /** Test. */
  public static final class TestLoadingClass {}

  /** Test. */
  public static final class DummyClassLoader extends ClassLoader {

    @Override
    public Class<?> loadClass(String name) throws ClassNotFoundException {
      if (TestLoadingClass.class.getName().equals(name)) {
        throw new ClassNotFoundException();
      } else {
        return JarClassLoader.class.getClassLoader().loadClass(name);
      }
    }
  }
}
