package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.CloseableTest;
import com.partisiablockchain.DummyExecutableEventFinder;
import com.partisiablockchain.TestObjects;
import com.partisiablockchain.blockchain.BlockchainLedger.BlockAndState;
import com.partisiablockchain.blockchain.account.AccountState;
import com.partisiablockchain.blockchain.contract.CoreContractState;
import com.partisiablockchain.blockchain.contract.CoreContractStateTest;
import com.partisiablockchain.blockchain.contract.InteractWithContractTransaction;
import com.partisiablockchain.blockchain.contract.StateComplex;
import com.partisiablockchain.blockchain.contract.TestContractByInvocation;
import com.partisiablockchain.blockchain.contract.ZkTestContractComplex;
import com.partisiablockchain.blockchain.contract.binder.ZkComplexTestBinder;
import com.partisiablockchain.blockchain.fee.ContractStorage;
import com.partisiablockchain.blockchain.fee.FeePluginHelper;
import com.partisiablockchain.blockchain.fee.FeeState;
import com.partisiablockchain.blockchain.genesis.GenesisStorage;
import com.partisiablockchain.blockchain.storage.StateStorageRaw;
import com.partisiablockchain.blockchain.transaction.EventTransaction;
import com.partisiablockchain.blockchain.transaction.ExecutableEvent;
import com.partisiablockchain.blockchain.transaction.FloodableEvent;
import com.partisiablockchain.blockchain.transaction.InnerSystemEvent;
import com.partisiablockchain.blockchain.transaction.MemoryStateStorage;
import com.partisiablockchain.blockchain.transaction.ReturnEnvelope;
import com.partisiablockchain.blockchain.transaction.SyncEvent;
import com.partisiablockchain.contract.pub.PubContract;
import com.partisiablockchain.contract.sys.GlobalPluginStateUpdate;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.flooding.ObjectCreator;
import com.partisiablockchain.serialization.LargeByteArray;
import com.partisiablockchain.serialization.SerializationResult;
import com.partisiablockchain.serialization.StateLong;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.serialization.StateSerializer;
import com.partisiablockchain.serialization.StateVoid;
import com.partisiablockchain.storage.BlockchainStorage;
import com.partisiablockchain.storage.BlockchainStorageTest;
import com.partisiablockchain.storage.MemoryStorage;
import com.partisiablockchain.storage.RootDirectory;
import com.partisiablockchain.tree.AvlTree;
import com.secata.jarutil.JarBuilder;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.coverage.ExceptionConverter;
import com.secata.tools.coverage.FunctionUtility;
import java.io.File;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import java.util.stream.Stream;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class BlockchainLedgerShardTest extends CloseableTest {

  private final KeyPair signer = new KeyPair(BigInteger.ONE);
  private final Map<MemoryStorage.PathAndHash, byte[]> storage = new HashMap<>();

  private String setUpGenesis(
      String chainId, String subChain, Consumer<MutableChainState> stateBuilder) {
    File genesis = ExceptionConverter.call(() -> File.createTempFile("genesis", ".zip"), "");

    GenesisStorage storage = new GenesisStorage();
    ChainStateCache context = new ChainStateCache(storage);

    MutableChainState genesisState = MutableChainState.emptyMaster(context);
    stateBuilder.accept(genesisState);
    Hash stateHash =
        genesisState
            .asImmutable(chainId, subChain, AvlTree.create(), AvlTree.create())
            .saveExecutedState(
                s -> new StateSerializer(storage, true).write(s, ExecutedState.class).hash());

    storage.persistGenesis(
        genesis, new FinalBlock(Block.createShardGenesis(2L, stateHash), new byte[0]));
    return genesis.getAbsolutePath();
  }

  @Test
  public void shardedChain() {
    final byte[] consensusJar =
        JarBuilder.buildJar(BlockchainLedgerConsensusTest.InnerConsensus.class);
    final byte[] routingJar = JarBuilder.buildJar(BlockchainLedgerRoutingTest.InnerRouting.class);

    KeyPair nextProducer = new KeyPair();
    RootDirectory rootDirectory =
        MemoryStorage.createRootDirectory(temporaryFolder.toFile(), storage);
    Consumer<MutableChainState> stateBuilder =
        mutable -> {
          mutable.addActiveShard(FunctionUtility.noOpBiConsumer(), "Shard0", TestObjects.SHARD_ONE);
          mutable.setPlugin(
              FunctionUtility.noOpBiConsumer(),
              null,
              ChainPluginType.CONSENSUS,
              consensusJar,
              SafeDataOutputStream.serialize(nextProducer.getPublic().createAddress()));
          mutable.setPlugin(
              FunctionUtility.noOpBiConsumer(),
              null,
              ChainPluginType.ROUTING,
              routingJar,
              SafeDataOutputStream.serialize(FunctionUtility.noOpConsumer()));
        };

    BlockchainLedger shard =
        register(
            BlockchainLedger.createBlockChain(
                ObjectCreator.networkConfig(),
                rootDirectory,
                setUpGenesis("ChainId", TestObjects.SHARD_ONE, stateBuilder),
                new DummyExecutableEventFinder()));

    BlockAndState latest = shard.latest();
    Block latestBlock = latest.getBlock();
    Assertions.assertThat(latestBlock.getCommitteeId()).isEqualTo(2L);
    Assertions.assertThat(latest.getState().getPluginJar(ChainPluginType.CONSENSUS).getData())
        .isEqualTo(consensusJar);
    Assertions.assertThat(latest.getState().getPluginJar(ChainPluginType.ROUTING).getData())
        .isEqualTo(routingJar);

    FinalBlock finalBlock = createBlock(nextProducer, latest, 2L);
    shard.appendBlock(finalBlock);
    Assertions.assertThat(shard.getLatestBlock()).isEqualTo(finalBlock.getBlock());

    shard.appendBlock(createBlock(nextProducer, shard.latest(), 3L));
  }

  @Test
  public void shouldBeAbleToInitializeFromStorage() {
    BlockchainLedger shard =
        register(
            BlockchainLedger.createBlockChain(
                ObjectCreator.networkConfig(),
                MemoryStorage.createRootDirectory(temporaryFolder.toFile(), storage),
                setUpGenesis("ChainId", TestObjects.SHARD_ONE, singleShardState()),
                new DummyExecutableEventFinder()));

    shard.appendBlock(createBlock(new KeyPair(BigInteger.ONE), shard.latest(), 2L));

    Assertions.assertThat(shard.getBlockTime()).isEqualTo(1L);
    shard.close();

    BlockchainLedger restarted =
        register(
            BlockchainLedger.createBlockChain(
                ObjectCreator.networkConfig(),
                MemoryStorage.createRootDirectory(temporaryFolder.toFile(), storage),
                setUpGenesis("ChainId", TestObjects.SHARD_ONE, singleShardState()),
                new DummyExecutableEventFinder()));
    Assertions.assertThat(restarted.getBlockTime()).isEqualTo(1L);
  }

  private Consumer<MutableChainState> singleShardState() {
    return getStateBuilder();
  }

  @Test
  public void eventInWrongOrderResultsInFaultyBlock() {
    KeyPair initialProducer = new KeyPair(BigInteger.ONE);
    ExecutableEvent executableEvent = createEvent(TestObjects.SHARD_ONE, 2L);

    BlockchainLedger shard =
        BlockchainLedger.createBlockChain(
            ObjectCreator.networkConfig(),
            MemoryStorage.createRootDirectory(temporaryFolder.toFile(), storage),
            setUpGenesis("ChainId", TestObjects.SHARD_ONE, singleShardState()),
            new DummyExecutableEventFinder());
    addPendingEvent(shard, initialProducer, true, executableEvent);

    Assertions.assertThatThrownBy(
            () ->
                shard.appendBlock(
                    createBlock(
                        initialProducer,
                        shard.latest(),
                        2L,
                        List.of(executableEvent.identifier()))))
        .isInstanceOf(RuntimeException.class)
        .hasMessageContaining("Invalid block received Block{blockTime=1,");
    shard.close();
  }

  @Test
  public void addPendingEventCompressed() {
    ExecutableEvent event =
        new ExecutableEvent(
            null,
            new EventTransaction(
                TestObjects.EMPTY_HASH,
                new ShardRoute(TestObjects.SHARD_ONE, 1L),
                2,
                1L,
                0,
                null,
                new SyncEvent(List.of(), List.of(), List.of())));

    DummyExecutableEventFinder finder = new DummyExecutableEventFinder();
    BlockchainLedger shard =
        register(
            BlockchainLedger.createBlockChain(
                ObjectCreator.networkConfig(),
                MemoryStorage.createRootDirectory(temporaryFolder.toFile(), storage),
                setUpGenesis("ChainId", TestObjects.SHARD_ONE, singleShardState()),
                finder));
    StateStorageRaw storage = ObjectCreator.createMemoryStateStorage();
    ImmutableChainState state =
        StateHelper.withEvent(
            MutableChainState.emptyMaster(new ChainStateCache(storage)),
            shard.getChainId(),
            null,
            event.identifier());
    state.saveExecutedState(
        s -> new StateSerializer(storage, true).write(s, ExecutedState.class).hash());
    Block block = createBlock(state);
    FinalBlock finalBlock =
        new FinalBlock(
            block,
            SafeDataOutputStream.serialize(new KeyPair(BigInteger.ONE).sign(block.identifier())));
    finder.setEvent(event);
    Assertions.assertThat(
            shard.addPendingEvent(FloodableEvent.create(event, finalBlock, storage).compress()))
        .isTrue();
  }

  @Test
  public void syncEvents() {
    CoreContractState.ContractSerialization<StateLong> contractSerialization =
        new CoreContractState.ContractSerialization<>(StateLong.class);
    testSyncEvents(
        JarClassLoaderTest.getPublicBinderJar(),
        JarBuilder.buildJar(MyContract.class),
        JarBuilder.buildJar(MyOtherContract.class),
        contractSerialization,
        new StateLong(2L),
        StateLong.class,
        TestObjects.CONTRACT_PUB1);
  }

  @Test
  @SuppressWarnings("rawtypes")
  public void syncEventsWithZeroKnowledge() {
    CoreContractState.ContractSerialization<StateComplex> contractSerialization =
        new CoreContractState.ContractSerialization<>(
            StateComplex.class, Integer.class, String.class, Long.class);
    testSyncEvents(
        JarBuilder.buildJar(ZkComplexTestBinder.class),
        JarBuilder.buildJar(ZkTestContractComplex.class),
        JarBuilder.buildJar(MyOtherContract.class),
        contractSerialization,
        new StateComplex<>(3, "lol", 2L),
        StateComplex.class,
        TestObjects.CONTRACT_ZK1);
  }

  @SuppressWarnings({"rawtypes", "unchecked"})
  private void testSyncEvents(
      byte[] binderJar,
      byte[] contractJar,
      byte[] abiJar,
      CoreContractState.ContractSerialization contractSerialization,
      StateSerializable innerStateSerialized,
      Class stateClass,
      BlockchainAddress contract) {
    MemoryStateStorage memoryStateStorage = new MemoryStateStorage();
    StateSerializer stateSerializerFreshStorage =
        new StateSerializer(memoryStateStorage, true, true);

    Hash binderHash = writeJar(stateSerializerFreshStorage, binderJar);
    Hash contractHash = writeJar(stateSerializerFreshStorage, contractJar);
    Hash abiHash = writeJar(stateSerializerFreshStorage, abiJar);

    SerializationResult innerStateResult =
        contractSerialization.write(stateSerializerFreshStorage, innerStateSerialized);
    ContractState contractState =
        createContractState(binderHash, contractHash, abiHash, innerStateResult);
    Hash accountStateHash = createAccountState(stateSerializerFreshStorage);
    Hash testAccountPluginHash = createAccountPlugin(stateSerializerFreshStorage);

    List<SyncEvent.AccountTransfer> accountTransfers =
        createAccountTransfers(
            List.of(TestObjects.ACCOUNT_ONE), accountStateHash, testAccountPluginHash);
    List<SyncEvent.ContractTransfer> contractTransfers =
        createContractTransfers(stateSerializerFreshStorage, contractState, contract);
    ExecutableEvent executableEvent =
        createExecutableEvent(
            accountTransfers,
            contractTransfers,
            memoryStateStorage.getData().values().stream().toList());

    String genesisFile = createGenesisFile();

    BlockchainLedger shard =
        register(
            BlockchainLedger.createBlockChain(
                ObjectCreator.networkConfig(),
                MemoryStorage.createRootDirectory(temporaryFolder.toFile(), storage),
                genesisFile,
                new DummyExecutableEventFinder()));
    KeyPair initialProducer = new KeyPair(BigInteger.ONE);
    addPendingEvent(shard, initialProducer, true, executableEvent);

    shard.appendBlock(
        createBlock(initialProducer, shard.latest(), 2L, List.of(executableEvent.identifier())));

    ImmutableChainState state = shard.latest().getState();
    BlockchainAddress resultContractAddress = state.getContracts().iterator().next();
    ContractState resultContractState = state.getContract(resultContractAddress);
    Assertions.assertThat(resultContractState).usingRecursiveComparison().isEqualTo(contractState);
    Hash resultContractIdentifier = resultContractState.getCore().getContractIdentifier();
    Assertions.assertThat(resultContractIdentifier).isEqualTo(contractHash);

    StateStorageRaw stateStorage = shard.getStateStorage();
    StateSerializer stateSerializer = new StateSerializer(stateStorage, true);
    LargeByteArray resultContractJar = stateSerializer.read(contractHash, LargeByteArray.class);
    Assertions.assertThat(resultContractJar.getData()).isEqualTo(contractJar);

    if (stateClass.equals(StateLong.class)) {
      StateLong innerState = stateSerializer.read(innerStateResult.hash(), StateLong.class);
      Assertions.assertThat(innerState.value()).isEqualTo(2L);
    } else {
      StateComplex<Integer, String, Long> innerState =
          stateSerializer.read(
              innerStateResult.hash(), StateComplex.class, Integer.class, String.class, Long.class);
      Assertions.assertThat(innerState.getAlphaT()).isEqualTo(3);
      Assertions.assertThat(innerState.getBravoT()).isEqualTo("lol");
      Assertions.assertThat(innerState.getCharlieT()).isEqualTo(2L);
    }

    BlockchainAddress resultAccountAddress = state.getAccounts().iterator().next();
    AccountPluginState<?, ?, ?> localAccountPluginState = state.getLocalAccountPluginState();
    ContractStorage resultContractStorage =
        (ContractStorage) localAccountPluginState.getContract(resultContractAddress);
    Assertions.assertThat(resultContractStorage.getLatestStorageTime()).isEqualTo(2);
    FeeState resultFeeState =
        (FeeState) localAccountPluginState.getAccountState(resultAccountAddress).account;
    Assertions.assertThat(resultFeeState.getDebt()).isEqualTo(10);

    Assertions.assertThat(state.getAccount(TestObjects.ACCOUNT_ONE)).isNotNull();
    Assertions.assertThat(state.lookupNonce(TestObjects.ACCOUNT_ONE)).isEqualTo(2L);
  }

  @Test
  public void shouldPatchLargeSyncEventFromStorage() {

    MemoryStateStorage memoryStateStorage = new MemoryStateStorage();
    StateSerializer stateSerializerFreshStorage =
        new StateSerializer(memoryStateStorage, true, true);

    CoreContractState.ContractSerialization<StateLong> contractSerialization =
        new CoreContractState.ContractSerialization<>(StateLong.class);

    SerializationResult innerStateResult =
        contractSerialization.write(stateSerializerFreshStorage, new StateLong(2L));
    ContractState contractState =
        createContractState(stateSerializerFreshStorage, innerStateResult);
    Hash accountStateHash = createAccountState(stateSerializerFreshStorage);
    Hash testAccountPluginHash = createAccountPlugin(stateSerializerFreshStorage);

    List<SyncEvent.AccountTransfer> accountTransfers =
        createAccountTransfers(getLargeData(), accountStateHash, testAccountPluginHash);
    List<SyncEvent.ContractTransfer> contractTransfers =
        createContractTransfers(
            stateSerializerFreshStorage, contractState, TestObjects.CONTRACT_PUB1);

    ExecutableEvent executableEvent =
        createExecutableEvent(
            accountTransfers,
            contractTransfers,
            memoryStateStorage.getData().values().stream().toList());

    RootDirectory rootDirectory =
        MemoryStorage.createRootDirectory(temporaryFolder.toFile(), storage);

    String genesisFile = createGenesisFile();

    BlockchainLedger shard =
        register(
            BlockchainLedger.createBlockChain(
                ObjectCreator.networkConfig(),
                rootDirectory,
                genesisFile,
                new DummyExecutableEventFinder()));
    KeyPair initialProducer = new KeyPair(BigInteger.ONE);
    addPendingEvent(shard, initialProducer, true, executableEvent);

    FinalBlock finalBlock =
        createBlock(initialProducer, shard.latest(), 2L, List.of(executableEvent.identifier()));
    shard.appendBlock(finalBlock);

    // Close blockchain and clear memory, such that we read all blocks when we start up again.
    shard.close();
    BlockchainStorageTest.clearStates(rootDirectory, storage);

    BlockchainLedger blockchainLedger =
        register(
            BlockchainLedger.createBlockChain(
                ObjectCreator.networkConfig(), rootDirectory, genesisFile, signer));
    Block expectedBlock = finalBlock.getBlock();
    Block actualBlock = blockchainLedger.getLatestBlock();
    Assertions.assertThat(expectedBlock).isEqualTo(actualBlock);
  }

  private Hash createAccountPlugin(StateSerializer stateSerializer) {
    FeeState feeState = FeeState.create();
    feeState = feeState.setDebt(10);
    return stateSerializer.write(feeState).hash();
  }

  private List<SyncEvent.AccountTransfer> createAccountTransfers(
      List<BlockchainAddress> addresses, Hash accountStateHash, Hash testAccountPluginHash) {
    List<SyncEvent.AccountTransfer> accountTransfers = new ArrayList<>();
    for (BlockchainAddress address : addresses) {
      accountTransfers.add(
          new SyncEvent.AccountTransfer(address, accountStateHash, testAccountPluginHash));
    }
    return accountTransfers;
  }

  private List<SyncEvent.ContractTransfer> createContractTransfers(
      StateSerializer stateSerializer, ContractState contractState, BlockchainAddress contract) {
    Hash contractStateHash = stateSerializer.write(contractState).hash();
    Hash testContractPluginHash = stateSerializer.write(new ContractStorage(2, 3)).hash();
    return List.of(
        new SyncEvent.ContractTransfer(contract, contractStateHash, testContractPluginHash));
  }

  private ContractState createContractState(
      Hash binderHash, Hash contractHash, Hash abiHash, SerializationResult innerStateResult) {
    CoreContractState core = CoreContractState.create(binderHash, contractHash, abiHash, 1234);
    ContractState contractState = ContractState.create(core);
    return contractState.withHashAndSize(
        innerStateResult.hash(), innerStateResult.totalByteCount());
  }

  private ContractState createContractState(
      StateSerializer stateSerializer, SerializationResult innerStateResult) {
    Hash binderHash = writeJar(stateSerializer, JarClassLoaderTest.getPublicBinderJar());
    Hash contractHash = writeJar(stateSerializer, JarBuilder.buildJar(MyContract.class));
    Hash abiHash = writeJar(stateSerializer, JarBuilder.buildJar(MyOtherContract.class));

    CoreContractState core = CoreContractState.create(binderHash, contractHash, abiHash, 1234);
    ContractState contractState = ContractState.create(core);
    return contractState.withHashAndSize(
        innerStateResult.hash(), innerStateResult.totalByteCount());
  }

  private Hash writeJar(StateSerializer stateSerializer, byte[] jar) {
    return stateSerializer.write(new LargeByteArray(jar), LargeByteArray.class).hash();
  }

  private Hash createAccountState(StateSerializer stateSerializer) {
    long accountNonce = 2L;
    AccountState accountState = AccountState.create(accountNonce);
    return stateSerializer.write(accountState).hash();
  }

  private ExecutableEvent createExecutableEvent(
      List<SyncEvent.AccountTransfer> accountTransfers,
      List<SyncEvent.ContractTransfer> contractTransfers,
      List<byte[]> memoryStateStorages) {

    SyncEvent syncEvent = new SyncEvent(accountTransfers, contractTransfers, memoryStateStorages);

    EventTransaction eventTransaction =
        new EventTransaction(
            TestObjects.EMPTY_HASH,
            new ShardRoute(TestObjects.SHARD_ONE, 1L),
            2,
            1L,
            0,
            null,
            syncEvent);

    return new ExecutableEvent(null, eventTransaction);
  }

  private String createGenesisFile() {
    return setUpGenesis(
        "ChainId",
        TestObjects.SHARD_ONE,
        m -> {
          singleShardState().accept(m);
          m.setPlugin(
              FunctionUtility.noOpBiConsumer(),
              null,
              ChainPluginType.ACCOUNT,
              FeePluginHelper.createJar(),
              SafeDataOutputStream.serialize(FunctionUtility.noOpConsumer()));
        });
  }

  private List<BlockchainAddress> getLargeData() {
    List<BlockchainAddress> addresses = new ArrayList<>();
    for (int i = 0; i < 1001; i++) {
      addresses.add(createAddress(i));
    }
    return addresses;
  }

  private BlockchainAddress createAddress(int i) {
    return BlockchainAddress.fromHash(
        BlockchainAddress.Type.CONTRACT_GOV, Hash.create(s -> s.writeInt(i)));
  }

  @Test
  public void validateFinalizedBlockOnSameShard() {
    KeyPair producer = new KeyPair(BigInteger.ONE);
    RootDirectory rootDirectory =
        MemoryStorage.createRootDirectory(temporaryFolder.toFile(), storage);
    DummyExecutableEventFinder finder = new DummyExecutableEventFinder();
    final BlockchainLedger blockchain =
        register(
            BlockchainLedger.createForTest(
                rootDirectory,
                mutable ->
                    mutable.addActiveShard(
                        FunctionUtility.noOpBiConsumer(), null, TestObjects.SHARD_TWO),
                new BlockChainTestNetwork(),
                finder));

    ExecutableEvent createShardEvent =
        new ExecutableEvent(
            TestObjects.SHARD_TWO,
            new EventTransaction(
                TestObjects.EMPTY_HASH,
                new ShardRoute(null, 1),
                2L,
                0L,
                0,
                null,
                new InnerSystemEvent.RemoveContract(TestObjects.CONTRACT_PUB1)));
    RootConsensusPlugin plugin = new RootConsensusPlugin();
    StateStorageRaw storage = ObjectCreator.createMemoryStateStorage();
    ImmutableChainState state =
        StateHelper.withEvent(
            MutableChainState.emptyMaster(new ChainStateCache(storage)),
            "ChainId",
            TestObjects.SHARD_TWO,
            Arrays.stream(new ExecutableEvent[] {createShardEvent})
                .map(ExecutableEvent::identifier)
                .toArray(Hash[]::new));
    state.saveExecutedState(
        s -> new StateSerializer(storage, true).write(s, ExecutedState.class).hash());
    Block block = createBlock(state);
    byte[] source = SafeDataOutputStream.serialize(producer.sign(block.identifier()));
    byte[] invalidFinalDataLength = new byte[source.length + 1];
    System.arraycopy(source, 0, invalidFinalDataLength, 0, source.length);
    FinalBlock finalBlock2 = new FinalBlock(block, invalidFinalDataLength);
    Assertions.assertThat(plugin.validateExternalBlock(finalBlock2)).isFalse();
    FloodableEvent event = FloodableEvent.create(createShardEvent, finalBlock2, storage);
    Assertions.assertThat(blockchain.addPendingEvent(event)).isFalse();
    finder.setEvent(event.getExecutableEvent());
    Assertions.assertThat(blockchain.addPendingEvent(event.compress())).isFalse();
  }

  @Test
  public void eventsWhileSyncing() {
    KeyPair initialProducer = new KeyPair(BigInteger.ONE);
    ExecutableEvent reroutedSystemEvent =
        new ExecutableEvent(
            null,
            new EventTransaction(
                TestObjects.EMPTY_HASH,
                new ShardRoute(TestObjects.SHARD_ONE, 1L),
                2,
                1L,
                0,
                null,
                new InnerSystemEvent.CreateAccountEvent(TestObjects.ACCOUNT_FOUR)));
    ExecutableEvent consensusEvent =
        new ExecutableEvent(
            null,
            new EventTransaction(
                TestObjects.EMPTY_HASH,
                new ShardRoute(TestObjects.SHARD_ONE, 2L),
                2,
                1L,
                0,
                null,
                new InnerSystemEvent.UpdateGlobalPluginStateEvent(
                    ChainPluginType.CONSENSUS, GlobalPluginStateUpdate.create(new byte[0]))));
    ExecutableEvent reroutedEvent = createEvent(TestObjects.SHARD_ONE, 3);

    BlockchainLedger shard =
        register(
            BlockchainLedger.createBlockChain(
                ObjectCreator.networkConfig(),
                MemoryStorage.createRootDirectory(temporaryFolder.toFile(), storage),
                setUpGenesis("ChainId", TestObjects.SHARD_ONE, singleShardState()),
                new DummyExecutableEventFinder()));
    addPendingEvent(
        shard, initialProducer, true, reroutedSystemEvent, consensusEvent, reroutedEvent);

    shard.appendBlock(
        createBlock(
            initialProducer,
            shard.latest(),
            2L,
            List.of(
                reroutedSystemEvent.identifier(),
                consensusEvent.identifier(),
                reroutedEvent.identifier())));

    ExecutedState executedState = shard.latest().getState().getExecutedState();
    Collection<Hash> eventTransactions = executedState.getEventTransactions();
    Assertions.assertThat(eventTransactions).hasSize(3);
    Stream<Hash> spawner =
        eventTransactions.stream().map(executedState.getSpawnedEvents()::getValue);
    Assertions.assertThat(spawner)
        .containsExactlyInAnyOrder(
            reroutedSystemEvent.identifier(),
            consensusEvent.identifier(),
            reroutedEvent.identifier());
    Assertions.assertThat(executedState.getExecutionStatus().size()).isEqualTo(3);
  }

  @Test
  public void shouldAllowReceivingEventsFromMaster() {
    KeyPair initialProducer = new KeyPair(BigInteger.ONE);

    ExecutableEvent syncEvent =
        new ExecutableEvent(
            null,
            new EventTransaction(
                TestObjects.EMPTY_HASH,
                new ShardRoute(TestObjects.SHARD_ONE, 1L),
                2,
                1L,
                0,
                null,
                new SyncEvent(List.of(), List.of(), List.of())));

    RootDirectory rootDataDirectory =
        MemoryStorage.createRootDirectory(temporaryFolder.toFile(), storage);
    BlockchainLedger shard =
        register(
            BlockchainLedger.createBlockChain(
                ObjectCreator.networkConfig(),
                rootDataDirectory,
                setUpGenesis("ChainId", TestObjects.SHARD_ONE, singleShardState()),
                new DummyExecutableEventFinder()));

    BlockchainStorage blockchainStorage = new BlockchainStorage(rootDataDirectory, "");
    Assertions.assertThat(blockchainStorage.getBlock(0))
        .describedAs("Should save the genesis block to storage")
        .isNotNull();
    blockchainStorage.close();

    addPendingEvent(shard, initialProducer, true, syncEvent);
    shard.appendBlock(
        createBlock(initialProducer, shard.latest(), 2L, List.of(syncEvent.identifier())));

    Assertions.assertThat(shard.latest().getState().isSyncing()).isFalse();

    ExecutableEvent executableEvent = createEvent(TestObjects.SHARD_ONE, 2L);

    AtomicInteger events = new AtomicInteger();
    shard.attach(new BlockchainLedger.Listener() {});
    shard.attach(
        new BlockchainLedger.Listener() {
          @Override
          public void newPendingEvent(ExecutableEvent event) {
            events.incrementAndGet();
          }
        });

    addPendingEvent(shard, initialProducer, true, executableEvent);
    // Add again to ensure listener is only invoked once
    addPendingEvent(shard, initialProducer, true, executableEvent);

    shard.appendBlock(
        createBlock(initialProducer, shard.latest(), 2L, List.of(executableEvent.identifier())));
    Assertions.assertThat(shard.latest().getState().getExecutedState().getEventTransactions())
        .hasSize(0);
    shard.appendBlock(createBlock(initialProducer, shard.latest(), 2L));

    Assertions.assertThat(shard.getEventTransaction(executableEvent.identifier())).isNotNull();
    Assertions.assertThat(events).hasValue(1);

    // Outdated event
    addPendingEvent(shard, initialProducer, false, executableEvent);

    // Wrong destination shard
    addPendingEvent(
        "Other Chain Id",
        shard,
        initialProducer,
        false,
        null,
        createEvent(TestObjects.SHARD_ONE, 3));

    // Wrong destination shard
    addPendingEvent(shard, initialProducer, false, createEvent("Somewhere else", 3));

    // Invalid block
    addPendingEvent(shard, new KeyPair(), false, createEvent(TestObjects.SHARD_ONE, 3));
    shard.close();
  }

  @Test
  public void failsToPayStorageFeeDuringCallback() {
    RootDirectory rootDirectory = MemoryStorage.createRootDirectory(temporaryFolder.toFile());
    BlockchainAddress contractAddress = TestObjects.CONTRACT_PUB1;

    long productionTime = 0;
    byte[] jar = JarBuilder.buildJar(TestContractByInvocation.class);
    ContractState.CallbackInfo callbackInfo =
        ContractState.CallbackInfo.create(
            List.of(TestObjects.EMPTY_HASH), 0L, new LargeByteArray(new byte[0]));
    BlockchainLedger blockchainLedger =
        register(
            BlockchainLedger.createForTest(
                rootDirectory,
                mutable -> {
                  StateHelper.initial(mutable);
                  mutable.addActiveShard(FunctionUtility.noOpBiConsumer(), null, "SomeShard");
                  CoreContractStateTest.createContract(
                      mutable, jar, contractAddress, JarClassLoaderTest.getPublicBinderJar());
                  FeePluginHelper.enableStorageFees(mutable, contractAddress, productionTime);
                  mutable.addCallbacks(
                      TestObjects.ACCOUNT_ONE,
                      contractAddress,
                      null,
                      TestObjects.EMPTY_HASH,
                      callbackInfo);
                },
                new BlockChainTestNetwork(),
                new DummyExecutableEventFinder()));

    // AccountPlugin triggers storage check for all blocks not 0 modulo 256.
    // Make sure the next block will not do a storage check to ensure the
    // contract is removed as part of handling the callback event.
    while ((blockchainLedger.latest().getBlockTime() + 1) % 256 != 0) {
      Block block =
          new Block(
              blockchainLedger.getLatestProductionTime() + 1,
              blockchainLedger.getBlockTime() + 1,
              0,
              blockchainLedger.getLatestBlock().identifier(),
              blockchainLedger.latest().getState().getHash(),
              List.of(),
              List.of());
      blockchainLedger.appendBlock(
          new FinalBlock(block, SafeDataOutputStream.serialize(signer.sign(block.identifier()))));
    }

    InnerSystemEvent.CallbackEvent callbackEvent =
        new InnerSystemEvent.CallbackEvent(
            new ReturnEnvelope(contractAddress), TestObjects.EMPTY_HASH, false, new byte[0]);
    ExecutableEvent event =
        new ExecutableEvent(
            "SomeShard",
            new EventTransaction(
                TestObjects.EMPTY_HASH, new ShardRoute(null, 1), 0, 0, 0, null, callbackEvent));
    addPendingEvent(
        blockchainLedger.getChainId(), blockchainLedger, signer, true, "SomeShard", event);
    int millisHour = 1_000 * 60 * 60;
    BlockAndState latest = blockchainLedger.latest();
    Block latestBlock = latest.getBlock();
    Block block =
        new Block(
            millisHour * 10_000L,
            latestBlock.getBlockTime() + 1,
            latestBlock.getBlockTime(),
            latestBlock.identifier(),
            latest.getState().getHash(),
            List.of(event.identifier()),
            List.of());
    blockchainLedger.appendBlock(
        new FinalBlock(block, SafeDataOutputStream.serialize(signer.sign(block.identifier()))));

    Assertions.assertThat(blockchainLedger.latest().getState().getAccount(contractAddress))
        .isNull();
    Assertions.assertThat(
            blockchainLedger.latest().getState().getCoreContractState(contractAddress))
        .isNull();
  }

  private void addPendingEvent(
      BlockchainLedger shard, KeyPair producer, boolean result, ExecutableEvent... events) {
    addPendingEvent(shard.getChainId(), shard, producer, result, null, events);
  }

  private void addPendingEvent(
      String chainId,
      BlockchainLedger shard,
      KeyPair producer,
      boolean result,
      String sendingShard,
      ExecutableEvent... events) {
    StateStorageRaw storage = ObjectCreator.createMemoryStateStorage();
    ImmutableChainState state =
        StateHelper.withEvent(
            MutableChainState.emptyMaster(new ChainStateCache(storage)),
            chainId,
            sendingShard,
            Arrays.stream(events).map(ExecutableEvent::identifier).toArray(Hash[]::new));
    state.saveExecutedState(
        s -> new StateSerializer(storage, true).write(s, ExecutedState.class).hash());
    Block block = createBlock(state);
    FinalBlock finalBlock =
        new FinalBlock(block, SafeDataOutputStream.serialize(producer.sign(block.identifier())));
    for (ExecutableEvent event : events) {
      Assertions.assertThat(
              shard.addPendingEvent(FloodableEvent.create(event, finalBlock, storage)))
          .isEqualTo(result);
    }
  }

  ExecutableEvent createEvent(String destination, long nonce) {
    return new ExecutableEvent(
        null,
        EventTransaction.createStandalone(
            TestObjects.EMPTY_HASH,
            TestObjects.ACCOUNT_FOUR,
            0L,
            InteractWithContractTransaction.create(TestObjects.CONTRACT_PUB1, new byte[0]),
            new ShardRoute(destination, nonce),
            2L,
            1L));
  }

  private Consumer<MutableChainState> getStateBuilder() {
    final byte[] consensusJar =
        JarBuilder.buildJar(BlockchainLedgerConsensusTest.InnerConsensus.class);
    final byte[] routingJar = JarBuilder.buildJar(BlockchainLedgerRoutingTest.InnerRouting.class);

    BlockchainAddress producer = new KeyPair(BigInteger.ONE).getPublic().createAddress();
    return mutable -> {
      mutable.setPlugin(
          FunctionUtility.noOpBiConsumer(),
          null,
          ChainPluginType.CONSENSUS,
          consensusJar,
          SafeDataOutputStream.serialize(producer));
      mutable.setPlugin(
          FunctionUtility.noOpBiConsumer(),
          null,
          ChainPluginType.ROUTING,
          routingJar,
          SafeDataOutputStream.serialize(FunctionUtility.noOpConsumer()));
      mutable.addActiveShard(
          FunctionUtility.noOpBiConsumer(), TestObjects.SHARD_ONE, TestObjects.SHARD_ONE);
      mutable.bumpGovernanceVersion();
    };
  }

  @Test
  public void shouldBeAbleToPatchFromStorage() {
    RootDirectory rootDirectory =
        MemoryStorage.createRootDirectory(temporaryFolder.toFile(), storage);
    BlockchainLedger shard =
        register(
            BlockchainLedger.createBlockChain(
                ObjectCreator.networkConfig(),
                rootDirectory,
                setUpGenesis("ChainId", TestObjects.SHARD_ONE, singleShardState()),
                new DummyExecutableEventFinder()));

    shard.appendBlock(createBlock(new KeyPair(BigInteger.ONE), shard.latest(), 2L));

    Assertions.assertThat(shard.getBlockTime()).isEqualTo(1L);
    shard.close();

    BlockchainStorageTest.clearStates(rootDirectory, storage);

    BlockchainLedger restarted =
        register(
            BlockchainLedger.createBlockChain(
                ObjectCreator.networkConfig(),
                rootDirectory,
                setUpGenesis("ChainId", TestObjects.SHARD_ONE, FunctionUtility.noOpConsumer()),
                new DummyExecutableEventFinder()));
    Assertions.assertThat(restarted.getBlockTime()).isEqualTo(1L);
  }

  @Test
  public void shouldBeAbleToInitializeGenesisFromStorage() {
    BlockchainLedger shard =
        register(
            BlockchainLedger.createBlockChain(
                ObjectCreator.networkConfig(),
                MemoryStorage.createRootDirectory(temporaryFolder.toFile(), storage),
                setUpGenesis("ChainId", TestObjects.SHARD_ONE, singleShardState()),
                new DummyExecutableEventFinder()));

    Assertions.assertThat(shard.getBlockTime()).isEqualTo(0L);
    shard.close();

    BlockchainLedger restarted =
        register(
            BlockchainLedger.createBlockChain(
                ObjectCreator.networkConfig(),
                MemoryStorage.createRootDirectory(temporaryFolder.toFile(), storage),
                null,
                new DummyExecutableEventFinder()));
    Assertions.assertThat(restarted.getBlockTime()).isEqualTo(0L);
  }

  private Block createBlock(ImmutableChainState state) {
    return new Block(
        System.currentTimeMillis(),
        3L,
        2L,
        TestObjects.EMPTY_HASH,
        state.getHash(),
        List.of(),
        List.of());
  }

  FinalBlock createBlock(KeyPair initialProducer, BlockAndState latest, long committeeId) {
    return createBlock(initialProducer, latest, committeeId, List.of());
  }

  FinalBlock createBlock(
      KeyPair initialProducer, BlockAndState latest, long committeeId, List<Hash> events) {
    Block block =
        new Block(
            System.currentTimeMillis(),
            latest.getBlock().getBlockTime() + 1,
            committeeId,
            latest.getBlock().identifier(),
            latest.getState().getHash(),
            events,
            List.of());
    return new FinalBlock(
        block, SafeDataOutputStream.serialize(initialProducer.sign(block.identifier())));
  }

  @Test
  public void propagateGovernanceEvent() {
    KeyPair initialProducer = new KeyPair(BigInteger.ONE);
    RootDirectory rootDirectory =
        MemoryStorage.createRootDirectory(temporaryFolder.toFile(), storage);
    BlockchainLedger blockchain =
        register(
            BlockchainLedger.createForTest(
                rootDirectory,
                mutableChainState ->
                    mutableChainState.addActiveShard(
                        FunctionUtility.noOpBiConsumer(), null, TestObjects.SHARD_ONE),
                new BlockChainTestNetwork(),
                new DummyExecutableEventFinder()));

    ExecutableEvent createShardEvent =
        new ExecutableEvent(
            TestObjects.SHARD_ONE,
            new EventTransaction(
                TestObjects.EMPTY_HASH,
                new ShardRoute(null, 1),
                2L,
                0L,
                0,
                null,
                new InnerSystemEvent.UpdatePluginEvent(
                    ChainPluginType.ROUTING,
                    JarBuilder.buildJar(BlockchainLedgerRoutingTest.ShardOneRouting.class),
                    new byte[0])));
    addPendingEvent(
        blockchain.getChainId(),
        blockchain,
        initialProducer,
        true,
        TestObjects.SHARD_ONE,
        createShardEvent);

    BlockAndState latest = blockchain.latest();
    blockchain.appendBlock(
        createBlock(
            initialProducer,
            latest,
            latest.getBlockTime() + 1,
            List.of(createShardEvent.identifier())));
    ImmutableChainState state = blockchain.latest().getState();
    Assertions.assertThat(state.getGovernanceVersion()).isEqualTo(1);
    // Propagated event and sync event
    Assertions.assertThat(state.getExecutedState().getEventTransactions()).hasSize(2);
    Assertions.assertThat(state.getExecutedState().getExecutionStatus().size()).isEqualTo(1);
    Hash event = state.getExecutedState().getEventTransactions().iterator().next();
    ExecutableEvent eventTransaction = blockchain.getEventTransaction(event);
    Assertions.assertThat(eventTransaction).isNotNull();
    Assertions.assertThat(eventTransaction.getEvent().getDestinationShard())
        .isEqualTo(TestObjects.SHARD_ONE);
  }

  /** For testing. */
  public static final class MyContract extends PubContract<StateVoid> {}

  private static final class MyOtherContract extends PubContract<StateLong> {}
}
