package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static com.partisiablockchain.blockchain.BlockchainLedgerTestHelper.createBlock;
import static com.partisiablockchain.blockchain.BlockchainLedgerTestHelper.createFinalBlock;

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.CloseableTest;
import com.partisiablockchain.DummyExecutableEventFinder;
import com.partisiablockchain.TestObjects;
import com.partisiablockchain.ThreadedTestHelper;
import com.partisiablockchain.blockchain.BlockchainLedger.BlockAndState;
import com.partisiablockchain.blockchain.BlockchainLedger.Listener;
import com.partisiablockchain.blockchain.contract.CoreContractStateTest;
import com.partisiablockchain.blockchain.contract.InteractWithContractTransaction;
import com.partisiablockchain.blockchain.transaction.CoreTransactionPart;
import com.partisiablockchain.blockchain.transaction.SignedTransaction;
import com.partisiablockchain.contract.sys.SysContract;
import com.partisiablockchain.contract.sys.SysContractContext;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.flooding.ObjectCreator;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.serialization.StateVoid;
import com.partisiablockchain.storage.MemoryStorage;
import com.secata.jarutil.JarBuilder;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.coverage.FunctionUtility;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** Test. */
public final class BlockchainLedgerConsensusTest extends CloseableTest {

  private static final byte[] consensusJar = JarBuilder.buildJar(SingleRollback.class);
  private final BlockChainTestNetwork network = new BlockChainTestNetwork();
  private final KeyPair keyPair = new KeyPair(BigInteger.ONE);
  private final BlockchainAddress signer = keyPair.getPublic().createAddress();
  private BlockchainLedger blockchain;
  private Block block;
  private Block latestBlock;
  private byte nextFinalization = 0;

  private final ArrayList<Block> finalBlocks = new ArrayList<>();
  private final ArrayList<Block> proposals = new ArrayList<>();
  private BlockAndState genesis;
  private final DummyExecutableEventFinder finder = new DummyExecutableEventFinder();

  private final Map<MemoryStorage.PathAndHash, byte[]> storageData = new ConcurrentHashMap<>();

  /** Setup blockchain. */
  @BeforeEach
  public void setUp() {
    this.blockchain =
        register(
            BlockchainLedger.createForTest(
                MemoryStorage.createRootDirectory(temporaryFolder.toFile(), storageData),
                this::setupChain,
                network,
                finder));
    finalBlocks.add(blockchain.getLatestBlock());
    blockchain.attach(
        new Listener() {
          @Override
          public void newFinalBlock(Block block, ImmutableChainState state) {
            finalBlocks.add(block);
          }

          @Override
          public void newBlockProposal(Block block) {
            proposals.add(block);
          }
        });
    genesis = blockchain.latest();
    latestBlock = blockchain.getLatestBlock();
    block = createBlock(blockchain.latest());
  }

  @Test
  public void getters() {
    ImmutableChainState state = blockchain.getChainState();
    Assertions.assertThat(state.getConsensusPlugin()).isInstanceOf(ConsensusPluginWrapper.class);
    Assertions.assertThat(state.getGlobalPluginState(ChainPluginType.CONSENSUS))
        .isInstanceOf(BlockchainLedgerTestHelper.ValueCount.class);
  }

  @Test
  public void invalidFinalBlock() {
    final byte[] invalidFinalization = new byte[0];
    final byte[] validFinalization = new byte[] {nextFinalization};
    testInvalidFinalized(block, invalidFinalization);
    testInvalidFinalized(
        createBlock(
            latestBlock.getProductionTime(),
            latestBlock.getBlockTime() + 1,
            TestObjects.EMPTY_HASH,
            blockchain.latest().getState().getHash(),
            List.of(),
            List.of()),
        validFinalization);
  }

  @Test
  public void getExecutedPossibleExecutedTransaction() {
    InteractWithContractTransaction interact =
        InteractWithContractTransaction.create(TestObjects.CONTRACT_PUB1, new byte[10]);
    SignedTransaction signedTransaction =
        BlockchainLedgerTestHelper.signedTransaction(blockchain, keyPair, signer, interact);
    blockchain.addPendingTransaction(signedTransaction);
    FinalBlock finalBlock =
        new FinalBlock(
            createBlock(blockchain.latest(), signedTransaction.identifier()),
            new byte[] {nextFinalization});
    blockchain.appendBlock(finalBlock);

    interact = InteractWithContractTransaction.create(TestObjects.CONTRACT_SYS, new byte[1]);
    SignedTransaction signedTransaction2 =
        BlockchainLedgerTestHelper.signedTransaction(
            blockchain, keyPair, keyPair.getPublic().createAddress(), interact);
    blockchain.addPendingTransaction(signedTransaction2);
    FinalBlock finalBlock2 =
        new FinalBlock(
            createBlock(blockchain.latest(), signedTransaction2.identifier()),
            new byte[] {nextFinalization});
    blockchain.appendBlock(finalBlock2);

    BlockchainLedger.PossiblyFinalizedTransaction transaction =
        blockchain.getExecutedTransaction(signedTransaction2.identifier());
    boolean isInProposal = blockchain.isInProposals(finalBlock2.getBlock().identifier());
    Assertions.assertThat(isInProposal).isTrue();
    Assertions.assertThat(blockchain.isInProposals(Hash.create(s -> s.writeString("hej"))))
        .isFalse();
    Assertions.assertThat(blockchain.getTransaction(signedTransaction2.identifier())).isNull();
    Assertions.assertThat(transaction.finalized()).isFalse();
    Assertions.assertThat(transaction.transaction().getInner().identifier())
        .isEqualTo(signedTransaction2.identifier());
    Assertions.assertThat(finalBlock2.getBlock().identifier())
        .isEqualTo(transaction.transaction().getBlockHash());
    SignedTransaction signedTransactionGotten =
        (SignedTransaction) transaction.transaction().getInner();
    Assertions.assertThat(signedTransactionGotten).isEqualTo(signedTransaction2);
  }

  private void updateConsensus(Class<?> consensus) {
    BlockchainLedgerTestHelper.appendBlock(
        blockchain,
        keyPair,
        signer,
        nextFinalization,
        UpdateConsensusInTest.create(JarBuilder.buildJar(consensus)));
    nextFinalization = 1;
  }

  @Test
  public void possibleHeads() {
    BlockAndState latest = blockchain.latest();
    FinalBlock block = createFinalBlock(latest, nextFinalization);
    blockchain.appendBlock(block);
    blockchain.appendBlock(
        new FinalBlock(
            new Block(
                System.currentTimeMillis() + 1,
                latest.getBlockTime() + 1,
                latest.getBlockTime(),
                latest.getBlock().identifier(),
                latest.getState().getHash(),
                List.of(),
                List.of(new Hash[] {})),
            new byte[] {nextFinalization}));
    Assertions.assertThat(blockchain.getProposals()).hasSize(2);
    List<BlockAndStateWithParent> possibleHeads = blockchain.getPossibleHeads();
    Assertions.assertThat(possibleHeads).hasSize(3);
    Assertions.assertThat(possibleHeads.get(0).finalState()).isEqualTo(latest.getState());
    Assertions.assertThat(possibleHeads.get(1).finalState()).isEqualTo(latest.getState());
    Assertions.assertThat(possibleHeads.get(2).currentBlock()).isEqualTo(latest.getBlock());
    Assertions.assertThat(possibleHeads.get(2).contracts().newContracts)
        .hasSize(1)
        .containsExactly(TestObjects.CONTRACT_SYS);
    Assertions.assertThat(possibleHeads.get(2).contracts().updatedContracts).isEmpty();
    Assertions.assertThat(possibleHeads.get(2).contracts().removedContract).isEmpty();
  }

  @Test
  public void proposeTwice() {
    FinalBlock block = createFinalBlock(blockchain.latest(), nextFinalization);
    blockchain.appendBlock(block);
    Assertions.assertThat(blockchain.getProposals()).hasSize(1);
    blockchain.appendBlock(block);
    Assertions.assertThat(blockchain.getProposals()).hasSize(1);
    Assertions.assertThat(blockchain.getLatestBlock()).isEqualTo(genesis.getBlock());
  }

  @Test
  public void finalizeProposal() {
    FinalBlock block = createFinalBlock(blockchain.latest(), nextFinalization);
    blockchain.appendBlock(block);
    Assertions.assertThat(blockchain.getProposals()).hasSize(1);
    FinalBlock secondBlock = createFinalBlock(blockchain.getProposals().get(0), nextFinalization);
    blockchain.appendBlock(secondBlock);
    Assertions.assertThat(blockchain.getProposals()).hasSize(1);
    Assertions.assertThat(blockchain.getLatestBlock()).isEqualTo(block.getBlock());
  }

  @Test
  public void proposalShouldBeConsiderProposalAfterReboot() {
    FinalBlock block = createFinalBlock(blockchain.latest(), nextFinalization);
    blockchain.appendBlock(block);
    Assertions.assertThat(blockchain.getProposals()).hasSize(1);

    blockchain.close();
    this.blockchain =
        register(
            BlockchainLedger.createBlockChain(
                ObjectCreator.networkConfig(),
                MemoryStorage.createRootDirectory(temporaryFolder.toFile(), storageData),
                null));

    Assertions.assertThat(blockchain.getLatestBlock()).isNotEqualTo(block.getBlock());
    Assertions.assertThat(blockchain.getProposals()).hasSize(1);
    Assertions.assertThat(blockchain.getProposals().get(0).getBlock()).isEqualTo(block.getBlock());
  }

  @Test
  public void proposalAndImmediateFinalizationOfNext() {
    updateConsensus(ZeroRollback.class);
    appendBlockProposal();
  }

  @Test
  public void proposalRolledBackByImmediateFinalization() {
    BlockAndState latestBlock = blockchain.latest();
    FinalBlock block = createFinalBlock(latestBlock, nextFinalization);
    blockchain.appendBlock(block);
    Assertions.assertThat(blockchain.getProposals()).hasSize(1);
    FinalBlock finalBlock =
        new FinalBlock(
            new Block(
                // Adding 1 to currentTimeMillis so the block won't collide with previous making it
                // historical.
                System.currentTimeMillis() + 1,
                latestBlock.getBlockTime() + 1,
                latestBlock.getBlockTime() + 1,
                latestBlock.getBlock().identifier(),
                latestBlock.getState().getHash(),
                List.of(),
                List.of()),
            new byte[] {nextFinalization, 1});
    blockchain.appendBlock(finalBlock);
    Assertions.assertThat(blockchain.getProposals()).hasSize(0);
    Assertions.assertThat(blockchain.getLatestBlock()).isEqualTo(finalBlock.getBlock());
  }

  @Test
  public void addPendingValidInProposal() {
    InteractWithContractTransaction interact =
        InteractWithContractTransaction.create(TestObjects.CONTRACT_SYS, new byte[0]);
    BlockchainLedgerTestHelper.appendBlock(blockchain, keyPair, signer, nextFinalization, interact);
    Assertions.assertThat(blockchain.getProposals()).hasSize(1);
    BlockAndState firstProposal = blockchain.getProposals().get(0);
    FinalBlock secondProposal = createFinalBlock(genesis, nextFinalization);
    blockchain.appendBlock(secondProposal);
    Assertions.assertThat(blockchain.getProposals()).hasSize(2);

    Assertions.assertThat(
            blockchain.addPendingTransaction(
                SignedTransaction.create(
                        CoreTransactionPart.create(
                            firstProposal.getState().getAccount(signer).getNonce(),
                            System.currentTimeMillis() + 100_000,
                            0),
                        interact)
                    .sign(keyPair, blockchain.getChainId())))
        .isTrue();

    Assertions.assertThat(blockchain.getPendingTransactions()).hasSize(2);
    blockchain.appendBlock(createFinalBlock(blockchain.getProposals().get(1), nextFinalization));
    Assertions.assertThat(blockchain.getLatestBlock()).isEqualTo(secondProposal.getBlock());
    Assertions.assertThat(blockchain.getPendingTransactions()).hasSize(1);
  }

  @Test
  public void noMatchingProposal() {
    FinalBlock finalBlock = createFinalBlock(genesis, nextFinalization);
    FinalBlock unknownParent =
        new FinalBlock(
            createBlock(
                finalBlock.getBlock().getProductionTime(),
                finalBlock.getBlock().getBlockTime() + 1,
                finalBlock.getBlock().identifier(),
                TestObjects.EMPTY_HASH,
                List.of(),
                List.of()),
            new byte[] {nextFinalization});
    blockchain.appendBlock(unknownParent);
    Assertions.assertThat(blockchain.getProposals()).isEmpty();
    Assertions.assertThat(blockchain.getLatestBlock()).isEqualTo(genesis.getBlock());
  }

  @Test
  public void consensusJar() {
    final byte[] consensusJar = BlockchainLedgerConsensusTest.consensusJar;

    ImmutableChainState immutableChainState = blockchain.getChainState();
    Assertions.assertThat(immutableChainState.getPluginJar(ChainPluginType.CONSENSUS)).isNotNull();
    Assertions.assertThat(immutableChainState.getPluginJar(ChainPluginType.CONSENSUS).getData())
        .isEqualTo(consensusJar);

    MutableChainState mutableChainState = immutableChainState.asMutable();
    Assertions.assertThat(mutableChainState.getPluginJar(ChainPluginType.CONSENSUS)).isNotNull();
    Assertions.assertThat(mutableChainState.getPluginJar(ChainPluginType.CONSENSUS).getData())
        .isEqualTo(consensusJar);
  }

  @Test
  public void updateForBlockIsCalled() throws Exception {
    updateConsensus(ZeroRollback.class);
    appendBlockProposal();
    updateConsensus(UpdateForBlockTestPlugin.class);

    for (int i = 0; i < 100; i++) {
      blockchain.appendBlock(
          new FinalBlock(createBlock(blockchain.latest()), new byte[] {nextFinalization}));
      // If the block production time is not strictly later (millisecond precision)
      // than the previous block, it will not be valid.
      // Therefore the test should wait 1 ms between appending blocks.
      TimeUnit.MILLISECONDS.sleep(1);
    }

    LocalCounterState state =
        (LocalCounterState) blockchain.getChainState().getLocalConsensusPluginState();

    Assertions.assertThat(state.getCount()).isEqualTo(100);

    MutableChainState mutableChainState = blockchain.getChainState().asMutable();

    state = (LocalCounterState) mutableChainState.getLocalConsensusPluginState();
    Assertions.assertThat(state.getCount()).isEqualTo(100);
  }

  @Test
  public void noInlineExecutionWithUnhandledEvents() {
    this.blockchain =
        register(
            BlockchainLedger.createForTest(
                MemoryStorage.createRootDirectory(temporaryFolder.toFile()),
                mutableChainState -> {
                  setupChain(mutableChainState);
                  // Fake a spawned event that has not yet been executed
                  mutableChainState.routeToShard((String) null);
                },
                network,
                new DummyExecutableEventFinder()));
    updateConsensus(WithShardConsensus.class);
    ImmutableChainState state = blockchain.getProposals().get(0).getState();
    Assertions.assertThat(state.getExecutedState().getExecutionStatus().size()).isEqualTo(1);
    Assertions.assertThat(blockchain.getPendingEvents()).hasSize(1);
  }

  private void appendBlockProposal() {
    List<BlockAndState> proposals = blockchain.getProposals();
    Assertions.assertThat(proposals).hasSize(1);
    BlockAndState parent = proposals.get(0);
    FinalBlock nextBlock = appendProposal(parent);

    proposals = blockchain.getProposals();
    Assertions.assertThat(proposals).hasSize(0);
    Assertions.assertThat(blockchain.getLatestBlock()).isEqualTo(nextBlock.getBlock());

    assertAsync(finalBlocks, genesis.getBlock(), parent.getBlock(), nextBlock.getBlock());
    assertAsync(this.proposals, parent.getBlock(), nextBlock.getBlock());
  }

  private FinalBlock appendProposal(BlockAndState parent) {
    Block parentBlock = parent.getBlock();
    FinalBlock finalBlock =
        new FinalBlock(
            new Block(
                System.currentTimeMillis(),
                parentBlock.getBlockTime() + 1,
                parentBlock.getBlockTime(),
                parentBlock.identifier(),
                parent.getState().getHash(),
                List.of(),
                List.of()),
            new byte[] {1});
    blockchain.appendBlock(finalBlock);
    return finalBlock;
  }

  private void assertAsync(List<Block> proposals, Block... block) {
    waitForSize(proposals, block.length);
    Assertions.assertThat(proposals).containsExactly(block);
  }

  private void waitForSize(List<Block> proposals, int length) {
    ThreadedTestHelper.waitForCondition(() -> proposals.size() == length, 1_000);
  }

  private void testInvalidFinalized(Block block, byte[] finalizationData) {
    Assertions.assertThatThrownBy(
        () -> blockchain.appendBlock(new FinalBlock(block, finalizationData)));
  }

  private void setupChain(MutableChainState mutableChainState) {
    mutableChainState.createAccount(signer);
    configureConsensusWithRollback(mutableChainState);
    Assertions.assertThat(mutableChainState.getConsensusPlugin())
        .isInstanceOf(ConsensusPluginWrapper.class);
    StateSerializable globalConsensus =
        mutableChainState.getGlobalPluginState(ChainPluginType.CONSENSUS);
    Assertions.assertThat(globalConsensus).isNotNull();
    Assertions.assertThat(globalConsensus)
        .isInstanceOf(BlockchainLedgerTestHelper.ByteVerifying.class);
    Assertions.assertThat(((BlockchainLedgerTestHelper.ByteVerifying) globalConsensus).getByte())
        .isEqualTo((byte) 0);
    CoreContractStateTest.createContract(
        mutableChainState,
        JarBuilder.buildJar(UpdateConsensusInTest.class),
        TestObjects.CONTRACT_SYS,
        JarClassLoaderTest.getSysBinderJar());
  }

  static void configureConsensusWithRollback(MutableChainState mutableChainState) {
    byte[] rpc = new byte[0];
    mutableChainState.setPlugin(
        FunctionUtility.noOpBiConsumer(), null, ChainPluginType.CONSENSUS, consensusJar, rpc);
  }

  /** Test. */
  @Immutable
  public abstract static class AbstractConsensus
      extends BlockchainConsensusPlugin<SingleAddressState, StateVoid> {

    @Override
    public Class<SingleAddressState> getGlobalStateClass() {
      return SingleAddressState.class;
    }

    @Override
    public InvokeResult<SingleAddressState> invokeGlobal(
        PluginContext pluginContext, SingleAddressState state, byte[] rpc) {
      return new InvokeResult<>(state, rpc);
    }

    @Override
    public StateVoid updateForBlock(
        PluginContext pluginContext,
        SingleAddressState globalState,
        StateVoid localState,
        Block block) {
      return localState;
    }

    @Override
    public SingleAddressState migrateGlobal(StateAccessor currentGlobal, SafeDataInputStream rpc) {
      return new SingleAddressState(BlockchainAddress.read(rpc));
    }

    @Override
    public final StateVoid migrateLocal(StateAccessor currentLocal) {
      return null;
    }

    @Override
    public Class<StateVoid> getLocalStateClass() {
      return StateVoid.class;
    }
  }

  /** Test. */
  @Immutable
  public static final class InnerConsensus extends AbstractConsensus {

    @Override
    public BlockValidation validateLocalBlock(
        SingleAddressState globalState, StateVoid local, FinalBlock block) {
      Signature signature =
          SafeDataInputStream.readFully(block.getFinalizationData(), Signature::read);
      BlockchainAddress signer = signature.recoverSender(block.getBlock().identifier());
      boolean accepted = signer.equals(globalState.address);
      if (accepted) {
        return BlockValidation.createAccepted(false);
      } else {
        return BlockValidation.createRejected();
      }
    }

    @Override
    public boolean validateExternalBlock(SingleAddressState globalState, FinalBlock block) {
      Signature signature =
          SafeDataInputStream.readFully(block.getFinalizationData(), Signature::read);
      BlockchainAddress signer = signature.recoverSender(block.getBlock().identifier());
      return signer.equals(globalState.address);
    }
  }

  /** Test. */
  @Immutable
  public static final class SingleAddressState implements StateSerializable {

    public final BlockchainAddress address;

    @SuppressWarnings("unused")
    public SingleAddressState() {
      address = null;
    }

    public SingleAddressState(BlockchainAddress address) {
      this.address = address;
    }
  }

  /** Test. */
  @Immutable
  public static final class UpdateForBlockTestPlugin
      extends BlockchainConsensusPlugin<StateVoid, LocalCounterState> {

    @Override
    public BlockValidation validateLocalBlock(
        StateVoid globalState, LocalCounterState local, FinalBlock block) {
      return BlockValidation.createAccepted(false);
    }

    @Override
    public boolean validateExternalBlock(StateVoid globalState, FinalBlock block) {
      return false;
    }

    @Override
    public InvokeResult<StateVoid> invokeGlobal(
        PluginContext pluginContext, StateVoid state, byte[] rpc) {
      return new InvokeResult<>(state, rpc);
    }

    @Override
    public StateVoid migrateGlobal(StateAccessor currentGlobal, SafeDataInputStream rpc) {
      return null;
    }

    @Override
    public LocalCounterState migrateLocal(StateAccessor currentLocal) {
      return new LocalCounterState(0);
    }

    @Override
    public LocalCounterState updateForBlock(
        PluginContext pluginContext,
        StateVoid globalState,
        LocalCounterState localState,
        Block block) {
      return localState.increment();
    }

    @Override
    public Class<StateVoid> getGlobalStateClass() {
      return StateVoid.class;
    }

    @Override
    public Class<LocalCounterState> getLocalStateClass() {
      return LocalCounterState.class;
    }
  }

  /** Test. */
  @Immutable
  public static final class SingleRollback
      extends TestConsensusPlugin<BlockchainLedgerTestHelper.ValueCount> {

    public SingleRollback() {}

    @Override
    public BlockValidation validateLocalBlock(
        BlockchainLedgerTestHelper.ValueCount globalState, StateVoid local, FinalBlock block) {
      byte[] finalizationData = block.getFinalizationData();
      if (finalizationData[0] == globalState.getByte()) {
        return BlockValidation.createAccepted(
            finalizationData.length <= 1 || finalizationData[1] == 0);
      } else {
        return BlockValidation.createRejected();
      }
    }

    @Override
    public boolean validateExternalBlock(
        BlockchainLedgerTestHelper.ValueCount globalState, FinalBlock block) {
      return false;
    }

    @Override
    public InvokeResult<BlockchainLedgerTestHelper.ValueCount> invokeGlobal(
        PluginContext pluginContext, BlockchainLedgerTestHelper.ValueCount state, byte[] rpc) {
      return new InvokeResult<>(state.setByte(rpc[0]), rpc);
    }

    @Override
    public BlockchainLedgerTestHelper.ValueCount migrateGlobal(
        StateAccessor currentGlobal, SafeDataInputStream rpc) {
      return new BlockchainLedgerTestHelper.ValueCount();
    }

    @Override
    public StateVoid migrateLocal(StateAccessor currentLocal) {
      return null;
    }

    @Override
    public Class<BlockchainLedgerTestHelper.ValueCount> getGlobalStateClass() {
      return BlockchainLedgerTestHelper.ValueCount.class;
    }
  }

  /** Test. */
  @Immutable
  public static final class WithShardConsensus
      extends TestConsensusPlugin<BlockchainLedgerTestHelper.ValueCount> {

    @Override
    public BlockValidation validateLocalBlock(
        BlockchainLedgerTestHelper.ValueCount globalState, StateVoid local, FinalBlock block) {
      return BlockValidation.createAccepted(false);
    }

    @Override
    public boolean validateExternalBlock(
        BlockchainLedgerTestHelper.ValueCount globalState, FinalBlock block) {
      return false;
    }

    @Override
    public Class<BlockchainLedgerTestHelper.ValueCount> getGlobalStateClass() {
      return BlockchainLedgerTestHelper.ValueCount.class;
    }

    @Override
    public BlockchainLedgerTestHelper.ValueCount migrateGlobal(
        StateAccessor currentGlobal, SafeDataInputStream rpc) {
      return new BlockchainLedgerTestHelper.ValueCount(rpc.readSignedByte());
    }

    @Override
    public StateVoid migrateLocal(StateAccessor currentLocal) {
      return null;
    }
  }

  /** Test. */
  @Immutable
  public static final class ZeroRollback
      extends TestConsensusPlugin<BlockchainLedgerTestHelper.ValueCount> {

    private static final Logger logger = LoggerFactory.getLogger(ZeroRollback.class);

    @Override
    public BlockValidation validateLocalBlock(
        BlockchainLedgerTestHelper.ValueCount globalState, StateVoid local, FinalBlock block) {
      if (validateExternalBlock(globalState, block)) {
        return BlockValidation.createAccepted(false);
      } else {
        return BlockValidation.createRejected();
      }
    }

    @Override
    public boolean validateExternalBlock(
        BlockchainLedgerTestHelper.ValueCount globalState, FinalBlock block) {
      return block.getFinalizationData()[0] == globalState.getByte();
    }

    @Override
    public Class<BlockchainLedgerTestHelper.ValueCount> getGlobalStateClass() {
      return BlockchainLedgerTestHelper.ValueCount.class;
    }

    @Override
    public BlockchainLedgerTestHelper.ValueCount migrateGlobal(
        StateAccessor currentGlobal, SafeDataInputStream rpc) {
      BlockchainLedgerTestHelper.ValueCount valueCount =
          new BlockchainLedgerTestHelper.ValueCount();
      try {
        byte value = rpc.readSignedByte();
        valueCount = valueCount.setByte(value);
      } catch (Exception e) {
        logger.trace("Ignoring exception", e);
      }
      return valueCount;
    }

    @Override
    public StateVoid migrateLocal(StateAccessor currentLocal) {
      return null;
    }
  }

  /** Test. */
  public static final class UpdateConsensusInTest extends SysContract<StateVoid> {

    static InteractWithContractTransaction create(byte[] jar) {
      return InteractWithContractTransaction.create(
          TestObjects.CONTRACT_SYS,
          SafeDataOutputStream.serialize(stream -> stream.writeDynamicBytes(jar)));
    }

    @Override
    public StateVoid onInvoke(
        SysContractContext context, StateVoid state, SafeDataInputStream rpc) {
      byte[] jar = rpc.readDynamicBytes();
      context.getInvocationCreator().updateConsensusPlugin(jar, new byte[] {1});
      return null;
    }
  }

  /** Test. */
  @Immutable
  public static final class LocalCounterState implements StateSerializable {

    private final int count;

    public LocalCounterState(int count) {
      this.count = count;
    }

    public LocalCounterState increment() {
      return new LocalCounterState(count + 1);
    }

    int getCount() {
      return count;
    }
  }

  /** Test. */
  @Immutable
  public abstract static class TestConsensusPlugin<GlobalT extends StateSerializable>
      extends BlockchainConsensusPlugin<GlobalT, StateVoid> {

    @Override
    public BlockValidation validateLocalBlock(
        GlobalT globalState, StateVoid local, FinalBlock block) {
      return BlockValidation.createAccepted(false);
    }

    @Override
    public InvokeResult<GlobalT> invokeGlobal(
        PluginContext pluginContext, GlobalT state, byte[] rpc) {
      return new InvokeResult<>(state, rpc);
    }

    @Override
    public GlobalT migrateGlobal(StateAccessor currentGlobal, SafeDataInputStream rpc) {
      return null;
    }

    @Override
    public StateVoid updateForBlock(
        PluginContext pluginContext, GlobalT globalState, StateVoid localState, Block block) {
      return localState;
    }

    @Override
    public Class<StateVoid> getLocalStateClass() {
      return StateVoid.class;
    }
  }
}
