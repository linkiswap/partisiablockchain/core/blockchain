package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.CloseableTest;
import com.partisiablockchain.DummyExecutableEventFinder;
import com.partisiablockchain.TestObjects;
import com.partisiablockchain.blockchain.BlockchainLedger.BlockAndState;
import com.partisiablockchain.blockchain.contract.CoreContractStateTest;
import com.partisiablockchain.blockchain.contract.InteractWithContractTransaction;
import com.partisiablockchain.blockchain.fee.FeePluginHelper;
import com.partisiablockchain.blockchain.transaction.CoreTransactionPart;
import com.partisiablockchain.blockchain.transaction.ExecutedTransaction;
import com.partisiablockchain.blockchain.transaction.SignedTransaction;
import com.partisiablockchain.contract.CallbackContext;
import com.partisiablockchain.contract.EventCreator;
import com.partisiablockchain.contract.EventManager;
import com.partisiablockchain.contract.pub.PubContract;
import com.partisiablockchain.contract.pub.PubContractContext;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.serialization.StateVoid;
import com.partisiablockchain.storage.MemoryStorage;
import com.partisiablockchain.storage.RootDirectory;
import com.secata.jarutil.JarBuilder;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;
import org.junit.jupiter.api.Test;

/** Test contract gas cost. */
public final class ContractGasFeeOnBlockchainTest extends CloseableTest {

  private static final KeyPair PRODUCER_KEY = new KeyPair(BigInteger.ONE);
  private static final BlockchainAddress PRODUCER_ACCOUNT =
      PRODUCER_KEY.getPublic().createAddress();

  public static final BlockchainAddress INNER_CONTRACT_ADDRESS = TestObjects.CONTRACT_PUB1;
  public static final BlockchainAddress OUTER_CONTRACT_ADDRESS = TestObjects.CONTRACT_PUB2;

  private static final long OUTER_CONTRACT_MINT_GAS = 101_000;
  private static final long INNER_CONTRACT_MINT_GAS = 1000;
  private static final long OUTER_CONTRACT_STORAGE_AND_NETWORK_GAS = 151;
  private static final long INNER_CONTRACT_STORAGE_AND_NETWORK_GAS = 55;
  private static final long OUTGOING_EVENT_ALLOCATED_GAS = 25_000;
  private static final long CALLBACK_ALLOCATED_GAS = 10_000;
  private static final long OUTGOING_EVENT_FROM_CALLBACK_ALLOCATED_GAS = 5_000;

  /**
   * Add a cost to this cost.
   *
   * @param lhs the cost to add to
   * @param rhs the cost to add
   * @return the result of the addition as a new cost object
   */
  static TransactionCost add(TransactionCost lhs, TransactionCost rhs) {
    HashMap<Hash, Long> map = new HashMap<>(lhs.getNetworkCosts());
    map.putAll(rhs.getNetworkCosts());

    return new TransactionCost(
        lhs.getAllocatedForEvents(),
        lhs.getCpu() + rhs.getCpu(),
        lhs.getRemaining() + rhs.getRemaining(),
        lhs.getPaidByContract() + rhs.getPaidByContract(),
        map);
  }

  /** Test payment for events and callbacks: User pays for both event and callback. */
  @Test
  public void userPaysForEventAndCallback() {
    BlockchainLedger blockchainLedger =
        initBlockChainLedger(INNER_CONTRACT_MINT_GAS, OUTER_CONTRACT_MINT_GAS);

    final BlockAndState initial = blockchainLedger.latest();
    assertThat(FeePluginHelper.balance(initial.getState(), INNER_CONTRACT_ADDRESS))
        .isEqualTo(INNER_CONTRACT_MINT_GAS);
    assertThat(FeePluginHelper.balance(initial.getState(), OUTER_CONTRACT_ADDRESS))
        .isEqualTo(OUTER_CONTRACT_MINT_GAS);

    final long userPaymentGas = 35_500;
    final int instruction = OuterContract.PLEASE_DO_NOT_PAY;
    assertRegisteredGasMatchesExpected(blockchainLedger, true, instruction, userPaymentGas);
  }

  /** Test payment for events and callbacks: Contract pays for event, User pays for callback. */
  @Test
  public void contractPaysForEventUserPaysForCallback() {
    BlockchainLedger blockchainLedger =
        initBlockChainLedger(INNER_CONTRACT_MINT_GAS, OUTER_CONTRACT_MINT_GAS);

    final BlockAndState initial = blockchainLedger.latest();
    assertThat(FeePluginHelper.balance(initial.getState(), INNER_CONTRACT_ADDRESS))
        .isEqualTo(INNER_CONTRACT_MINT_GAS);
    assertThat(FeePluginHelper.balance(initial.getState(), OUTER_CONTRACT_ADDRESS))
        .isEqualTo(OUTER_CONTRACT_MINT_GAS);

    final long userPaymentGas = 11_000;
    final int instruction = OuterContract.PLEASE_PAY_FOR_EVENT;

    assertRegisteredGasMatchesExpected(blockchainLedger, false, instruction, userPaymentGas);
  }

  /** Test payment for events and callbacks: User pays for event, Contract pays for callback. */
  @Test
  public void userPaysForEventContractPaysForCallback() {

    BlockchainLedger blockchainLedger =
        initBlockChainLedger(INNER_CONTRACT_MINT_GAS, OUTER_CONTRACT_MINT_GAS);

    final BlockAndState initial = blockchainLedger.latest();
    assertThat(FeePluginHelper.balance(initial.getState(), INNER_CONTRACT_ADDRESS))
        .isEqualTo(INNER_CONTRACT_MINT_GAS);
    assertThat(FeePluginHelper.balance(initial.getState(), OUTER_CONTRACT_ADDRESS))
        .isEqualTo(OUTER_CONTRACT_MINT_GAS);

    final long userPaymentGas = 26_000;
    final int instruction = OuterContract.PLEASE_PAY_FOR_CALLBACK;

    assertRegisteredGasMatchesExpected(blockchainLedger, false, instruction, userPaymentGas);
  }

  /** Test payment for events and callbacks: Contract pays for event and callback. */
  @Test
  public void contractPaysForEventAndCallback() {

    BlockchainLedger blockchainLedger =
        initBlockChainLedger(INNER_CONTRACT_MINT_GAS, OUTER_CONTRACT_MINT_GAS);

    final BlockAndState initial = blockchainLedger.latest();
    assertThat(FeePluginHelper.balance(initial.getState(), INNER_CONTRACT_ADDRESS))
        .isEqualTo(INNER_CONTRACT_MINT_GAS);
    assertThat(FeePluginHelper.balance(initial.getState(), OUTER_CONTRACT_ADDRESS))
        .isEqualTo(OUTER_CONTRACT_MINT_GAS);

    final long userPaymentGas = 1_000;
    final int instruction = OuterContract.PLEASE_PAY_FOR_BOTH;

    assertRegisteredGasMatchesExpected(blockchainLedger, false, instruction, userPaymentGas);
  }

  private void assertGasBalances(
      BlockchainLedger blockchainLedger,
      long userPaymentGas,
      boolean outerContractFinalGasPositive) {
    final long innerContractGasStatus =
        INNER_CONTRACT_MINT_GAS
            + OUTGOING_EVENT_ALLOCATED_GAS
            + OUTGOING_EVENT_FROM_CALLBACK_ALLOCATED_GAS
            - INNER_CONTRACT_STORAGE_AND_NETWORK_GAS;
    final ImmutableChainState state = blockchainLedger.latest().getState();
    assertThat(FeePluginHelper.balance(state, INNER_CONTRACT_ADDRESS))
        .isEqualTo(innerContractGasStatus);
    final long outerContractGasStatus =
        OUTER_CONTRACT_MINT_GAS
            + userPaymentGas
            - OUTGOING_EVENT_ALLOCATED_GAS
            - OUTGOING_EVENT_FROM_CALLBACK_ALLOCATED_GAS
            - OUTER_CONTRACT_STORAGE_AND_NETWORK_GAS;
    assertThat(FeePluginHelper.balance(state, OUTER_CONTRACT_ADDRESS))
        .isEqualTo(outerContractGasStatus);

    final long outerContractGasChange = outerContractGasStatus - OUTER_CONTRACT_MINT_GAS;
    final long innerContractGasChange = innerContractGasStatus - INNER_CONTRACT_MINT_GAS;
    if (outerContractFinalGasPositive) {
      assertThat(outerContractGasChange).isGreaterThan(0L);
    } else {
      assertThat(outerContractGasChange).isLessThan(0L);
    }
    assertThat(innerContractGasChange).isGreaterThan(0L);
    // Gas in total system is unchanged
    final long totalGasChange =
        userPaymentGas
            - outerContractGasChange
            - OUTER_CONTRACT_STORAGE_AND_NETWORK_GAS
            - innerContractGasChange
            - INNER_CONTRACT_STORAGE_AND_NETWORK_GAS;
    assertThat(totalGasChange).isEqualTo(0L);
  }

  private void assertRegisteredGasMatchesExpected(
      BlockchainLedger blockchainLedger,
      boolean outerContractFinalGasPositive,
      int instruction,
      long userPaymentGas) {
    Hash hash = sendTransactionToOuterContract(blockchainLedger, userPaymentGas, instruction);

    assertGasBalances(blockchainLedger, userPaymentGas, outerContractFinalGasPositive);

    TransactionCost transactionCost = getTotalRegisteredCostForTransaction(blockchainLedger, hash);
    assertThat(transactionCost.getTotalCost() - transactionCost.getPaidByContract())
        .isEqualTo(userPaymentGas);
  }

  /**
   * Send a user transaction to the outer contract.
   *
   * @param blockchainLedger The blockchain ledger.
   * @param userPaymentGas The gas that the user pays for the transactions.
   * @param instruction The instruction sent to the outer contract (controls how it acts)
   * @return the transaction hash
   */
  private Hash sendTransactionToOuterContract(
      BlockchainLedger blockchainLedger, long userPaymentGas, int instruction) {
    final String chainId = blockchainLedger.getChainId();
    final long nonce = blockchainLedger.latest().getState().getAccount(PRODUCER_ACCOUNT).getNonce();
    SignedTransaction invokePayForOutgoingEvent =
        SignedTransaction.create(
                CoreTransactionPart.create(
                    nonce, System.currentTimeMillis() + 100_000, userPaymentGas),
                InteractWithContractTransaction.create(
                    OUTER_CONTRACT_ADDRESS,
                    SafeDataOutputStream.serialize(
                        s -> {
                          s.writeInt(instruction);
                        })))
            .sign(PRODUCER_KEY, chainId);
    blockchainLedger.addPendingTransaction(invokePayForOutgoingEvent);
    BlockchainLedgerTest.produce(blockchainLedger, invokePayForOutgoingEvent);
    return invokePayForOutgoingEvent.identifier();
  }

  private BlockchainLedger initBlockChainLedger(
      long innerContractMintGas, long outerContractMintGas) {
    final BlockChainTestNetwork network = new BlockChainTestNetwork();
    RootDirectory rootDirectory = MemoryStorage.createRootDirectory(temporaryFolder.toFile());
    return register(
        BlockchainLedger.createForTest(
            rootDirectory,
            chainstate -> {
              StateHelper.initial(chainstate);
              byte[] jar = JarBuilder.buildJar(BlockchainLedgerTest.DeployContractInTest.class);
              BlockchainAddress deployPublicContract = TestObjects.CONTRACT_BOOTSTRAP;
              CoreContractStateTest.createContract(
                  chainstate, jar, deployPublicContract, JarClassLoaderTest.getSysBinderJar());
              FeePluginHelper.mintGas(chainstate, deployPublicContract, 1_000_000);
              CoreContractStateTest.createContract(
                  chainstate,
                  JarBuilder.buildJar(InnerContract.class),
                  INNER_CONTRACT_ADDRESS,
                  JarClassLoaderTest.getPublicBinderJar());
              FeePluginHelper.mintGas(chainstate, INNER_CONTRACT_ADDRESS, innerContractMintGas);
              CoreContractStateTest.createContract(
                  chainstate,
                  JarBuilder.buildJar(OuterContract.class),
                  OUTER_CONTRACT_ADDRESS,
                  JarClassLoaderTest.getPublicBinderJar());
              FeePluginHelper.mintGas(chainstate, OUTER_CONTRACT_ADDRESS, outerContractMintGas);
            },
            network,
            new DummyExecutableEventFinder()));
  }

  static TransactionCost getTotalRegisteredCostForTransaction(BlockchainLedger ledger, Hash root) {
    TransactionCost cost = ledger.getTransactionCost(root);
    if (cost == null) {
      cost = new TransactionCost(0, 0, 0, 0, Map.of());
    }
    return recursiveSum(ledger, root, cost);
  }

  private static TransactionCost recursiveSum(
      BlockchainLedger ledger, Hash root, TransactionCost cost) {
    BlockchainLedger.PossiblyFinalizedTransaction maybeFinalized =
        ledger.getExecutedTransaction(root);
    ExecutedTransaction executed = maybeFinalized.transaction();
    for (Hash event : executed.getEvents()) {
      if (!root.equals(event)) {
        TransactionCost that = getTotalRegisteredCostForTransaction(ledger, event);
        if (that != null) {
          cost = add(cost, that);
        }
      }
    }
    return cost;
  }

  /** Tests contract paying for outgoing events/callback. */
  public static final class OuterContract extends PubContract<StateVoid> {

    static final int PLEASE_DO_NOT_PAY = 1;
    static final int PLEASE_PAY_FOR_EVENT = 2;
    static final int PLEASE_PAY_FOR_CALLBACK = 3;
    static final int PLEASE_PAY_FOR_BOTH = 4;

    @Override
    public StateVoid onInvoke(
        PubContractContext context, StateVoid state, SafeDataInputStream rpc) {
      final int instruction = rpc.readInt();
      final EventManager eventManager = context.getRemoteCallsCreator();
      if (instruction == PLEASE_DO_NOT_PAY) {
        // Call the receiver contract with callback.
        // The sender pays for the call & callback.
        eventManager.registerCallback(safeDataOutputStream -> {}, CALLBACK_ALLOCATED_GAS);
        eventManager
            .invoke(INNER_CONTRACT_ADDRESS)
            .allocateCost(OUTGOING_EVENT_ALLOCATED_GAS)
            .withPayload(safeDataOutputStream -> {})
            .send();
      } else if (instruction == PLEASE_PAY_FOR_EVENT) {
        // Call the receiver contract with callback.
        // This contract pays for the call. The sender pays for the callback.
        eventManager.registerCallback(safeDataOutputStream -> {}, CALLBACK_ALLOCATED_GAS);
        eventManager
            .invoke(INNER_CONTRACT_ADDRESS)
            .allocateCostFromContract(OUTGOING_EVENT_ALLOCATED_GAS)
            .withPayload(safeDataOutputStream -> {})
            .send();
      } else if (instruction == PLEASE_PAY_FOR_CALLBACK) {
        // Call the receiver contract with callback.
        // The sender pays for the call. This contract pays for the callback.
        eventManager.registerCallbackWithCostFromContract(
            safeDataOutputStream -> {}, CALLBACK_ALLOCATED_GAS);
        eventManager
            .invoke(INNER_CONTRACT_ADDRESS)
            .allocateCost(OUTGOING_EVENT_ALLOCATED_GAS)
            .withPayload(safeDataOutputStream -> {})
            .send();
      } else if (instruction == PLEASE_PAY_FOR_BOTH) {
        // Call the receiver contract with callback.
        // This contract pays for the call & the callback.
        eventManager.registerCallbackWithCostFromContract(
            safeDataOutputStream -> {}, CALLBACK_ALLOCATED_GAS);
        eventManager
            .invoke(INNER_CONTRACT_ADDRESS)
            .allocateCostFromContract(OUTGOING_EVENT_ALLOCATED_GAS)
            .withPayload(safeDataOutputStream -> {})
            .send();
      }
      return state;
    }

    @Override
    public StateVoid onCallback(
        PubContractContext context,
        StateVoid state,
        CallbackContext callbackContext,
        SafeDataInputStream rpc) {
      final EventCreator eventManager = context.getInvocationCreator();
      eventManager
          .invoke(INNER_CONTRACT_ADDRESS)
          .allocateCost(OUTGOING_EVENT_FROM_CALLBACK_ALLOCATED_GAS)
          .withPayload(safeDataOutputStream -> {})
          .send();
      return state;
    }
  }

  /** Tests contract receiving an event paid by for the sending contract. */
  public static final class InnerContract extends PubContract<StateVoid> {

    @Override
    public StateVoid onInvoke(
        PubContractContext context, StateVoid state, SafeDataInputStream rpc) {
      return state;
    }
  }
}
