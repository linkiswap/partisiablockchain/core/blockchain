package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.TestObjects;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.crypto.Signature;
import com.secata.stream.SafeDataOutputStream;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class RootConsensusPluginTest {

  private final KeyPair signer = new KeyPair(BigInteger.TEN);
  private final RootConsensusPlugin plugin = new RootConsensusPlugin();
  private final Block block =
      new Block(
          System.currentTimeMillis(),
          1,
          0,
          TestObjects.EMPTY_HASH,
          TestObjects.EMPTY_HASH,
          List.of(),
          List.of());

  @Test
  public void validateFinalizedBlock() {
    Signature signature = signer.sign(block.identifier());
    byte[] validSignature = SafeDataOutputStream.serialize(signature::write);
    byte[] invalidSignature =
        SafeDataOutputStream.serialize(new Signature(3, signature.getR(), signature.getS()));
    SafeDataOutputStream.serialize(new KeyPair(BigInteger.TWO).sign(block.identifier())::write);
    Assertions.assertThat(
            plugin.validateLocalBlock(new FinalBlock(block, validSignature)).isAccepted())
        .isTrue();
    Assertions.assertThat(
            plugin.validateLocalBlock(new FinalBlock(block, invalidSignature)).isAccepted())
        .isFalse();
    Assertions.assertThat(
            plugin
                .validateLocalBlock(new FinalBlock(block, Arrays.copyOf(validSignature, 66)))
                .isAccepted())
        .isFalse();
  }
}
