package com.partisiablockchain.blockchain.genesis;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.Hash;
import com.secata.stream.DataStreamSerializable;
import com.secata.stream.SafeDataInputStream;
import com.secata.tools.coverage.FunctionUtility;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Tests. */
public final class GenesisStorageTest {

  @Test
  public void readWrite() {
    String value = "My string";
    DataStreamSerializable serialize = s -> s.writeString(value);
    Hash id = Hash.create(serialize);
    GenesisStorage genesisStorage = new GenesisStorage();
    Assertions.assertThat(genesisStorage.read(id, SafeDataInputStream::readString)).isNull();
    Assertions.assertThat(genesisStorage.write(id, serialize)).isTrue();
    Assertions.assertThat(genesisStorage.read(id, SafeDataInputStream::readString))
        .isEqualTo(value);
    Assertions.assertThat(genesisStorage.write(id, FunctionUtility.noOpConsumer())).isFalse();
    Assertions.assertThat(genesisStorage.read(id, SafeDataInputStream::readString))
        .isEqualTo(value);
  }
}
