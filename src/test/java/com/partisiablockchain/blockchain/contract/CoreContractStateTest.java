package com.partisiablockchain.blockchain.contract;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.TestObjects;
import com.partisiablockchain.binder.BinderEvent;
import com.partisiablockchain.blockchain.MutableChainState;
import com.partisiablockchain.blockchain.contract.CoreContractState.ContractSerialization;
import com.partisiablockchain.blockchain.contract.binder.BlockchainContract;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.flooding.ObjectCreator;
import com.partisiablockchain.serialization.StateSerializable;
import java.util.function.Function;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class CoreContractStateTest {

  /**
   * Creates a contract serialization.
   *
   * @param builder mutable chain state
   * @param handlerInfo the contract jar
   * @param address the address of the contract
   * @param binderInfo the binder jar
   * @param <T> the state type of the underlying contract
   * @return the serialization
   */
  public static <T extends StateSerializable> ContractSerialization<T> createContract(
      MutableChainState builder, byte[] handlerInfo, BlockchainAddress address, byte[] binderInfo) {
    return createContract(builder, handlerInfo, address, binderInfo, null);
  }

  /**
   * Creates a contract serialization.
   *
   * @param builder mutable chain state
   * @param handlerInfo the contract jar
   * @param address the address of the contract
   * @param binderInfo the binder jar
   * @param onCreate create function for the contract state
   * @param <T> the state type of the underlying contract
   * @return the serialization
   */
  public static <T extends StateSerializable, U extends BinderEvent>
      ContractSerialization<T> createContract(
          MutableChainState builder,
          byte[] handlerInfo,
          BlockchainAddress address,
          byte[] binderInfo,
          Function<BlockchainContract<T, U>, T> onCreate) {
    return createContractPrivate(builder, handlerInfo, address, binderInfo, onCreate);
  }

  private static <T extends StateSerializable, U extends BinderEvent>
      ContractSerialization<T> createContractPrivate(
          MutableChainState builder,
          byte[] contractJar,
          BlockchainAddress address,
          byte[] binderJar,
          Function<BlockchainContract<T, U>, T> onCreate) {
    CoreContractState core =
        create(builder.saveJar(binderJar), builder.saveJar(contractJar), contractJar.length);
    BlockchainContract<T, U> binder = builder.createContract(address, core);
    StateSerializable state = onCreate != null ? onCreate.apply(binder) : null;
    builder.setContractState(address, state);
    return binder.getContractSerialization();
  }

  /**
   * Create a new CoreContractState.
   *
   * @param binderJarIdentifier hash of the saved the binder jar
   * @param contractJarIdentifier hash of the saved the contract jar
   * @param codeSize size of the code in this contract, used for pricing
   * @return the created object
   */
  public static CoreContractState create(
      Hash binderJarIdentifier, Hash contractJarIdentifier, int codeSize) {
    Hash emptyAbiIdentifier = Hash.create(o -> o.writeDynamicBytes(new byte[0]));
    return CoreContractState.create(
        binderJarIdentifier, contractJarIdentifier, emptyAbiIdentifier, codeSize);
  }

  @Test
  public void getters() {
    CoreContractState core =
        CoreContractState.create(
            TestObjects.EMPTY_HASH, TestObjects.EMPTY_HASH, TestObjects.EMPTY_HASH, 0);
    Assertions.assertThat(core.getBinderIdentifier()).isEqualTo(TestObjects.EMPTY_HASH);
    Assertions.assertThat(core.getAbiIdentifier()).isEqualTo(TestObjects.EMPTY_HASH);
    Assertions.assertThat(core.getContractIdentifier()).isEqualTo(TestObjects.EMPTY_HASH);
  }

  @Test
  public void illegalContractSerialization() {
    ContractSerialization<FirstState> serialization = new ContractSerialization<>(FirstState.class);

    Assertions.assertThatThrownBy(
            () ->
                serialization.write(
                    ObjectCreator.createMemoryStoredSerializer(), new SecondState()))
        .isInstanceOf(IllegalStateException.class)
        .hasMessage(
            "Trying to save contract of type "
                + FirstState.class
                + " but received contract of type "
                + SecondState.class);
  }

  /** Test state. */
  @Immutable
  public static final class FirstState implements StateSerializable {

    FirstState() {}
  }

  /** Test state. */
  @Immutable
  public static final class SecondState implements StateSerializable {

    SecondState() {}
  }
}
