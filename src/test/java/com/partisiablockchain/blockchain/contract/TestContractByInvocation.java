package com.partisiablockchain.blockchain.contract;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.contract.pub.PubContract;
import com.partisiablockchain.contract.pub.PubContractContext;
import com.partisiablockchain.serialization.StateSerializable;
import com.secata.stream.SafeDataInputStream;

/** Test. */
public final class TestContractByInvocation
    extends PubContract<TestContractByInvocation.TestState> {

  /** Test. */
  @Immutable
  public static final class TestState implements StateSerializable {

    public final int state;

    @SuppressWarnings("unused")
    TestState() {
      state = 0;
    }

    TestState(int nextState) {
      state = nextState;
    }
  }

  @Override
  public TestState onInvoke(PubContractContext context, TestState state, SafeDataInputStream rpc) {
    int nextState = rpc.readInt();
    long blockProductionTime = rpc.readLong();
    int infoState = state == null ? 0 : state.state;
    if (infoState + 1 != nextState || context.getBlockProductionTime() != blockProductionTime) {
      throw new RuntimeException("Should not happen - only here for usage of state");
    }
    return new TestState(nextState);
  }
}
