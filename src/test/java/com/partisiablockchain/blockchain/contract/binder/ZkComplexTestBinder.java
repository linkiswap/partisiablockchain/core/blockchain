package com.partisiablockchain.blockchain.contract.binder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.binder.BinderInteraction;
import com.partisiablockchain.binder.BinderResult;
import com.partisiablockchain.binder.CallResult;
import com.partisiablockchain.binder.zk.ZkBinderContext;
import com.partisiablockchain.binder.zk.ZkBinderContract;
import com.partisiablockchain.blockchain.contract.StateComplex;
import com.partisiablockchain.contract.CallbackContext;
import com.partisiablockchain.contract.zk.ZkClosed;
import com.partisiablockchain.contract.zk.ZkContract;
import com.partisiablockchain.serialization.StateVoid;
import com.secata.stream.SafeDataInputStream;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;
import java.util.function.Supplier;

/** Test. */
public final class ZkComplexTestBinder
    implements ZkBinderContract<StateComplex<Integer, String, Long>> {

  private final ZkContract<
          StateComplex<Integer, String, Long>, StateVoid, ZkClosed<StateVoid>, Supplier<String>>
      contract;

  public ZkComplexTestBinder(
      ZkContract<
              StateComplex<Integer, String, Long>, StateVoid, ZkClosed<StateVoid>, Supplier<String>>
          contract) {
    this.contract = contract;
  }

  @SuppressWarnings("unchecked")
  @Override
  public Class<StateComplex<Integer, String, Long>> getStateClass() {
    ParameterizedType genericSuperClass =
        (ParameterizedType) contract.getClass().getGenericSuperclass();
    Type type = genericSuperClass.getActualTypeArguments()[0];
    return (Class<StateComplex<Integer, String, Long>>) ((ParameterizedType) type).getRawType();
  }

  @Override
  public List<Class<?>> getStateClassTypeParameters() {
    return List.of(Integer.class, String.class, Long.class);
  }

  @Override
  public ZkContract<?, ?, ?, ?> getContract() {
    return contract;
  }

  @Override
  public BinderResult<StateComplex<Integer, String, Long>, BinderInteraction> create(
      ZkBinderContext context, byte[] rpc) {
    return result(contract.onCreate(null, null, SafeDataInputStream.createFromBytes(rpc)));
  }

  @Override
  public BinderResult<StateComplex<Integer, String, Long>, BinderInteraction> invoke(
      ZkBinderContext context, StateComplex<Integer, String, Long> state, byte[] rpc) {
    return result(
        contract.onOpenInput(null, null, state, SafeDataInputStream.createFromBytes(rpc)));
  }

  @Override
  public BinderResult<StateComplex<Integer, String, Long>, BinderInteraction> callback(
      ZkBinderContext context,
      StateComplex<Integer, String, Long> state,
      CallbackContext callbackContext,
      byte[] rpc) {
    return result(
        contract.onCallback(
            null, null, state, callbackContext, SafeDataInputStream.createFromBytes(rpc)));
  }

  private BinderResult<StateComplex<Integer, String, Long>, BinderInteraction> result(
      StateComplex<Integer, String, Long> state) {
    return new CommonBinderResult<>(state);
  }

  static final class CommonBinderResult<StateT> implements BinderResult<StateT, BinderInteraction> {

    private final StateT state;

    CommonBinderResult(StateT state) {
      this.state = state;
    }

    @Override
    public StateT getState() {
      return state;
    }

    @Override
    public List<BinderInteraction> getInvocations() {
      return List.of();
    }

    @Override
    public CallResult getCallResult() {
      return null;
    }
  }
}
