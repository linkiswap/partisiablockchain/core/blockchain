package com.partisiablockchain.blockchain.contract.binder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.TestObjects;
import com.partisiablockchain.blockchain.MutableChainState;
import com.partisiablockchain.blockchain.StateHelper;
import com.partisiablockchain.blockchain.transaction.AccountPluginForTest;
import com.partisiablockchain.blockchain.transaction.ExecutionContextTransaction;
import com.partisiablockchain.blockchain.transaction.ExecutionContextTransactionTest;
import com.partisiablockchain.blockchain.transaction.ExecutionEventManager;
import com.partisiablockchain.blockchain.transaction.FeeCollector;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.Mockito;

/** ContextTest. */
public abstract class ContextTest<T> {

  protected T context;
  protected FeeCollector feeCollector;
  protected ExecutionContextTransaction executionContext;
  protected BlockchainContractContext contractContext;

  /** Setup context with mocks. */
  @BeforeEach
  public void before() {
    contractContext = Mockito.mock(BlockchainContractContext.class);
    Mockito.when(contractContext.getContractAddress()).thenReturn(TestObjects.CONTRACT_PUB1);

    MutableChainState state = StateHelper.initialWithAdditions(builder -> {});

    feeCollector = FeeCollector.create(new AccountPluginForTest(1), 100);
    ExecutionEventManager eventManager = Mockito.mock(ExecutionEventManager.class);
    executionContext =
        ExecutionContextTransactionTest.create(
            "ChainId", state, TestObjects.ACCOUNT_ONE, 77, 77, eventManager, feeCollector);
  }
}
