package com.partisiablockchain.blockchain.contract.binder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.TestObjects;
import com.partisiablockchain.blockchain.MutableChainState;
import com.partisiablockchain.blockchain.transaction.EventCollector;
import com.partisiablockchain.blockchain.transaction.ExecutionContextTransaction;
import com.partisiablockchain.blockchain.transaction.ExecutionContextTransactionTest;
import com.partisiablockchain.blockchain.transaction.ExecutionEventManager;
import com.partisiablockchain.blockchain.transaction.FeeCollector;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

/** Test. */
public final class BinderContextImplTest {

  private BinderContextImpl binder;
  private ExecutionContextTransaction executionContext;

  /** Setup. */
  @BeforeEach
  public void setUp() {
    MutableChainState chainState = Mockito.mock(MutableChainState.class);
    FeeCollector feeCollector = FeeCollector.create(null, 0L);
    ExecutionEventManager eventManager =
        new ExecutionEventManager(
            () ->
                new EventCollector(
                    null, chainState, 1L, 1L, TestObjects.EMPTY_HASH, 0, null, feeCollector));

    executionContext =
        ExecutionContextTransactionTest.create(
            "ChainId", chainState, TestObjects.ACCOUNT_ONE, 77, 77, eventManager, 100);

    BlockchainContractContext contractContext =
        new BlockchainContractContextImpl(TestObjects.CONTRACT_SYS);

    binder = new PubBinderContextImpl(contractContext, executionContext);
  }

  @Test
  public void getContractAddress() {
    Assertions.assertThat(binder.getContractAddress()).isEqualTo(TestObjects.CONTRACT_SYS);
  }

  @Test
  public void getBlockTime() {
    Assertions.assertThat(binder.getBlockTime()).isEqualTo(executionContext.getBlockTime());
  }

  @Test
  public void getBlockProductionTime() {
    Assertions.assertThat(binder.getBlockProductionTime())
        .isEqualTo(executionContext.getBlockProductionTime());
  }

  @Test
  public void getFrom() {
    Assertions.assertThat(binder.getFrom()).isEqualTo(executionContext.getFrom());
  }

  @Test
  public void getTransactionHash() {
    Assertions.assertThat(binder.getCurrentTransactionHash())
        .isEqualTo(executionContext.getTransactionHash());
    Assertions.assertThat(binder.getOriginalTransactionHash())
        .isEqualTo(executionContext.getOriginatingTransactionHash());
  }
}
