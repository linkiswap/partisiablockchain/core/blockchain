package com.partisiablockchain.blockchain.contract.binder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.TestObjects;
import com.partisiablockchain.binder.sys.SystemInteraction;
import com.partisiablockchain.blockchain.AccountPluginAccessor;
import com.partisiablockchain.blockchain.ChainPluginType;
import com.partisiablockchain.blockchain.ConsensusPluginAccessor;
import com.partisiablockchain.blockchain.JarClassLoaderTest;
import com.partisiablockchain.blockchain.MutableChainState;
import com.partisiablockchain.blockchain.RoutingPluginAccessor;
import com.partisiablockchain.blockchain.ShardRoute;
import com.partisiablockchain.blockchain.contract.CallbackToContract;
import com.partisiablockchain.blockchain.contract.CoreContractState;
import com.partisiablockchain.blockchain.contract.CoreContractStateTest;
import com.partisiablockchain.blockchain.transaction.AccountPluginForTest;
import com.partisiablockchain.blockchain.transaction.EventCollector;
import com.partisiablockchain.blockchain.transaction.EventCollectorUtil;
import com.partisiablockchain.blockchain.transaction.EventSender;
import com.partisiablockchain.blockchain.transaction.EventTransaction;
import com.partisiablockchain.blockchain.transaction.ExecutableEvent;
import com.partisiablockchain.blockchain.transaction.ExecutionContextTransaction;
import com.partisiablockchain.blockchain.transaction.ExecutionContextTransactionTest;
import com.partisiablockchain.blockchain.transaction.ExecutionEventManager;
import com.partisiablockchain.blockchain.transaction.FeeCollector;
import com.partisiablockchain.blockchain.transaction.InnerSystemEvent;
import com.partisiablockchain.blockchain.transaction.InnerSystemEvent.SetFeatureEvent;
import com.partisiablockchain.blockchain.transaction.PendingByocFee;
import com.partisiablockchain.blockchain.transaction.PendingFee;
import com.partisiablockchain.blockchain.transaction.ReturnEnvelope;
import com.partisiablockchain.contract.sys.GlobalPluginStateUpdate;
import com.partisiablockchain.contract.sys.LocalPluginStateUpdate;
import com.partisiablockchain.contract.sys.PluginInteractionCreator;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.serialization.StateSerializable;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.coverage.FunctionUtility;
import com.secata.tools.immutable.FixedList;
import java.util.List;
import java.util.Set;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

/** Test for {@link SysBinderContextImpl}. */
public final class SysBinderContextImplTest {

  private SysBinderContextImpl binder;
  private MutableChainState state;
  private final StateSerializable globalAccountPluginState = Mockito.mock(StateSerializable.class);
  private final StateSerializable globalConsensusPluginState =
      Mockito.mock(StateSerializable.class);
  private AccountPluginAccessor accountPluginAccessor = new AccountPluginForTest(1);
  private final ConsensusPluginAccessor consensusPluginAccessor =
      Mockito.mock(ConsensusPluginAccessor.class);
  private final RoutingPluginAccessor routingPluginAccessor =
      Mockito.mock(RoutingPluginAccessor.class);
  private final PluginInteractionCreator accountInteractions =
      Mockito.mock(PluginInteractionCreator.class);
  private final PluginInteractionCreator consensusInteractions =
      Mockito.mock(PluginInteractionCreator.class);
  private final PluginInteractionCreator routingInteractions =
      Mockito.mock(PluginInteractionCreator.class);
  private ExecutionEventManager eventManager;
  private FeeCollector feeCollector;
  private ExecutionContextTransaction executionContext;

  /** Setup. */
  @BeforeEach
  public void setUp() {
    state = Mockito.mock(MutableChainState.class);
    Mockito.when(state.getPluginInteractions(ChainPluginType.ACCOUNT))
        .thenReturn(accountInteractions);
    Mockito.when(state.getPluginInteractions(ChainPluginType.CONSENSUS))
        .thenReturn(consensusInteractions);
    Mockito.when(state.getPluginInteractions(ChainPluginType.ROUTING))
        .thenReturn(routingInteractions);
    Mockito.when(state.getAccountPlugin()).thenReturn(accountPluginAccessor);
    Mockito.when(state.getGlobalPluginState(ChainPluginType.ACCOUNT))
        .thenReturn(globalAccountPluginState);
    Mockito.when(state.getConsensusPlugin()).thenReturn(consensusPluginAccessor);
    Mockito.when(state.getGlobalPluginState(ChainPluginType.CONSENSUS))
        .thenReturn(globalConsensusPluginState);
    Mockito.when(state.getRoutingPlugin()).thenReturn(routingPluginAccessor);
    Mockito.when(state.getFeature("FEATURE")).thenReturn("FEATURE_VALUE");
    CoreContractState coreContractState =
        CoreContractStateTest.create(TestObjects.EMPTY_HASH, TestObjects.EMPTY_HASH, 1337);
    Mockito.when(state.getCoreContractState(TestObjects.CONTRACT_SYS))
        .thenReturn(coreContractState);

    feeCollector = FeeCollector.create(accountPluginAccessor, Integer.MAX_VALUE);
    createBinder(state);
  }

  private void createBinder(MutableChainState state) {
    eventManager =
        new ExecutionEventManager(
            () ->
                new EventCollector(
                    null,
                    state,
                    1L,
                    1L,
                    TestObjects.EMPTY_HASH,
                    0,
                    TestObjects.CONTRACT_SYS,
                    feeCollector));

    executionContext =
        ExecutionContextTransactionTest.create(
            "ChainId", this.state, TestObjects.ACCOUNT_ONE, 77, 77, eventManager, feeCollector);

    BlockchainContractContext contractContext =
        new BlockchainContractContextImpl(TestObjects.CONTRACT_SYS);

    binder = new SysBinderContextImpl(contractContext, executionContext);
  }

  @Test
  public void createAccount() {
    Mockito.when(state.routeToShard(TestObjects.ACCOUNT_ONE)).thenReturn(new ShardRoute(null, 0));
    BlockchainAddress account = TestObjects.ACCOUNT_ONE;
    withEventCollector(new SystemInteraction.CreateAccount(account));
    executeEvent(0, InnerSystemEvent.EMPTY_RETURN_VALUE);
    Mockito.verify(state).createAccount(account);
  }

  private void withEventCollector(SystemInteraction systemInteraction) {
    EventCollector collector = eventManager.createCollector();
    EventCollectorUtil.handle(systemInteraction, collector);
  }

  @Test
  public void createShard() {
    Mockito.when(state.routeToShard((String) null)).thenReturn(new ShardRoute(null, 0));
    String shardId = TestObjects.SHARD_ONE;
    withEventCollector(new SystemInteraction.CreateShard(shardId));
    executeEvent(0, InnerSystemEvent.EMPTY_RETURN_VALUE);
    Mockito.verify(state).addActiveShard(Mockito.any(), Mockito.eq(null), Mockito.eq(shardId));
  }

  @Test
  public void removeShard() {
    Mockito.when(state.routeToShard((String) null)).thenReturn(new ShardRoute(null, 0));
    String shardId = TestObjects.SHARD_ONE;
    withEventCollector(new SystemInteraction.RemoveShard(shardId));
    executeEvent(0, InnerSystemEvent.EMPTY_RETURN_VALUE);
    Mockito.verify(state).removeActiveShard(Mockito.any(), Mockito.eq(null), Mockito.eq(shardId));
  }

  @Test
  public void getGovernance() {
    Assertions.assertThat(binder.getGovernance().getChainId()).isEqualTo("ChainId");
  }

  @Test
  public void getAccountPlugin() {
    Assertions.assertThat(binder.getGlobalAccountPluginState()).isEqualTo(globalAccountPluginState);
  }

  @Test
  public void removeContract() {
    BlockchainAddress contract = TestObjects.CONTRACT_PUB1;
    Mockito.when(state.routeToShard(contract)).thenReturn(new ShardRoute(null, 0));
    withEventCollector(new SystemInteraction.RemoveContract(contract));
    executeEvent(0, InnerSystemEvent.EMPTY_RETURN_VALUE);
    Mockito.verify(state).removeContract(contract, 0);
  }

  @Test
  public void updateAccountPluginState() {
    byte[] invocation = new byte[2];
    LocalPluginStateUpdate localUpdate =
        LocalPluginStateUpdate.create(TestObjects.ACCOUNT_FOUR, invocation);
    Mockito.when(state.routeToShard(TestObjects.ACCOUNT_FOUR))
        .thenReturn(new ShardRoute(null, 10L));
    withEventCollector(new SystemInteraction.UpdateLocalAccountPluginState(localUpdate));
    executeEvent(0, null);
    Mockito.verify(state)
        .updateLocalPluginState(ChainPluginType.ACCOUNT, TestObjects.ACCOUNT_FOUR, invocation, 0);
    GlobalPluginStateUpdate globalUpdate = GlobalPluginStateUpdate.create(invocation);
    Mockito.when(state.routeToShard((String) null)).thenReturn(new ShardRoute(null, 0));
    withEventCollector(new SystemInteraction.UpdateGlobalAccountPluginState(globalUpdate));
    executeEvent(1, null);
    Mockito.verify(state).updateGlobalPluginState(0, ChainPluginType.ACCOUNT, invocation);
  }

  @Test
  public void updateContextFreeAccountPluginState() {
    byte[] invocation = new byte[2];
    Mockito.when(state.routeToShard(TestObjects.SHARD_ONE)).thenReturn(new ShardRoute(null, 10L));
    withEventCollector(
        new SystemInteraction.UpdateContextFreeAccountPluginState(
            TestObjects.SHARD_ONE, invocation));
    executeEvent(0, null);
    Mockito.verify(state).updateLocalPluginState(ChainPluginType.ACCOUNT, invocation, 0);
  }

  @Test
  public void updateAccountPlugin() {
    byte[] newJar = new byte[12];
    byte[] rpc = new byte[3];
    Mockito.when(state.routeToShard((String) null)).thenReturn(new ShardRoute(null, 0));
    withEventCollector(new SystemInteraction.UpdateAccountPlugin(newJar, rpc));
    executeEvent(0, InnerSystemEvent.EMPTY_RETURN_VALUE);
    Mockito.verify(state)
        .setPlugin(FunctionUtility.noOpBiConsumer(), null, ChainPluginType.ACCOUNT, newJar, rpc);
  }

  @Test
  public void getPluginInteractions() {
    Assertions.assertThat(binder.getAccountPluginInteractions()).isEqualTo(accountInteractions);
    Assertions.assertThat(binder.getConsensusPluginInteractions()).isEqualTo(consensusInteractions);
  }

  @Test
  public void exists() {
    Mockito.when(state.routeToShard(TestObjects.ACCOUNT_ONE)).thenReturn(new ShardRoute(null, 0));
    withEventCollector(new SystemInteraction.Exists(TestObjects.ACCOUNT_ONE));
    executeEvent(0, SafeDataOutputStream.serialize(stream -> stream.writeBoolean(false)));
    Mockito.verify(state).existsAccounts(TestObjects.ACCOUNT_ONE);
    Mockito.verify(state).getContracts();
  }

  private void executeEvent(int index, byte[] expectedReturnValue) {
    List<ExecutableEvent> transactions =
        binder.getExecutionContext().getExecutionEventManager().getTransactions();
    Assertions.assertThat(transactions).hasSize(index + 1);
    EventTransaction.InnerEvent inner = transactions.get(index).getEvent().getInner();
    Assertions.assertThat(inner).isInstanceOf(InnerSystemEvent.class);
    byte[] returnValue =
        ((InnerSystemEvent) inner)
            .execute(
                null,
                FunctionUtility.noOpBiConsumer(),
                (contract, callback) -> {
                  ShardRoute shardRoute = new ShardRoute(null, 0L);

                  CallbackToContract callbackToContract =
                      new CallbackToContract(contract, callback);
                  EventTransaction event =
                      EventTransaction.create(
                          TestObjects.EMPTY_HASH, callbackToContract, shardRoute, 123, 1L, 1234);
                  ExecutableEvent executableEvent = new ExecutableEvent("subChainId", event);
                  transactions.add(executableEvent);
                },
                state,
                0);
    Assertions.assertThat(returnValue).isEqualTo(expectedReturnValue);
  }

  @Test
  public void getConsensusPlugin() {
    Assertions.assertThat(binder.getGlobalConsensusPluginState())
        .isEqualTo(globalConsensusPluginState);
  }

  @Test
  public void updateLocalConsensusPluginState() {
    byte[] invocation = new byte[2];
    LocalPluginStateUpdate localUpdate =
        LocalPluginStateUpdate.create(TestObjects.ACCOUNT_FOUR, invocation);
    Mockito.when(state.routeToShard(TestObjects.ACCOUNT_FOUR))
        .thenReturn(new ShardRoute(null, 10L));
    withEventCollector(new SystemInteraction.UpdateLocalConsensusPluginState(localUpdate));
    executeEvent(0, null);

    Mockito.when(state.routeToShard((String) null)).thenReturn(new ShardRoute(null, 0));
    GlobalPluginStateUpdate globalUpdate = GlobalPluginStateUpdate.create(invocation);
    withEventCollector(new SystemInteraction.UpdateGlobalConsensusPluginState(globalUpdate));
    executeEvent(1, null);
    Mockito.verify(state).updateGlobalPluginState(0, ChainPluginType.CONSENSUS, invocation);
  }

  @Test
  public void updateConsensusPlugin() {
    Mockito.when(state.routeToShard((String) null)).thenReturn(new ShardRoute(null, 0));
    byte[] newJar = new byte[12];
    byte[] invocation = new byte[4];
    withEventCollector(new SystemInteraction.UpdateConsensusPlugin(newJar, invocation));
    executeEvent(0, InnerSystemEvent.EMPTY_RETURN_VALUE);
    Mockito.verify(state)
        .setPlugin(
            FunctionUtility.noOpBiConsumer(), null, ChainPluginType.CONSENSUS, newJar, invocation);
  }

  @Test
  public void updateRoutingPlugin() {
    Mockito.when(state.routeToShard((String) null)).thenReturn(new ShardRoute(null, 0));
    byte[] newJar = new byte[12];
    byte[] invocation = new byte[4];
    withEventCollector(new SystemInteraction.UpdateRoutingPlugin(newJar, invocation));
    executeEvent(0, InnerSystemEvent.EMPTY_RETURN_VALUE);
    Mockito.verify(state)
        .setPlugin(
            FunctionUtility.noOpBiConsumer(), null, ChainPluginType.ROUTING, newJar, invocation);
  }

  @Test
  public void getFeature() {
    Assertions.assertThat(binder.getFeature("FEATURE")).isEqualTo("FEATURE_VALUE");
    Assertions.assertThat(binder.getFeature("NO_FEATURE")).isNull();
  }

  @Test
  public void setFeature() {
    Mockito.when(state.routeToShard((String) null)).thenReturn(new ShardRoute(null, 0));
    withEventCollector(new SystemInteraction.SetFeature("NEXT_FEATURE", "TRUE"));
    executeEvent(0, InnerSystemEvent.EMPTY_RETURN_VALUE);
    Mockito.verify(state).setFeature("NEXT_FEATURE", "TRUE");
  }

  @Test
  public void eventManager() {
    accountPluginAccessor = new AccountPluginForTest(0);

    Mockito.when(state.routeToShard(TestObjects.CONTRACT_PUB1))
        .thenReturn(new ShardRoute(null, 10L));
    EventCollector collector1 = eventManager.createCollector();
    EventSender eventHandler1 =
        new EventSender(executionContext.getFrom(), collector1.getCurrentContract());
    EventCollectorUtil.interaction(
        TestObjects.CONTRACT_PUB1, new byte[0], true, 27, collector1, eventHandler1);
    Assertions.assertThat(eventManager.getTransactions()).hasSize(1);
    EventCollector collector = eventManager.createCollector();
    EventSender eventHandler =
        new EventSender(executionContext.getFrom(), collector.getCurrentContract());
    EventCollectorUtil.interaction(
        TestObjects.CONTRACT_PUB1, new byte[0], true, 26, collector, eventHandler);
    collector.setCallback(0L, false);
    Assertions.assertThat(collector1.getTransactions()).hasSize(1);
    Assertions.assertThat(collector.getTransactions()).hasSize(1);
  }

  @Test
  public void setFeatureOnShardShouldBeSentToMaster() {
    Mockito.when(state.routeToShard((String) null)).thenReturn(new ShardRoute(null, 10L));
    withEventCollector(new SystemInteraction.SetFeature("Feature", "Val"));
    Mockito.verify(state, Mockito.never()).setFeature(Mockito.any(), Mockito.any());
    EventTransaction event = validateEvent();
    Assertions.assertThat(event.getInner()).isInstanceOf(SetFeatureEvent.class);
  }

  @Test
  public void upgradeSystemContract() {
    Mockito.when(state.routeToShard(TestObjects.CONTRACT_PUB1))
        .thenReturn(new ShardRoute(null, 10L));
    withEventCollector(
        new SystemInteraction.UpgradeSystemContract(
            new byte[1], new byte[2], new byte[3], new byte[4], TestObjects.CONTRACT_PUB1));
    Mockito.verify(state, Mockito.never()).setFeature(Mockito.any(), Mockito.any());
    EventTransaction event = validateEvent();
    Assertions.assertThat(event.getInner())
        .isInstanceOf(InnerSystemEvent.UpgradeSystemContractEvent.class);
  }

  private EventTransaction validateEvent() {
    List<ExecutableEvent> transactions = eventManager.getTransactions();
    Assertions.assertThat(transactions).hasSize(1);
    ExecutableEvent executableEvent = transactions.get(0);
    EventTransaction event = executableEvent.getEvent();
    Assertions.assertThat(event.getDestinationShard()).isNull();
    Assertions.assertThat(event.getNonce()).isEqualTo(10L);
    return event;
  }

  @Test
  public void createContractOnMaster() {
    accountPluginAccessor = new AccountPluginForTest(0);
    setUp();

    Mockito.when(state.routeToShard(TestObjects.CONTRACT_PUB1)).thenReturn(new ShardRoute(null, 0));
    BlockchainAddress blockchainAddress = TestObjects.CONTRACT_PUB1;
    Mockito.when(state.routeToShard(blockchainAddress)).thenReturn(new ShardRoute(null, 10L));
    EventCollector collector = eventManager.createCollector();
    EventCollectorUtil.deploy(
        TestObjects.CONTRACT_PUB1,
        JarClassLoaderTest.getPublicBinderJar(),
        new byte[0],
        new byte[0],
        new byte[0],
        true,
        12235,
        collector,
        new EventSender(executionContext.getFrom(), executionContext.getFrom()));

    validateEvent();
  }

  @Test
  public void returnValueCallback() {
    byte[] testBytes = SafeDataOutputStream.serialize(stream -> stream.writeInt(123));
    InnerSystemEvent.CallbackEvent event =
        new InnerSystemEvent.CallbackEvent(
            new ReturnEnvelope(TestObjects.CONTRACT_PUB1),
            Hash.create(stream -> stream.writeInt(123)),
            true,
            testBytes);

    byte[] returnValue =
        event.execute(null, FunctionUtility.noOpBiConsumer(), (contract, callback) -> {}, state, 0);
    Assertions.assertThat(SafeDataInputStream.createFromBytes(returnValue).readInt())
        .isEqualTo(123);
  }

  @Test
  public void checkExistenceContracts() {
    Set<BlockchainAddress> contracts = Set.of(TestObjects.CONTRACT_PUB1);
    Mockito.when(state.getContracts()).thenReturn(contracts);

    InnerSystemEvent.CheckExistenceEvent existingContract =
        new InnerSystemEvent.CheckExistenceEvent(TestObjects.CONTRACT_PUB1);
    byte[] returnValue =
        existingContract.execute(
            null, FunctionUtility.noOpBiConsumer(), (contract, callback) -> {}, state, 0);
    Assertions.assertThat(SafeDataInputStream.createFromBytes(returnValue).readBoolean()).isTrue();

    InnerSystemEvent.CheckExistenceEvent nonExistingContract =
        new InnerSystemEvent.CheckExistenceEvent(TestObjects.CONTRACT_PUB2);
    returnValue =
        nonExistingContract.execute(
            null, FunctionUtility.noOpBiConsumer(), (contract, callback) -> {}, state, 0);
    Assertions.assertThat(SafeDataInputStream.createFromBytes(returnValue).readBoolean()).isFalse();
  }

  @Test
  public void checkExistenceAccounts() {
    Mockito.when(state.existsAccounts(TestObjects.ACCOUNT_ONE)).thenReturn(true);

    InnerSystemEvent.CheckExistenceEvent existingAccount =
        new InnerSystemEvent.CheckExistenceEvent(TestObjects.ACCOUNT_ONE);
    byte[] returnValue =
        existingAccount.execute(
            null, FunctionUtility.noOpBiConsumer(), (contract, callback) -> {}, state, 0);
    Assertions.assertThat(SafeDataInputStream.createFromBytes(returnValue).readBoolean()).isTrue();

    InnerSystemEvent.CheckExistenceEvent nonExistingAccount =
        new InnerSystemEvent.CheckExistenceEvent(TestObjects.ACCOUNT_TWO);
    returnValue =
        nonExistingAccount.execute(
            null, FunctionUtility.noOpBiConsumer(), (contract, callback) -> {}, state, 0);
    Assertions.assertThat(SafeDataInputStream.createFromBytes(returnValue).readBoolean()).isFalse();
  }

  @Test
  public void payServiceFees() {
    FixedList<BlockchainAddress> targets =
        FixedList.create(
            List.of(TestObjects.ACCOUNT_ONE, TestObjects.ACCOUNT_THREE, TestObjects.ACCOUNT_FOUR));
    FixedList<Integer> weights = FixedList.create(List.of(1, 2, 2));
    binder.payServiceFees(250_000L, targets, weights);
    List<PendingFee> pendingServiceFees = binder.getExecutionContext().getPendingServiceFees();
    Assertions.assertThat(pendingServiceFees)
        .usingRecursiveComparison()
        .isEqualTo(
            List.of(
                PendingFee.create(TestObjects.ACCOUNT_ONE, 50_000L),
                PendingFee.create(TestObjects.ACCOUNT_THREE, 100_000L),
                PendingFee.create(TestObjects.ACCOUNT_FOUR, 100_000L)));
  }

  @Test
  public void payInfrastructureFees() {
    FixedList<BlockchainAddress> targets =
        FixedList.create(
            List.of(TestObjects.ACCOUNT_ONE, TestObjects.ACCOUNT_TWO, TestObjects.ACCOUNT_THREE));
    FixedList<Integer> weights = FixedList.create(List.of(1, 2, 2));
    binder.payInfrastructureFees(50_000, targets, weights);
    List<PendingFee> pendingInfrastructureFees =
        binder.getExecutionContext().getPendingInfrastructureFees();
    Assertions.assertThat(pendingInfrastructureFees)
        .usingRecursiveComparison()
        .isEqualTo(
            List.of(
                PendingFee.create(TestObjects.ACCOUNT_ONE, 10_000L),
                PendingFee.create(TestObjects.ACCOUNT_TWO, 20_000L),
                PendingFee.create(TestObjects.ACCOUNT_THREE, 20_000L)));

    binder.payInfrastructureFees(50_000, targets, weights);
    pendingInfrastructureFees = binder.getExecutionContext().getPendingInfrastructureFees();
    Assertions.assertThat(pendingInfrastructureFees)
        .usingRecursiveComparison()
        .isEqualTo(
            List.of(
                PendingFee.create(TestObjects.ACCOUNT_ONE, 20_000L),
                PendingFee.create(TestObjects.ACCOUNT_TWO, 40_000L),
                PendingFee.create(TestObjects.ACCOUNT_THREE, 40_000L)));
  }

  @Test
  public void cpuFee() {
    Assertions.assertThat(feeCollector.sum()).isEqualTo(0);
    binder.registerCpuFee(13);
    Assertions.assertThat(feeCollector.sum()).isEqualTo(13);
    Assertions.assertThat(binder.availableGas()).isEqualTo(Integer.MAX_VALUE);
  }

  @Test
  public void registerDeductedByocFees() {
    FixedList<BlockchainAddress> nodes =
        FixedList.create(
            List.of(TestObjects.ACCOUNT_ONE, TestObjects.ACCOUNT_TWO, TestObjects.ACCOUNT_THREE));
    binder.registerDeductedByocFees(Unsigned256.create(100), "symbol", nodes);
    List<PendingByocFee> pending = binder.getExecutionContext().getPendingByocFees();
    Assertions.assertThat(pending)
        .usingRecursiveComparison()
        .isEqualTo(List.of(PendingByocFee.create(nodes, Unsigned256.create(100), "symbol")));
  }
}
