package com.partisiablockchain.blockchain.contract;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.google.errorprone.annotations.ImmutableTypeParameter;
import com.partisiablockchain.serialization.StateSerializable;

/**
 * A type parameterized state serializable test object.
 *
 * @param <AlphaT> the first type parameter
 * @param <BravoT> the second type parameter
 * @param <CharlieT> the third type parameter
 */
@Immutable
public final class StateComplex<
        @ImmutableTypeParameter AlphaT,
        @ImmutableTypeParameter BravoT,
        @ImmutableTypeParameter CharlieT>
    implements StateSerializable {

  private final AlphaT alphaT;
  private final BravoT bravoT;
  private final CharlieT charlieT;

  /** Default constructor. */
  public StateComplex() {
    this.alphaT = null;
    this.bravoT = null;
    this.charlieT = null;
  }

  /**
   * Constructor that takes parameters.
   *
   * @param alphaT a alpha T
   * @param bravoT a bravo U
   * @param charlieT a charlie V
   */
  public StateComplex(AlphaT alphaT, BravoT bravoT, CharlieT charlieT) {
    this.alphaT = alphaT;
    this.bravoT = bravoT;
    this.charlieT = charlieT;
  }

  /**
   * Returns the alpha T.
   *
   * @return the alpha T
   */
  public AlphaT getAlphaT() {
    return alphaT;
  }

  /**
   * Returns the bravo T.
   *
   * @return the bravo T
   */
  public BravoT getBravoT() {
    return bravoT;
  }

  /**
   * Returns the charlie T.
   *
   * @return the charlie T
   */
  public CharlieT getCharlieT() {
    return charlieT;
  }
}
