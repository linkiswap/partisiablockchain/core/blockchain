package com.partisiablockchain.blockchain.contract;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.TestObjects;
import com.partisiablockchain.ZkTestBinder;
import com.partisiablockchain.blockchain.JarClassLoaderTest;
import com.partisiablockchain.blockchain.MutableChainState;
import com.partisiablockchain.blockchain.StateHelper;
import com.partisiablockchain.blockchain.contract.CreateContractTransactionTest.PubTestContract.PubTestState;
import com.partisiablockchain.blockchain.contract.CreateContractTransactionTest.SysTestContract.SysTestState;
import com.partisiablockchain.blockchain.fee.ContractStorage;
import com.partisiablockchain.blockchain.fee.FeePluginHelper;
import com.partisiablockchain.blockchain.transaction.EventCollector;
import com.partisiablockchain.blockchain.transaction.ExecutionContextTransaction;
import com.partisiablockchain.blockchain.transaction.ExecutionContextTransactionTest;
import com.partisiablockchain.blockchain.transaction.ExecutionEventManager;
import com.partisiablockchain.blockchain.transaction.FeeCollector;
import com.partisiablockchain.contract.pub.PubContract;
import com.partisiablockchain.contract.pub.PubContractContext;
import com.partisiablockchain.contract.sys.SysContract;
import com.partisiablockchain.contract.sys.SysContractContext;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.serialization.StateSerializable;
import com.secata.jarutil.JarBuilder;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import java.math.BigInteger;
import java.util.Map;
import org.assertj.core.api.Assertions;
import org.bouncycastle.util.encoders.Hex;
import org.junit.jupiter.api.Test;

/** Test. */
public final class CreateContractTransactionTest {

  private final KeyPair rootKey = new KeyPair(BigInteger.valueOf(213));
  private final BlockchainAddress root = rootKey.getPublic().createAddress();
  private final BlockchainAddress from = TestObjects.ACCOUNT_TWO;
  private final MutableChainState state =
      StateHelper.createMutableState(
          Map.of(TestObjects.ACCOUNT_ONE, 10L, from, 10L, root, 10L), root);

  public static CreateContractTransaction create(
      BlockchainAddress address, byte[] binderJar, byte[] contractJar, byte[] rpc) {
    return CreateContractTransaction.create(address, binderJar, contractJar, new byte[0], rpc);
  }

  @Test
  public void nullAbi() {
    CreateContractTransaction transaction =
        CreateContractTransaction.create(from, new byte[1], new byte[2], null, new byte[3]);

    byte[] bytes = SafeDataOutputStream.serialize(transaction::write);
    Assertions.assertThat(Hex.toHexString(bytes))
        .isEqualTo(
            ""
                + "00000000000000000000000000"
                + "00000000000000000200000001"
                + "00000000020000000000000000"
                + "0003000000");
  }

  @Test
  public void deploySys() {
    byte[] bindingJar = JarClassLoaderTest.getSysBinderJar();
    byte[] contractJar = JarBuilder.buildJar(SysTestContract.class);

    CreateContractTransaction transaction =
        create(TestObjects.CONTRACT_SYS, bindingJar, contractJar, new byte[0]);
    long previousBalance = FeePluginHelper.balance(state, from);
    Assertions.assertThat(previousBalance).isEqualTo(10);

    transaction.execute(executionContext(root));

    int storageGas = 0;
    long currentBalance = FeePluginHelper.balance(state, from);
    Assertions.assertThat(currentBalance).isEqualTo(previousBalance - storageGas);
    StateSerializable serializedContract = state.getContractState(TestObjects.CONTRACT_SYS);
    Assertions.assertThat(serializedContract).hasFieldOrPropertyWithValue("state", 37);
  }

  @Test
  public void deployTooLargeRpc() {
    byte[] bindingJar = JarClassLoaderTest.getSysBinderJar();
    byte[] contractJar = JarBuilder.buildJar(SysTestContract.class);

    CreateContractTransaction transaction =
        create(TestObjects.CONTRACT_SYS, bindingJar, contractJar, new byte[1]);
    Assertions.assertThatThrownBy(() -> transaction.execute(executionContext(root)))
        .hasMessageContaining("Entire content of stream was not read");
  }

  @Test
  public void simpleProperties() {
    CreateContractTransaction transaction =
        create(TestObjects.CONTRACT_SYS, new byte[0], new byte[0], new byte[0]);

    Assertions.assertThat(transaction.getTargetContract()).isEqualTo(TestObjects.CONTRACT_SYS);
  }

  @Test
  public void deployingSystemContractDoesNotEnablesStorageFees() {
    byte[] bindingJar = JarClassLoaderTest.getSysBinderJar();
    byte[] contractJar = JarBuilder.buildJar(SysTestContract.class);

    CreateContractTransaction transaction =
        create(TestObjects.CONTRACT_GOV1, bindingJar, contractJar, new byte[0]);

    ExecutionContextTransaction executionContext = executionContext(root);
    transaction.execute(executionContext);

    ContractStorage storage = FeePluginHelper.contractStorage(state, TestObjects.CONTRACT_GOV1);
    Assertions.assertThat(storage).isNotNull();
    Assertions.assertThat(storage.getLatestStorageTime()).isEqualTo(-1L);
    Assertions.assertThat(storage.getBalance()).isEqualTo(0L);
  }

  @Test
  public void deployingPublicContractEnablesStorageFees() {
    byte[] bindingJar = JarClassLoaderTest.getPublicBinderJar();
    byte[] contractJar = JarBuilder.buildJar(PubTestContract.class);

    CreateContractTransaction transaction =
        create(TestObjects.CONTRACT_PUB1, bindingJar, contractJar, new byte[0]);

    ExecutionContextTransaction executionContext = executionContext(root);
    transaction.execute(executionContext);

    ContractStorage storage = FeePluginHelper.contractStorage(state, TestObjects.CONTRACT_PUB1);
    Assertions.assertThat(storage).isNotNull();
    Assertions.assertThat(storage.getLatestStorageTime())
        .isEqualTo(executionContext.getBlockProductionTime());
    Assertions.assertThat(storage.getBalance()).isEqualTo(0L);
  }

  @Test
  public void deployingZkContractEnablesStorageFees() {
    byte[] bindingJar = JarBuilder.buildJar(ZkTestBinder.class);
    byte[] contractJar = JarBuilder.buildJar(ZkTestContract.class);

    CreateContractTransaction transaction =
        create(TestObjects.CONTRACT_ZK1, bindingJar, contractJar, new byte[0]);

    ExecutionContextTransaction executionContext = executionContext(root);
    transaction.execute(executionContext);

    ContractStorage storage = FeePluginHelper.contractStorage(state, TestObjects.CONTRACT_ZK1);
    Assertions.assertThat(storage).isNotNull();
    Assertions.assertThat(storage.getLatestStorageTime())
        .isEqualTo(executionContext.getBlockProductionTime());
    Assertions.assertThat(storage.getBalance()).isEqualTo(0L);

    state.removeContract(TestObjects.CONTRACT_ZK1, 0);
    storage = FeePluginHelper.contractStorage(state, TestObjects.CONTRACT_ZK1);
    Assertions.assertThat(storage).isNull();
  }

  private ExecutionContextTransaction executionContext(BlockchainAddress root) {
    FeeCollector feeCollector = FeeCollector.create(null, 0L);
    return ExecutionContextTransactionTest.create(
        "ChainId",
        state,
        root,
        1L,
        1L,
        new ExecutionEventManager(
            () ->
                new EventCollector(
                    null, state, 1L, 1L, TestObjects.EMPTY_HASH, 0, null, feeCollector)),
        0);
  }

  /** Test. */
  public static final class PubTestContract extends PubContract<PubTestState> {

    /** Test. */
    @Immutable
    public static final class PubTestState implements StateSerializable {

      public final int state;

      @SuppressWarnings("unused")
      PubTestState() {
        state = 0;
      }

      PubTestState(int nextState) {
        state = nextState;
      }
    }

    @Override
    public PubTestState onCreate(PubContractContext context, SafeDataInputStream rpc) {
      return new PubTestState(37);
    }
  }

  /** Test. */
  public static final class SysTestContract extends SysContract<SysTestState> {

    /** Test. */
    @Immutable
    public static final class SysTestState implements StateSerializable {

      public final int state;

      @SuppressWarnings("unused")
      SysTestState() {
        state = 0;
      }

      SysTestState(int nextState) {
        state = nextState;
      }
    }

    @Override
    public SysTestState onCreate(SysContractContext context, SafeDataInputStream rpc) {
      return new SysTestState(37);
    }
  }
}
