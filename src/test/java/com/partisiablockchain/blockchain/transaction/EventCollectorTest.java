package com.partisiablockchain.blockchain.transaction;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.TestObjects;
import com.partisiablockchain.blockchain.MutableChainState;
import com.partisiablockchain.blockchain.StateHelper;
import com.partisiablockchain.blockchain.contract.InteractWithContractTransaction;
import com.partisiablockchain.serialization.LargeByteArray;
import com.secata.tools.coverage.FunctionUtility;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class EventCollectorTest {

  private final MutableChainState chainState =
      StateHelper.initialWithAdditions(FunctionUtility.noOpConsumer());
  private final FeeCollector feeCollector =
      FeeCollector.create(chainState.getAccountPlugin(), 12345L);
  private final EventCollector eventManager =
      new EventCollector(
          "Test",
          chainState,
          123,
          1L,
          TestObjects.EMPTY_HASH,
          120,
          TestObjects.CONTRACT_PUB1,
          feeCollector);

  @Test
  public void createCallback() {
    EventCollector eventManagerRpc =
        new EventCollector(
            "Test",
            chainState,
            123,
            1L,
            TestObjects.EMPTY_HASH,
            120,
            TestObjects.CONTRACT_PUB1,
            feeCollector);
    Assertions.assertThat(eventManagerRpc.getTransactions()).hasSize(0);
    eventManagerRpc.createCallback(
        TestObjects.CONTRACT_BOOTSTRAP,
        new CallbackCreator.Callback(
            TestObjects.EMPTY_HASH,
            TestObjects.ACCOUNT_ONE,
            12345L,
            new LargeByteArray(new byte[0])));
    Assertions.assertThat(eventManagerRpc.getTransactions()).hasSize(1);
    ExecutableEvent actual = eventManagerRpc.getTransactions().get(0);
    Assertions.assertThat(actual.getEvent().getInner().getEventType())
        .isEqualTo(EventTransaction.EventType.CALLBACK);
  }

  @Test
  public void checkCallbackFailing() {
    EventCollector eventManagerRpc =
        new EventCollector(
            "Test",
            chainState,
            123,
            1L,
            TestObjects.EMPTY_HASH,
            120,
            TestObjects.CONTRACT_PUB1,
            feeCollector);
    assertCallBack(eventManagerRpc);
  }

  @Test
  public void currentContract() {
    EventCollector eventManagerRpc =
        new EventCollector(
            "Test",
            chainState,
            123,
            1L,
            TestObjects.EMPTY_HASH,
            120,
            TestObjects.CONTRACT_PUB1,
            feeCollector);
    Assertions.assertThat(eventManagerRpc.getCurrentContract())
        .isEqualTo(TestObjects.CONTRACT_PUB1);
  }

  @Test
  public void checkSystemEventOfCallBackType() {
    EventCollector eventManagerRpc =
        new EventCollector(
            "Test",
            chainState,
            123,
            1L,
            TestObjects.EMPTY_HASH,
            120,
            TestObjects.CONTRACT_GOV1,
            feeCollector);
    assertCallBack(eventManagerRpc);
  }

  @Test
  public void levelHeight() {
    Assertions.assertThat(eventManager.shouldCreateCallback()).isFalse();
    EventCollector eventManager127 =
        new EventCollector(
            "Test",
            chainState,
            123,
            1L,
            TestObjects.EMPTY_HASH,
            127,
            TestObjects.CONTRACT_PUB1,
            feeCollector);
    Assertions.assertThat(eventManager127.getTransactions()).hasSize(0);
    eventManager127.createEventTransaction(
        TestObjects.ACCOUNT_ONE,
        InteractWithContractTransaction.create(TestObjects.CONTRACT_BOOTSTRAP, new byte[0]),
        1230L,
        false);
    Assertions.assertThat(eventManager127.getTransactions()).hasSize(1);
    EventCollector eventManager128 =
        new EventCollector(
            "Test",
            chainState,
            123,
            1L,
            TestObjects.EMPTY_HASH,
            128,
            TestObjects.CONTRACT_PUB1,
            feeCollector);
    Assertions.assertThatThrownBy(
        () ->
            eventManager128.createEventTransaction(
                TestObjects.ACCOUNT_ONE,
                InteractWithContractTransaction.create(TestObjects.CONTRACT_BOOTSTRAP, new byte[0]),
                1230L,
                false));
    Assertions.assertThat(eventManager128.getTransactions()).hasSize(0);
  }

  private void assertCallBack(EventCollector eventManagerRpc) {
    eventManagerRpc.checkCallbackFailing(TestObjects.EMPTY_HASH, null);
    Assertions.assertThat(eventManagerRpc.getTransactions()).hasSize(0);
    eventManagerRpc.checkCallbackFailing(
        TestObjects.EMPTY_HASH, new ReturnEnvelope(TestObjects.CONTRACT_BOOTSTRAP));
    Assertions.assertThat(eventManagerRpc.getTransactions()).hasSize(1);
    ExecutableEvent actual = eventManagerRpc.getTransactions().get(0);
    Assertions.assertThat(actual.getEvent().getInner().getEventType())
        .isEqualTo(EventTransaction.EventType.SYSTEM);
  }
}
