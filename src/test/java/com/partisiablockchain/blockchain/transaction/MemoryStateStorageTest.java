package com.partisiablockchain.blockchain.transaction;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.blockchain.account.AccountState;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.serialization.StateSerializer;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class MemoryStateStorageTest {

  @Test
  public void write() {
    MemoryStateStorage memoryStateStorage = new MemoryStateStorage();
    Assertions.assertThat(
            memoryStateStorage.write(
                Hash.fromString("0000000000000000000000000000000000000000000000000000000000000000"),
                s -> s.writeInt(1)))
        .isTrue();
  }

  @Test
  public void read() {
    MemoryStateStorage memoryStateStorage = new MemoryStateStorage();
    StateSerializer stateSerializer = new StateSerializer(memoryStateStorage, true, true);
    AccountState state = AccountState.create(1);
    Hash stateHash = stateSerializer.write(state).hash();
    Assertions.assertThat(memoryStateStorage.getData().get(stateHash)).isNotNull();
    AccountState result = stateSerializer.read(stateHash, AccountState.class);
    Assertions.assertThat(state.getNonce()).isEqualTo(result.getNonce());
  }
}
