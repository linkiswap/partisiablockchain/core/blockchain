package com.partisiablockchain.blockchain.transaction;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.binder.sys.SystemInteraction;

/** Test utility. */
public final class EventCollectorUtil {

  /**
   * Handles a system interaction.
   *
   * @param systemInteraction the system interaction
   */
  public static void handle(SystemInteraction systemInteraction, EventCollector eventCollector) {
    eventCollector.handle(systemInteraction);
  }

  /**
   * Interacts with a contract.
   *
   * @param eventHandler the event handler
   * @param address the address
   * @param rpc the rpc bytes
   * @param asSender true as original sender
   * @param cost cost to the event
   */
  public static void interaction(
      BlockchainAddress address,
      byte[] rpc,
      boolean asSender,
      int cost,
      EventCollector eventCollector,
      EventSender eventHandler) {
    eventCollector.interaction(address, rpc, asSender, cost, false, eventHandler);
  }

  /**
   * Deploys with a contract.
   *
   * @param eventSender the event handler
   * @param address the address
   * @param binder the binder bytes
   * @param contract the contract bytes
   * @param abi the abi bytes
   * @param rpc the rpc bytes
   * @param originalSender true as original sender
   * @param cost cost to the event
   */
  public static void deploy(
      BlockchainAddress address,
      byte[] binder,
      byte[] contract,
      byte[] abi,
      byte[] rpc,
      boolean originalSender,
      int cost,
      EventCollector eventCollector,
      EventSender eventSender) {
    eventCollector.deploy(address, binder, contract, abi, rpc, originalSender, cost, eventSender);
  }
}
