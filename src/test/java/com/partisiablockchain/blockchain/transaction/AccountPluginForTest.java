package com.partisiablockchain.blockchain.transaction;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.blockchain.AccountPluginAccessor;

/** An account plugin for testing purposes. */
@Immutable
public final class AccountPluginForTest extends AccountPluginAccessor {

  private final long factor;

  /**
   * Construct the plugin.
   *
   * @param factor the factor to multiply the cost with
   */
  public AccountPluginForTest(long factor) {
    this.factor = factor;
  }

  @Override
  public long convertNetworkFee(long l) {
    return l * factor;
  }

  @Override
  public boolean canCoverFee(SignedTransaction transaction, long blockProductionTime) {
    return false;
  }
}
