package com.partisiablockchain.blockchain.transaction;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.TestObjects;
import com.partisiablockchain.blockchain.ChainPluginType;
import com.partisiablockchain.blockchain.JarClassLoaderTest;
import com.partisiablockchain.blockchain.MutableChainState;
import com.partisiablockchain.blockchain.ShardId;
import com.partisiablockchain.blockchain.StateHelper;
import com.partisiablockchain.blockchain.contract.CoreContractStateTest;
import com.partisiablockchain.blockchain.transaction.EventTransaction.InnerEvent;
import com.partisiablockchain.blockchain.transaction.InnerSystemEvent.CreateAccountEvent;
import com.partisiablockchain.blockchain.transaction.InnerSystemEvent.SetFeatureEvent;
import com.partisiablockchain.blockchain.transaction.InnerSystemEvent.UpdateContextFreePluginState;
import com.partisiablockchain.blockchain.transaction.InnerSystemEvent.UpdateGlobalPluginStateEvent;
import com.partisiablockchain.blockchain.transaction.InnerSystemEvent.UpdateLocalPluginStateEvent;
import com.partisiablockchain.blockchain.transaction.InnerSystemEvent.UpdatePluginEvent;
import com.partisiablockchain.contract.sys.GlobalPluginStateUpdate;
import com.partisiablockchain.contract.sys.LocalPluginStateUpdate;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.serialization.StateLong;
import com.secata.jarutil.JarBuilder;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.coverage.FunctionUtility;
import java.nio.charset.StandardCharsets;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

/** Test. */
public final class InnerSystemEventTest {

  @Test
  public void readWriteCreateAccount() {
    CreateAccountEvent createAccountEvent = new CreateAccountEvent(TestObjects.ACCOUNT_FOUR);
    readWriteAndAssert(createAccountEvent, TestObjects.ACCOUNT_FOUR);
  }

  @Test
  public void readWriteRemoveContract() {
    InnerSystemEvent.RemoveContract event =
        new InnerSystemEvent.RemoveContract(TestObjects.CONTRACT_PUB1);
    readWriteAndAssert(event, TestObjects.CONTRACT_PUB1);
  }

  @Test
  public void removeContract() {
    MutableChainState state = Mockito.mock(MutableChainState.class);
    InnerSystemEvent.RemoveContract event =
        new InnerSystemEvent.RemoveContract(TestObjects.CONTRACT_SYS);
    byte[] returnValue = event.execute(null, FunctionUtility.noOpBiConsumer(), null, state, 0);
    Mockito.verify(state).removeContract(TestObjects.CONTRACT_SYS, 0);
    Assertions.assertThat(returnValue).isEqualTo(InnerSystemEvent.EMPTY_RETURN_VALUE);
  }

  @Test
  public void readWriteSetFeature() {
    SetFeatureEvent event = new SetFeatureEvent("Fe", "Val");
    readWriteAndAssert(event, null);
  }

  @Test
  public void readWriteCallback() {
    InnerSystemEvent.CallbackEvent event =
        new InnerSystemEvent.CallbackEvent(
            new ReturnEnvelope(TestObjects.CONTRACT_PUB1),
            Hash.create(stream -> stream.writeInt(123)),
            true,
            "123".getBytes(StandardCharsets.UTF_8));
    readWriteAndAssert(event, TestObjects.CONTRACT_PUB1);
  }

  private void readWriteAndAssert(InnerSystemEvent event, BlockchainAddress expectedTarget) {
    InnerEvent eventFromBytes =
        SafeDataInputStream.readFully(SafeDataOutputStream.serialize(event), InnerEvent::read);
    BlockchainAddress target = event.target();
    Assertions.assertThat(target).isEqualTo(expectedTarget);
    Assertions.assertThat(eventFromBytes).usingRecursiveComparison().isEqualTo(event);
  }

  @Test
  public void readWriteUpdatePlugin() {
    UpdatePluginEvent event =
        new UpdatePluginEvent(ChainPluginType.CONSENSUS, new byte[22], new byte[12]);
    readWriteAndAssert(event, null);
  }

  @Test
  public void readWriteUpdateGlobalPluginState() {
    UpdateGlobalPluginStateEvent event =
        new UpdateGlobalPluginStateEvent(
            ChainPluginType.CONSENSUS, GlobalPluginStateUpdate.create(new byte[22]));
    readWriteAndAssert(event, null);
  }

  @Test
  public void updateGlobalPluginStateEvent_ReturnValue() {
    byte[] expectedReturnValue = new byte[] {1, 2, 3};
    MutableChainState state = Mockito.mock(MutableChainState.class);
    Mockito.when(state.updateGlobalPluginState(Mockito.anyLong(), Mockito.any(), Mockito.any()))
        .thenReturn(expectedReturnValue);
    UpdateGlobalPluginStateEvent event =
        new UpdateGlobalPluginStateEvent(
            ChainPluginType.ACCOUNT, GlobalPluginStateUpdate.create(new byte[0]));
    byte[] returnValue = event.execute(null, FunctionUtility.noOpBiConsumer(), null, state, 0);
    Assertions.assertThat(returnValue).isEqualTo(expectedReturnValue);
  }

  @Test
  public void readWriteUpdateLocalPluginState() {
    UpdateLocalPluginStateEvent event =
        new UpdateLocalPluginStateEvent(
            ChainPluginType.CONSENSUS,
            LocalPluginStateUpdate.create(TestObjects.ACCOUNT_FOUR, new byte[3]));
    readWriteAndAssert(event, TestObjects.ACCOUNT_FOUR);
  }

  @Test
  public void readWriteUpdateLocalPluginStateNamedShard() {
    UpdateContextFreePluginState event =
        new UpdateContextFreePluginState(ChainPluginType.ACCOUNT, new byte[0]);
    readWriteAndAssert(event, null);
  }

  @Test
  public void readWriteCheckExistence() {
    InnerSystemEvent.CheckExistenceEvent event =
        new InnerSystemEvent.CheckExistenceEvent(TestObjects.ACCOUNT_TWO);
    readWriteAndAssert(event, TestObjects.ACCOUNT_TWO);
  }

  @Test
  public void readWriteCreateShard() {
    String shardId = "Shard0";
    InnerSystemEvent.CreateShardEvent event = new InnerSystemEvent.CreateShardEvent(shardId);
    readWriteAndAssert(event, null);
  }

  @Test
  public void createShard() {
    ShardId shardId = new ShardId("Shard0");
    MutableChainState state = Mockito.mock(MutableChainState.class);
    InnerSystemEvent.CreateShardEvent event =
        new InnerSystemEvent.CreateShardEvent(shardId.getId());
    byte[] returnValue = event.execute(null, FunctionUtility.noOpBiConsumer(), null, state, 0);
    Mockito.verify(state).addActiveShard(FunctionUtility.noOpBiConsumer(), null, shardId.getId());
    Assertions.assertThat(returnValue).isEqualTo(InnerSystemEvent.EMPTY_RETURN_VALUE);
  }

  @Test
  public void readWriteRemoveShard() {
    String shardId = "Shard0";
    InnerSystemEvent.RemoveShardEvent event = new InnerSystemEvent.RemoveShardEvent(shardId);
    readWriteAndAssert(event, null);
  }

  @Test
  public void removeShard() {
    ShardId shardId = new ShardId("Shard0");
    MutableChainState state = Mockito.mock(MutableChainState.class);
    InnerSystemEvent.RemoveShardEvent event =
        new InnerSystemEvent.RemoveShardEvent(shardId.getId());
    byte[] returnValue = event.execute(null, FunctionUtility.noOpBiConsumer(), null, state, 0);
    Mockito.verify(state)
        .removeActiveShard(FunctionUtility.noOpBiConsumer(), null, shardId.getId());
    Assertions.assertThat(returnValue).isEqualTo(InnerSystemEvent.EMPTY_RETURN_VALUE);
  }

  @Test
  public void updateLocalPluginStateEvent_ReturnValue() {
    byte[] expectedReturnValue = new byte[] {1, 2, 3};
    MutableChainState state = Mockito.mock(MutableChainState.class);
    Mockito.when(
            state.updateLocalPluginState(
                Mockito.any(), Mockito.any(), Mockito.any(), Mockito.anyLong()))
        .thenReturn(expectedReturnValue);
    UpdateLocalPluginStateEvent event =
        new UpdateLocalPluginStateEvent(
            ChainPluginType.ACCOUNT,
            LocalPluginStateUpdate.create(TestObjects.ACCOUNT_FOUR, new byte[0]));
    byte[] returnValue = event.execute(null, FunctionUtility.noOpBiConsumer(), null, state, 0);
    Assertions.assertThat(returnValue).isEqualTo(expectedReturnValue);
  }

  @Test
  public void updateLocalPluginStateEventNamedShard() {
    byte[] expectedReturnValue = new byte[] {1, 2, 3};
    MutableChainState state = Mockito.mock(MutableChainState.class);
    Mockito.when(state.updateLocalPluginState(Mockito.any(), Mockito.any(), Mockito.anyLong()))
        .thenReturn(expectedReturnValue);
    UpdateContextFreePluginState event =
        new UpdateContextFreePluginState(ChainPluginType.ACCOUNT, new byte[0]);
    byte[] returnValue = event.execute(null, FunctionUtility.noOpBiConsumer(), null, state, 0);
    Assertions.assertThat(returnValue).isEqualTo(expectedReturnValue);
  }

  @Test
  public void determineShardForUpdateLocalPluginState() {
    UpdateLocalPluginStateEvent event =
        new UpdateLocalPluginStateEvent(
            ChainPluginType.CONSENSUS, LocalPluginStateUpdate.create(null, new byte[3]));

    BlockchainAddress target = event.target();
    Assertions.assertThat(target).isEqualTo(null);
  }

  @Test
  public void upgradeEvent() {
    byte[] contractJar = {69};
    byte[] binderJar = {42};
    byte[] rpc = {88};
    BlockchainAddress contractAddress = TestObjects.ACCOUNT_TWO;
    InnerSystemEvent.UpgradeSystemContractEvent upgradeEvent =
        new InnerSystemEvent.UpgradeSystemContractEvent(
            contractJar, binderJar, new byte[0], rpc, contractAddress);

    InnerSystemEvent.SystemEventType systemEventType = upgradeEvent.getSystemEventType();
    Assertions.assertThat(systemEventType)
        .isEqualTo(InnerSystemEvent.SystemEventType.UPGRADE_SYSTEM_CONTRACT);

    readWriteAndAssert(upgradeEvent, TestObjects.ACCOUNT_TWO);
  }

  @Test
  public void upgradeEventExecute() {
    byte[] oldContractJar =
        JarBuilder.buildJar(ExecutionContextTest.AllocatedCostSysContract.class);
    byte[] oldBinderJar = JarClassLoaderTest.getSysBinderJar();
    BlockchainAddress contractAddress = TestObjects.CONTRACT_SYS;
    MutableChainState state =
        StateHelper.initialWithAdditions(
            builder -> {
              Hash binderJarHash = builder.saveJar(oldBinderJar);
              Hash contractJarHash = builder.saveJar(oldContractJar);
              builder.createContract(
                  contractAddress,
                  CoreContractStateTest.create(binderJarHash, contractJarHash, 2236));
              builder.setContractState(contractAddress, new StateLong(2L));
            });
    int oldContractSize = state.getCoreContractState(contractAddress).computeStorageLength();
    byte[] newContractJar = JarBuilder.buildJar(ExecutionContextTest.PayServiceFees.class);
    byte[] rpc = {88};
    InnerSystemEvent.UpgradeSystemContractEvent upgradeEvent =
        new InnerSystemEvent.UpgradeSystemContractEvent(
            newContractJar, oldBinderJar, new byte[0], rpc, contractAddress);
    byte[] result = upgradeEvent.execute(null, null, null, state, 0);

    int contractSize = state.getCoreContractState(contractAddress).computeStorageLength();
    Assertions.assertThat(result).isEqualTo(InnerSystemEvent.EMPTY_RETURN_VALUE);
    Assertions.assertThat(oldContractSize).isNotEqualTo(contractSize);
    Assertions.assertThat(state.getContractState(contractAddress)).isNull();
  }
}
