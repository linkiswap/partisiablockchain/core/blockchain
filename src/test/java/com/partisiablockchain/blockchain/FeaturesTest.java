package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.tree.AvlTree;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

final class FeaturesTest {

  @Test
  void hasOpenAccountCreation() {
    AvlTree<String, String> features = AvlTree.create();
    Assertions.assertThat(new Features(features).hasOpenAccountCreation()).isFalse();
    Assertions.assertThat(
            new Features(features.set(Features.FEATURE_OPEN_ACCOUNT_CREATION, "enabled"))
                .hasOpenAccountCreation())
        .isTrue();
  }

  @Test
  void hasCheckExistenceForContractsWithOpenAccount() {
    AvlTree<String, String> features = AvlTree.create();
    Assertions.assertThat(new Features(features).hasCheckExistenceForContractWithOpenAccount())
        .isFalse();
    Assertions.assertThat(
            new Features(
                    features.set(
                        Features.FEATURE_CHECK_EXISTENCE_FOR_CONTRACT_OPEN_ACCOUNT, "enabled"))
                .hasCheckExistenceForContractWithOpenAccount())
        .isTrue();
  }

  @Test
  void hasRegisterRemainingGasFromFailingSignedTransaction() {
    AvlTree<String, String> features = AvlTree.create();
    Assertions.assertThat(new Features(features).hasRegisterGasForFailingSignedTransaction())
        .isFalse();
    Assertions.assertThat(
            new Features(
                    features.set(Features.REGISTER_GAS_FOR_FAILING_SIGNED_TRANSACTION, "enabled"))
                .hasRegisterGasForFailingSignedTransaction())
        .isTrue();
  }
}
