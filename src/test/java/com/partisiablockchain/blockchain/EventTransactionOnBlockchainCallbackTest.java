package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.CloseableTest;
import com.partisiablockchain.DummyExecutableEventFinder;
import com.partisiablockchain.TestObjects;
import com.partisiablockchain.blockchain.BlockchainLedger.BlockAndState;
import com.partisiablockchain.blockchain.contract.CoreContractStateTest;
import com.partisiablockchain.blockchain.contract.InteractWithContractTransaction;
import com.partisiablockchain.blockchain.fee.FeePluginHelper;
import com.partisiablockchain.blockchain.transaction.CoreTransactionPart;
import com.partisiablockchain.blockchain.transaction.SignedTransaction;
import com.partisiablockchain.contract.CallbackContext;
import com.partisiablockchain.contract.EventCreator;
import com.partisiablockchain.contract.EventManager;
import com.partisiablockchain.contract.pub.PubContract;
import com.partisiablockchain.contract.pub.PubContractContext;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.storage.MemoryStorage;
import com.partisiablockchain.storage.RootDirectory;
import com.partisiablockchain.tree.AvlTree;
import com.secata.jarutil.JarBuilder;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.coverage.FunctionUtility;
import java.math.BigInteger;
import java.nio.file.Path;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/** Test. */
public final class EventTransactionOnBlockchainCallbackTest extends CloseableTest {

  private final KeyPair producerKey = new KeyPair(BigInteger.ONE);
  private final BlockchainAddress producerAccount = producerKey.getPublic().createAddress();

  private BlockchainLedger ledger;

  /** Creates a new test object. */
  @BeforeEach
  public void setUp() {
    this.ledger = register(setupForTest(temporaryFolder));
  }

  static BlockchainLedger setupForTest(Path temporaryFolder) {
    RootDirectory rootDirectory = MemoryStorage.createRootDirectory(temporaryFolder.toFile());
    BlockChainTestNetwork network = new BlockChainTestNetwork();
    return BlockchainLedger.createForTest(
        rootDirectory,
        chainstate -> {
          StateHelper.initial(chainstate);
          byte[] jar = JarBuilder.buildJar(BlockchainLedgerTest.DeployContractInTest.class);

          BlockchainAddress deployPublicContract = TestObjects.CONTRACT_BOOTSTRAP;
          CoreContractStateTest.createContract(
              chainstate, jar, deployPublicContract, JarClassLoaderTest.getSysBinderJar());
          FeePluginHelper.mintGas(chainstate, deployPublicContract, 1_000_000);
          byte[] rpc = new byte[0];
          chainstate.setPlugin(
              FunctionUtility.noOpBiConsumer(), null, ChainPluginType.ACCOUNT, null, rpc);
        },
        network,
        new DummyExecutableEventFinder());
  }

  @Test
  public void createSimpleEvents() {
    runTest(2, 3);
  }

  @Test
  public void createManyEvents() {
    runTest(3, 7);
  }

  private void runTest(int eventCount, int callbackRounds) {
    SignedTransaction deployEvent =
        createDeployPub(
            SafeDataOutputStream.serialize(
                safeDataOutputStream -> {
                  safeDataOutputStream.writeInt(eventCount);
                  safeDataOutputStream.writeInt(callbackRounds);
                }));
    ledger.addPendingTransaction(deployEvent);

    BlockchainLedgerTest.produce(ledger, deployEvent);

    BlockAndState latest = ledger.latest();

    AvlTree<Hash, Boolean> executionResults =
        latest.getState().getExecutedState().getExecutionStatus();
    EventTransactionOnBlockchainTest.printEventTree(deployEvent.identifier(), ledger, "");
    int numberOfEvents = eventCount * callbackRounds;
    assertThat(executionResults.size()).isEqualTo(1 + 2 + numberOfEvents * 2 + callbackRounds);

    BlockchainLedgerTest.produce(ledger);

    StateSerializable contractState = latest.getState().getContractState(TestObjects.CONTRACT_PUB2);
    assertThat(contractState).isNotNull();
    assertThat(contractState)
        .hasFieldOrPropertyWithValue("numberOfEvents", eventCount * (callbackRounds + 1));

    latest = ledger.latest();
    assertThat(ledger.getTransactionsForBlock(latest.getBlock())).hasSize(0);
  }

  private long getNonce(BlockchainAddress account) {
    return ledger.latest().getState().getAccount(account).getNonce();
  }

  private SignedTransaction createDeployPub(byte[] rpc) {
    byte[] binderJar = JarClassLoaderTest.getPublicBinderJar();
    byte[] jar = JarBuilder.buildJar(EventWithCallbackContract.class);
    InteractWithContractTransaction deployTransaction =
        BlockchainLedgerTest.DeployContractInTest.create(
            TestObjects.CONTRACT_PUB2, binderJar, jar, rpc, 0);
    return SignedTransaction.create(
            CoreTransactionPart.create(
                getNonce(producerAccount), System.currentTimeMillis() + 100_000, 0),
            deployTransaction)
        .sign(producerKey, ledger.getChainId());
  }

  /** Test. */
  public static final class EventWithCallbackContract
      extends PubContract<EventWithCallbackContract.State> {

    @Override
    public State onCreate(PubContractContext context, SafeDataInputStream rpc) {
      EventManager callCreator = context.getRemoteCallsCreator();
      BlockchainAddress contract = context.getContractAddress();
      int events = rpc.readInt();
      int callbackCounter = rpc.readInt();
      for (int i = 0; i < events; i++) {
        callCreator.invoke(contract).withPayload(EventCreator.EMPTY_RPC).sendFromContract();
      }
      callCreator.registerCallbackWithCostFromRemaining(
          stream -> {
            stream.writeInt(events);
            stream.writeInt(callbackCounter);
          });
      return new State(0, events * (callbackCounter + 1));
    }

    @Override
    public State onInvoke(PubContractContext context, State state, SafeDataInputStream rpc) {
      return new State(state.countedEvents + 1, state.numberOfEvents);
    }

    @Override
    public State onCallback(
        PubContractContext context,
        State state,
        CallbackContext callbackContext,
        SafeDataInputStream rpc) {
      EventManager callCreator = context.getRemoteCallsCreator();
      BlockchainAddress contract = context.getContractAddress();
      int events = rpc.readInt();
      int callbackCounter = rpc.readInt();
      int size = callbackContext.results().size();
      if (events != size) {
        throw new RuntimeException("Expecting no of events " + events + "=" + size);
      }
      int countedEvents = state.countedEvents;
      int numberOfEvents = state.numberOfEvents;
      String computation =
          events + "*" + callbackCounter + "+" + countedEvents + "=" + numberOfEvents;
      if (events * callbackCounter + countedEvents != numberOfEvents) {
        throw new RuntimeException("Expecting " + computation);
      }

      if (callbackCounter > 1) {
        for (int i = 0; i < events; i++) {
          callCreator.invoke(contract).withPayload(EventCreator.EMPTY_RPC).sendFromContract();
        }
        callCreator.registerCallbackWithCostFromRemaining(
            stream -> {
              stream.writeInt(events);
              stream.writeInt(callbackCounter - 1);
            });
      } else {
        context.setResult(s -> s.writeLong(1234));
      }
      return new State(countedEvents, numberOfEvents);
    }

    /** Test. */
    @Immutable
    public static final class State implements StateSerializable {

      public final int numberOfEvents;
      public final int countedEvents;

      @SuppressWarnings("unused")
      State() {
        numberOfEvents = 0;
        countedEvents = 0;
      }

      State(int countedEvents, int numberOfEvents) {
        this.numberOfEvents = numberOfEvents;
        this.countedEvents = countedEvents;
      }
    }
  }
}
