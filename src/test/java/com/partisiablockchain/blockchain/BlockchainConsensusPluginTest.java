package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateVoid;
import com.secata.stream.SafeDataInputStream;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class BlockchainConsensusPluginTest {

  @Test
  public void invokeLocal() {
    BlockchainConsensusPlugin<StateVoid, Hash> consensusPlugin = new EmptyPlugin();
    Assertions.assertThatThrownBy(() -> consensusPlugin.invokeLocal(null, null, null, null, null))
        .isInstanceOf(UnsupportedOperationException.class);
    Assertions.assertThat(consensusPlugin.migrateLocal(null)).isNull();
    Assertions.assertThat(consensusPlugin.getLocalStateClass()).isEqualTo(Hash.class);
    Assertions.assertThat(consensusPlugin.updateForBlock(() -> 0, null, Block.GENESIS_PARENT, null))
        .isEqualTo(Block.GENESIS_PARENT);
  }

  private static final class EmptyPlugin extends BlockchainConsensusPlugin<StateVoid, Hash> {

    @Override
    public Class<StateVoid> getGlobalStateClass() {
      return StateVoid.class;
    }

    @Override
    public InvokeResult<StateVoid> invokeGlobal(
        PluginContext pluginContext, StateVoid state, byte[] rpc) {
      return null;
    }

    @Override
    public Hash updateForBlock(
        PluginContext pluginContext, StateVoid globalState, Hash hash, Block block) {
      return hash;
    }

    @Override
    public StateVoid migrateGlobal(StateAccessor currentGlobal, SafeDataInputStream rpc) {
      return null;
    }

    @Override
    public Hash migrateLocal(StateAccessor currentLocal) {
      return null;
    }

    @Override
    public BlockValidation validateLocalBlock(StateVoid globalState, Hash local, FinalBlock block) {
      return null;
    }

    @Override
    public boolean validateExternalBlock(StateVoid globalState, FinalBlock block) {
      return false;
    }

    @Override
    public Class<Hash> getLocalStateClass() {
      return Hash.class;
    }
  }
}
