package com.partisiablockchain.storage;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.CloseableTest;
import java.io.File;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class RootDirectoryTest extends CloseableTest {

  @Test
  public void createSub() {
    RootDirectory rootDirectory = MemoryStorage.createRootDirectory(temporaryFolder.toFile());
    Assertions.assertThat(rootDirectory.createSub("test")).isNotNull();

    Assertions.assertThat(new File(temporaryFolder.toFile(), "test").exists()).isTrue();
  }

  @Test
  public void isEmpty() {
    RootDirectory rootDirectory = MemoryStorage.createRootDirectory(temporaryFolder.toFile());
    Assertions.assertThat(rootDirectory.isEmpty()).isTrue();

    File test = new File(temporaryFolder.toFile(), "test");
    Assertions.assertThat(test.mkdir()).isTrue();
    Assertions.assertThat(rootDirectory.isEmpty()).isFalse();
  }

  @Test
  public void createRoot() {
    RootDirectory rootDirectory =
        MemoryStorage.createRootDirectory(new File(temporaryFolder.toFile(), "sub"));
    Assertions.assertThat(rootDirectory.isEmpty()).isTrue();
    File test = new File(temporaryFolder.toFile(), "sub");
    Assertions.assertThat(test.exists()).isTrue();
  }

  @Test
  public void invalidDir() {
    File root = temporaryFolder.toFile();
    File subDir = new File(root, "Illegal-\\\u0000-path");
    RootDirectory rootDirectory = MemoryStorage.createRootDirectory(subDir);
    Assertions.assertThatThrownBy(rootDirectory::isEmpty)
        .isInstanceOf(IllegalStateException.class)
        .hasMessageStartingWith("Cannot work in an illegal path: ")
        .hasMessageContaining(subDir.getName())
        .hasMessageContaining(subDir.getPath());
  }

  @Test
  void createSubFileIllegalSubPath() {
    File root = temporaryFolder.toFile();
    File subDir = new File(root, "createSubFile");
    RootDirectory rootDirectory = MemoryStorage.createRootDirectory(subDir);
    rootDirectory.createFile("okay");
    Assertions.assertThatThrownBy(() -> rootDirectory.createFile("../subpath"))
        .hasMessage("Filename is not legal: ../subpath")
        .isInstanceOf(IllegalArgumentException.class);
  }
}
