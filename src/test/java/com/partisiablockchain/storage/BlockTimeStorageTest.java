package com.partisiablockchain.storage;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.CloseableTest;
import java.io.File;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class BlockTimeStorageTest extends CloseableTest {

  @Test
  public void shouldWriteMinValueToFile() {
    File file = new File(temporaryFolder.toFile(), "shouldWriteMinValueToFile");
    BlockTimeStorage storage = new BlockTimeStorage(file, 7);
    Assertions.assertThat(storage.getLatestBlockTime()).isEqualTo(7);
    storage.close();

    BlockTimeStorage nextStorage = new BlockTimeStorage(file, 0);
    nextStorage.close();
    Assertions.assertThat(nextStorage.getLatestBlockTime()).isEqualTo(7);
  }

  @Test
  public void shouldNotWriteWhenNotGreaterThan() {
    File file = new File(temporaryFolder.toFile(), "shouldNotWriteWhenNotGreaterThan");
    BlockTimeStorage storage = new BlockTimeStorage(file, 7);
    storage.close();
    storage.storeLatestBlockTime(7);
    Assertions.assertThatThrownBy(() -> storage.storeLatestBlockTime(8))
        .hasStackTraceContaining("IOException: Stream Closed");
  }
}
