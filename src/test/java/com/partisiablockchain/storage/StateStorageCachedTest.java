package com.partisiablockchain.storage;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.CloseableTest;
import com.partisiablockchain.TestObjects;
import com.partisiablockchain.crypto.Hash;
import com.secata.stream.SafeDataInputStream;
import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.AbstractExecutorService;
import java.util.concurrent.TimeUnit;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/** Test. */
public final class StateStorageCachedTest extends CloseableTest {

  private Storage storage;

  /** Setup the test. */
  @BeforeEach
  public void setUp() {
    File dataDirectory = temporaryFolder.toFile();
    Assertions.assertThat(dataDirectory.mkdirs()).isFalse();
    storage = new MemoryStorage(new HashMap<>(), "test", false);
  }

  @Test
  public void makeSureWeFromReadCached() {
    StateStorageCached stateStorageCached =
        new StateStorageCached(storage, new InThreadExecutor(true));
    Hash identifier = Hash.create(s -> s.writeString("StraightIntoStorage"));
    Assertions.assertThat(
            stateStorageCached.write(identifier, s1 -> s1.writeString("StraightIntoStorage")))
        .isTrue();
    Assertions.assertThat(stateStorageCached.read(identifier, SafeDataInputStream::readString))
        .isEqualTo("StraightIntoStorage");
    Assertions.assertThat(
            stateStorageCached.write(identifier, s1 -> s1.writeString("StraightIntoStorage")))
        .isFalse();

    storage.write(identifier, s -> s.writeString("ChangedString"));

    // Make sure we hit the read cache
    Assertions.assertThat(stateStorageCached.read(identifier, SafeDataInputStream::readString))
        .isEqualTo("StraightIntoStorage");
  }

  @Test
  public void readingCachedFromPendingWrites() {
    Hash identifier = Hash.create(TestObjects.EMPTY_HASH);
    StateStorageCached stateStorageCached =
        new StateStorageCached(storage, new InThreadExecutor(false));
    stateStorageCached.write(identifier, TestObjects.EMPTY_HASH);

    // This will only work if pending write cache is active
    Assertions.assertThat(stateStorageCached.read(identifier, Hash::read))
        .isEqualTo(TestObjects.EMPTY_HASH);
  }

  @Test
  public void inconsistentHash() {
    StateStorageCached stateStorageCached =
        new StateStorageCached(storage, new InThreadExecutor(false));
    Assertions.assertThatThrownBy(
            () -> stateStorageCached.write(TestObjects.EMPTY_HASH, s -> s.writeByte(0)))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessageContaining("Inconsistent")
        .hasMessageContaining(TestObjects.EMPTY_HASH.toString())
        .hasMessageContaining(Hash.create(s -> s.writeByte(0)).toString());
  }

  @Test
  public void readingFromStorageAndCache() {
    StateStorageCached stateStorageCached =
        new StateStorageCached(storage, new InThreadExecutor(false));
    storage.write(TestObjects.EMPTY_HASH, s -> s.writeString("OriginalString"));

    Assertions.assertThat(
            stateStorageCached.read(TestObjects.EMPTY_HASH, SafeDataInputStream::readString))
        .isEqualTo("OriginalString");

    storage.write(TestObjects.EMPTY_HASH, s -> s.writeString("ChangedString"));
    Assertions.assertThat(
            stateStorageCached.read(TestObjects.EMPTY_HASH, SafeDataInputStream::readString))
        .isEqualTo("OriginalString");
  }

  @Test
  public void readNull() {
    StateStorageCached stateStorageCached =
        new StateStorageCached(storage, new InThreadExecutor(false));
    Hash identifier = Hash.create(s -> s.writeString("MyHash"));
    Assertions.assertThat(stateStorageCached.read(identifier, SafeDataInputStream::readString))
        .isNull();

    // Ensure that the read is not cached
    boolean written = stateStorageCached.write(identifier, s -> s.writeString("MyHash"));
    Assertions.assertThat(written).isTrue();
  }

  @Test
  public void shouldCacheRawReads() {
    StateStorageCached stateStorageCached =
        new StateStorageCached(storage, new InThreadExecutor(false));
    Hash identifier = Hash.create(s -> s.writeString("Value"));
    storage.write(identifier, s -> s.writeString("Value"));

    Assertions.assertThat(stateStorageCached.read(identifier)).isNotNull();
    storage.remove(identifier);
    Assertions.assertThat(stateStorageCached.read(identifier)).isNotNull();
  }

  @Test
  public void close() {
    InThreadExecutor executor = new InThreadExecutor(false);
    StateStorageCached stateStorageCached = new StateStorageCached(storage, executor);
    stateStorageCached.close();
    Assertions.assertThat(executor.closed).isTrue();
    Assertions.assertThat(executor.awaitTermination).isTrue();
  }

  private static final class InThreadExecutor extends AbstractExecutorService {

    boolean closed;
    boolean awaitTermination;
    private final boolean run;

    private InThreadExecutor(boolean run) {
      this.run = run;
    }

    @Override
    public void execute(Runnable command) {
      if (run) {
        command.run();
      }
    }

    @Override
    public void shutdown() {
      closed = true;
    }

    @Override
    public List<Runnable> shutdownNow() {
      return List.of();
    }

    @Override
    public boolean isShutdown() {
      return closed;
    }

    @Override
    public boolean isTerminated() {
      return closed;
    }

    @Override
    public boolean awaitTermination(long timeout, TimeUnit unit) {
      awaitTermination = true;
      return true;
    }
  }
}
