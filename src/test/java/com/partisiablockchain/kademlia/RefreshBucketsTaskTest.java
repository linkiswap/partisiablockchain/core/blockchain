package com.partisiablockchain.kademlia;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class RefreshBucketsTaskTest {

  private int nextPort = 2000;
  private final List<KademliaKey> lookups = new ArrayList<>();
  private final KademliaKey id = new KademliaKey();
  private final List<KademliaBucket> buckets =
      List.of(new KademliaBucket(), new KademliaBucket(), new KademliaBucket());
  private final RefreshBucketsTask task = new RefreshBucketsTask(id, buckets, lookups::add);

  @Test
  public void noNonEmpty() {
    task.run();

    Assertions.assertThat(lookups).isEmpty();
  }

  @Test
  public void firstNonEmpty() throws Exception {
    addAtDistance(0);
    task.run();

    Assertions.assertThat(lookups).hasSize(1);
    Assertions.assertThat(lookups.get(0).distance(this.id)).isEqualTo(0);
  }

  @Test
  public void secondNonEmpty() throws Exception {
    addAtDistance(1);
    task.run();

    Assertions.assertThat(lookups).hasSize(2);
    Assertions.assertThat(lookups.get(0).distance(this.id)).isEqualTo(1);
    Assertions.assertThat(lookups.get(1).distance(this.id)).isEqualTo(0);
  }

  private void addAtDistance(int distance) throws UnknownHostException {
    KademliaBucket bucket = buckets.get(distance);
    InetAddress localHost = InetAddress.getLocalHost();
    KademliaNode node = new KademliaNode(id.createForDistance(distance), localHost, nextPort++);
    bucket.add(node);
  }
}
