package com.partisiablockchain.flooding;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.ThreadedTestHelper;
import com.partisiablockchain.blockchain.contract.InteractWithContractTransaction;
import com.partisiablockchain.blockchain.transaction.CoreTransactionPart;
import com.partisiablockchain.blockchain.transaction.SignedTransaction;
import com.partisiablockchain.crypto.KeyPair;
import com.secata.stream.SafeDataOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;
import org.bouncycastle.tls.TlsClient;
import org.bouncycastle.tls.TlsClientProtocol;
import org.bouncycastle.tls.TlsProtocol;
import org.bouncycastle.tls.TlsServer;
import org.bouncycastle.tls.TlsServerProtocol;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

/** Test. */
public final class ConnectionTest {

  private Packet<?> transactionPacket;
  private MyInputStream inputStream;
  private MyOutputStream outputStream;
  private TlsProtocol tlsProtocol;
  private Connection connection;

  private Packet<?> received;

  /** Setup. */
  @BeforeEach
  public void setup() {
    transactionPacket = new Packet<>(Packet.Type.TRANSACTION, createTransaction());
    inputStream = new MyInputStream(packetBytes(transactionPacket));
    outputStream = new MyOutputStream();
    tlsProtocol = new TlsServerProtocol(inputStream, outputStream);
    connection =
        new Connection(
            (c, packet) -> received = packet,
            inputStream,
            outputStream,
            UUID.randomUUID().toString(),
            tlsProtocol);
  }

  @Test
  public void gracefulClose() {
    connection.send(transactionPacket);

    ThreadedTestHelper.waitForCondition(() -> outputStream.enteredWait);

    assertThat(connection.isAlive()).isTrue();

    ThreadedTestHelper.waitForCondition(() -> inputStream.enteredWait);
    ThreadedTestHelper.waitForCondition(() -> received != null);

    connection.close();
    assertThat(connection.isAlive()).isFalse();
    outputStream.run();

    Object payload = received.getPayload();
    assertThat(payload).isInstanceOf(SignedTransaction.class);
    SignedTransaction transaction = (SignedTransaction) payload;
    assertThat(transaction.getCore().getNonce()).isEqualTo(0);

    assertThat(inputStream.closeCalled).isTrue();
    assertThat(outputStream.closeCalled).isTrue();
    ThreadedTestHelper.waitForCondition(() -> outputStream.didInterrupt);
    ThreadedTestHelper.waitForCondition(() -> inputStream.didInterrupt);

    assertThat(tlsProtocol.isClosed()).isTrue();
  }

  @Test
  public void exceedQueueLimit() {
    // Add one extra packet since sender thread will take one
    for (int i = 0; i < Connection.MAX_OUTBOUND_QUEUE_SIZE + 1; i++) {
      connection.send(transactionPacket);
    }
    connection.send(transactionPacket);
    assertThat(connection.isAlive()).isFalse();
  }

  @Test
  public void closeIsOnlyCalledOnceByConnection() throws InterruptedException {
    // Add one extra packet since sender thread will take one
    for (int i = 0; i < Connection.MAX_OUTBOUND_QUEUE_SIZE + 1; i++) {
      connection.send(transactionPacket);
    }
    connection.send(transactionPacket);
    connection.send(transactionPacket);
    connection.send(transactionPacket);
    connection.send(transactionPacket);
    Thread.sleep(100);
    // Calls close twice, once from send via Connection and once by AutoCloseable
    assertThat(outputStream.timesCloseCalled).isEqualTo(2);
  }

  @Test
  public void serverConnectionCloseIfHandshakeFails() {
    TlsServerProtocol tlsProtocol = new TlsServerProtocol(inputStream, outputStream);

    TlsServer tlsServer = Mockito.mock(TlsServer.class);

    assertThatThrownBy(
            () ->
                Connection.createConnection(
                    (c, packet) -> received = packet,
                    "chainId",
                    tlsProtocol,
                    tlsServer,
                    TlsServerProtocol::accept))
        .isInstanceOf(NullPointerException.class);

    assertThat(inputStream.closeCalled).isTrue();
    assertThat(outputStream.closeCalled).isTrue();
    assertThat(tlsProtocol.isClosed()).isTrue();
  }

  @Test
  public void closeOnWrongChainId() throws IOException {
    inputStream = new MyInputStream(new byte[4]);

    outputStream.run();

    TlsClientProtocol tlsClientProtocol = Mockito.mock(TlsClientProtocol.class);
    Mockito.when(tlsClientProtocol.getInputStream()).thenReturn(inputStream);
    Mockito.when(tlsClientProtocol.getOutputStream()).thenReturn(outputStream);
    TlsClient tlsClient = Mockito.mock(TlsClient.class);

    assertThatThrownBy(
            () ->
                Connection.createConnection(
                    (c, packet) -> received = packet,
                    "chainId",
                    tlsClientProtocol,
                    tlsClient,
                    (protocol, peer) -> {}))
        .isInstanceOf(RuntimeException.class)
        .hasMessageContaining("Received invalid chainId. expected='chainId', received=''");

    Mockito.verify(tlsClientProtocol).close();
  }

  @Test
  public void clientConnectionCloseIfHandshakeFail() {
    TlsClientProtocol tlsProtocol = new TlsClientProtocol(inputStream, outputStream);

    TlsClient tlsClient = Mockito.mock(TlsClient.class);

    assertThatThrownBy(
            () ->
                Connection.createConnection(
                    (c, packet) -> received = packet,
                    "chainId",
                    tlsProtocol,
                    tlsClient,
                    TlsClientProtocol::connect))
        .isInstanceOf(NullPointerException.class);

    assertThat(inputStream.closeCalled).isTrue();
    assertThat(outputStream.closeCalled).isTrue();
    assertThat(tlsProtocol.isClosed()).isTrue();
  }

  private byte[] packetBytes(Packet<?>... packets) {
    return SafeDataOutputStream.serialize(
        stream -> {
          for (Packet<?> packet : packets) {
            packet.send(stream);
          }
        });
  }

  private SignedTransaction createTransaction() {
    return SignedTransaction.create(
            CoreTransactionPart.create(0, 0, 10),
            InteractWithContractTransaction.create(
                BlockchainAddress.fromString("000000000000000000000000000000000000000001"),
                new byte[] {1, 2, 3, 4}))
        .sign(new KeyPair(), "ConnectionTest");
  }

  private static final class MyInputStream extends InputStream {

    private boolean enteredWait;
    private boolean run;
    private int index = 0;
    private final byte[] bytes;
    private boolean closeCalled;
    private boolean didInterrupt;

    private MyInputStream(byte[] bytes) {
      this.bytes = bytes;
    }

    @Override
    public synchronized int read() {
      if (index < bytes.length) {
        byte value = bytes[index];
        index++;
        return Byte.toUnsignedInt(value);
      }
      enteredWait = true;
      try {
        while (!run) {
          this.wait(10 * 1000);
        }
      } catch (InterruptedException e) {
        this.didInterrupt = true;
        throw new RuntimeException(e);
      }
      this.didInterrupt = didInterrupt || Thread.currentThread().isInterrupted();
      return -1;
    }

    @Override
    public int read(byte[] b, int off, int len) throws IOException {
      return super.read(b, off, len);
    }

    @Override
    public void close() {
      closeCalled = true;
    }
  }

  private static final class MyOutputStream extends OutputStream {

    private final ByteArrayOutputStream output = new ByteArrayOutputStream();
    private boolean enteredWait;
    private boolean run;
    private boolean closeCalled;
    private int timesCloseCalled = 0;
    private boolean didInterrupt;

    private synchronized void run() {
      this.run = true;
      notifyAll();
    }

    private synchronized void waitForRun() {
      enteredWait = true;
      while (!run) {
        try {
          this.wait(10 * 1000);
        } catch (InterruptedException e) {
          didInterrupt = true;
        }
      }
      didInterrupt = didInterrupt || Thread.currentThread().isInterrupted();
    }

    @Override
    public void write(int b) {
      waitForRun();
      output.write(b);
    }

    @Override
    public void close() {
      timesCloseCalled++;
      closeCalled = true;
    }
  }
}
