package com.partisiablockchain.flooding;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import nl.jqno.equalsverifier.EqualsVerifier;
import org.junit.jupiter.api.Test;

/** Test. */
public final class AddressTest {

  // PMD does not allow hardcoded ips
  private static final String IP = "8.8" + ".8.8";

  @Test
  public void equals() {
    EqualsVerifier.forClass(Address.class).verify();
  }

  @Test
  public void testToString() {
    Address address = new Address(IP, 8321);
    assertThat(address.toString()).contains(IP).contains("8321");
  }

  @Test
  public void parseString() {
    String host = "MyNewHost.test";
    Address address = Address.parseAddress(host + ":" + "758");
    assertThat(address.getHost()).isEqualTo(host);
    assertThat(address.getPort()).isEqualTo(758);
  }
}
