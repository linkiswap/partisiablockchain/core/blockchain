package com.partisiablockchain.flooding;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.CloseableTest;
import com.partisiablockchain.ThreadedTestHelper;
import com.partisiablockchain.blockchain.BlockchainTestHelper;
import com.partisiablockchain.blockchain.FinalBlock;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.flooding.NetworkNode.IncomingPacket;
import com.secata.tools.coverage.FunctionUtility;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigInteger;
import java.net.Socket;
import java.time.Duration;
import java.util.List;
import java.util.UUID;
import java.util.function.BooleanSupplier;
import java.util.function.Consumer;
import org.bouncycastle.tls.TlsProtocol;
import org.bouncycastle.tls.TlsServerProtocol;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

/** Test. */
public final class NetworkNodeTest extends CloseableTest {

  private NetworkNode networkNode;
  private FinalBlock block;
  private String chainId;

  private final KeyPair keyPair1 = new KeyPair(BigInteger.ONE);
  private final KeyPair keyPair2 = new KeyPair(BigInteger.TWO);

  /** Setup default. */
  @BeforeEach
  @SuppressWarnings("FutureReturnValueIgnored")
  public void setup() throws IOException {
    BlockchainTestHelper blockchainTestHelper = new BlockchainTestHelper(temporaryFolder);
    register(blockchainTestHelper.blockchainLedger);
    block = blockchainTestHelper.createFinalBlock(new KeyPair(BigInteger.TEN), List.of());
    chainId = UUID.randomUUID().toString();
    networkNode = createTestNode(this);
  }

  /** Creates a test version of ther network node for use in unit test. */
  private NetworkNode createTestNode(CloseableTest registration) throws IOException {
    NetworkNode networkNode =
        new NetworkNode(
            ObjectCreator.noOpServer(registration),
            ObjectCreator.noOpClient(),
            chainId,
            keyPair1,
            FunctionUtility.noOpConsumer());
    registration.register(networkNode);
    return networkNode;
  }

  @Test
  public void closeConnectionOnReconnectAndInClose() {
    Address address = new Address("localhost", 7777);
    InputStream inputStream = new ByteArrayInputStream(new byte[2]);
    OutputStream outputStream = new ByteArrayOutputStream(2);
    TlsProtocol tlsProtocol = new TlsServerProtocol(inputStream, outputStream);
    Connection firstConnection =
        new Connection(
            (c, p) -> {},
            ObjectCreator.blockingInput(this),
            ObjectCreator.blockingOutput(this),
            chainId,
            tlsProtocol);
    networkNode.create(address, firstConnection);
    assertThat(networkNode.get(address).isConnected()).isTrue();
    assertThat(firstConnection.isAlive()).isTrue();

    Connection secondConnection =
        new Connection(
            (c, p) -> {},
            ObjectCreator.blockingInput(this),
            ObjectCreator.blockingOutput(this),
            chainId,
            tlsProtocol);
    networkNode.create(address, secondConnection);
    assertThat(networkNode.get(address).isConnected()).isTrue();
    assertThat(firstConnection.isAlive()).isFalse();
    assertThat(secondConnection.isAlive()).isTrue();

    assertThat(networkNode.getServer().isClosed()).isFalse();
    assertThat(networkNode.getClient().isClosed()).isFalse();
    networkNode.close();
    assertThat(networkNode.getServer().isClosed()).isTrue();
    assertThat(networkNode.getClient().isClosed()).isTrue();

    assertThat(networkNode.get(address)).isNull();
  }

  @Test
  public void getLiveConnections() throws InterruptedException {
    final Address firstAddress = new Address("localhost", ObjectCreator.port());
    final Address secondAddress = new Address("localhost", ObjectCreator.port());

    PacketReceiver incoming1 = new PacketReceiver();
    final NetworkNode node1 =
        new NetworkNode(
            new NetworkConfig(firstAddress, FunctionUtility.nullSupplier(), secondAddress),
            chainId,
            new KeyPair(BigInteger.TEN),
            incoming1);
    register(node1);
    PacketReceiver incoming2 = new PacketReceiver();
    final NetworkNode node2 =
        new NetworkNode(
            new NetworkConfig(secondAddress, FunctionUtility.nullSupplier(), null),
            chainId,
            keyPair2,
            incoming2);
    register(node2);

    // Wait for sync packets
    waitForSync(incoming2);
    waitForSync(incoming1);

    assertThat(node1.getLiveConnections().size()).isEqualTo(1);
    node2.close();
    Thread.sleep(100);
    assertThat(node1.getLiveConnections().size()).isEqualTo(0);
  }

  @Test
  @SuppressWarnings("unchecked")
  public void queueOutgoingNonFlood() {
    Address firstAddress = new Address("localhost", ObjectCreator.port());
    Address secondAddress = new Address("localhost", ObjectCreator.port());

    PacketReceiver incoming2 = new PacketReceiver();
    final NetworkNode node2 =
        new NetworkNode(
            new NetworkConfig(secondAddress, FunctionUtility.nullSupplier(), firstAddress),
            chainId,
            keyPair2,
            incoming2);
    register(node2);

    PacketReceiver incoming1 = new PacketReceiver();
    final NetworkNode node1 =
        new NetworkNode(
            new NetworkConfig(firstAddress, FunctionUtility.nullSupplier(), null),
            chainId,
            keyPair1,
            incoming1);
    register(node1);

    // Wait for sync packets
    waitForSync(incoming2);
    waitForSync(incoming1);

    long blockTime = 7;
    node1.sendToAny(new Packet<>(Packet.Type.BLOCK_REQUEST, new BlockRequest(blockTime)));

    ThreadedTestHelper.waitForCondition(incoming2, Duration.ofSeconds(1));

    assertThat(incoming2.packet.getPacket().getType()).isEqualTo(Packet.Type.BLOCK_REQUEST);
    Packet<BlockRequest> packet = (Packet<BlockRequest>) incoming2.packet.getPacket();
    assertThat(packet.getPayload().getBlockTime()).isEqualTo(blockTime);
  }

  @Test
  void emptyNode() {
    networkNode.sendToAny(new Packet<>(Packet.Type.BLOCK_REQUEST, new BlockRequest(0)));
  }

  @Test
  public void queueOutgoingFlood() {
    final Address firstAddress = new Address("localhost", ObjectCreator.port());
    final Address secondAddress = new Address("localhost", ObjectCreator.port());
    final Address thirdAddress = new Address("localhost", ObjectCreator.port());

    PacketReceiver incoming1 = new PacketReceiver();
    final NetworkNode node1 =
        new NetworkNode(
            new NetworkConfig(firstAddress, FunctionUtility.nullSupplier(), null),
            chainId,
            keyPair1,
            incoming1);
    register(node1);
    PacketReceiver incoming2 = new PacketReceiver();
    final NetworkNode node2 =
        new NetworkNode(
            new NetworkConfig(secondAddress, FunctionUtility.nullSupplier(), firstAddress),
            chainId,
            keyPair2,
            incoming2);
    register(node2);

    // Wait for sync packets
    waitForSync(incoming2);
    waitForSync(incoming1);

    Packet<FinalBlock> sentPacket = new Packet<>(Packet.Type.BLOCK, block);
    node1.sendToAll(sentPacket);

    waitForAndReceiveBlock(incoming2);

    PacketReceiver incoming3 = new PacketReceiver();
    final NetworkNode node3 =
        new NetworkNode(
            new NetworkConfig(thirdAddress, FunctionUtility.nullSupplier(), firstAddress),
            chainId,
            new KeyPair(),
            incoming3);
    register(node3);

    node1.sendToAll(sentPacket);
    assertThatThrownBy(() -> waitForAndReceiveBlock(incoming3)).isInstanceOf(AssertionError.class);
  }

  @Test
  public void connectedNodes() {
    Address firstAddress = new Address("localhost", ObjectCreator.port());
    Address secondAddress = new Address("localhost", ObjectCreator.port());
    PacketReceiver incoming1 = new PacketReceiver();
    final NetworkNode node1 =
        new NetworkNode(
            new NetworkConfig(firstAddress, FunctionUtility.nullSupplier(), null),
            chainId,
            keyPair1,
            incoming1);
    register(node1);
    PacketReceiver incoming2 = new PacketReceiver();
    final NetworkNode node2 =
        new NetworkNode(
            new NetworkConfig(secondAddress, FunctionUtility.nullSupplier(), firstAddress),
            chainId,
            keyPair2,
            incoming2);
    register(node2);

    // Wait for sync packets
    waitForSync(incoming1);
    waitForSync(incoming2);

    // The next two lines ensures that nothing is left in the network nodes, since we can
    // send directly two and from
    sendBlock(node1, incoming2);
    sendBlock(node2, incoming1);
  }

  @Test
  public void handshakeFails() throws IOException {
    Address toConnect = new Address("localhost", 4578);
    Socket socket = Mockito.mock(Socket.class);
    Mockito.when(socket.getInputStream()).thenThrow(new IOException("69"));

    networkNode.setClientConnectedAddress(toConnect, socket);
    assertThat(networkNode.get(toConnect)).isNull();

    networkNode.setServerConnectedAddress(toConnect, socket);
    assertThat(networkNode.get(toConnect)).isNull();
  }

  private void waitForSync(PacketReceiver receiver) {
    ThreadedTestHelper.waitForCondition(receiver, Duration.ofSeconds(2));
    assertThat(receiver.packet.getPacket().getPayload()).isInstanceOf(SyncRequest.class);
  }

  private void sendBlock(NetworkNode sender, PacketReceiver packetReceiver) {
    sender.sendToAll(new Packet<>(Packet.Type.BLOCK, block));

    waitForAndReceiveBlock(packetReceiver);
  }

  private void waitForAndReceiveBlock(PacketReceiver packetReceiver) {
    ThreadedTestHelper.waitForCondition(packetReceiver, Duration.ofSeconds(1));

    Packet<?> packet = packetReceiver.packet.getPacket();
    assertThat(packet.getPayload()).isInstanceOf(FinalBlock.class);
    FinalBlock response = (FinalBlock) packet.getPayload();
    assertThat(response.getBlock()).isEqualTo(block.getBlock());
  }

  private static final class PacketReceiver
      implements Consumer<NetworkNode.IncomingPacket>, BooleanSupplier {

    private IncomingPacket packet;

    private PacketReceiver() {}

    @Override
    public boolean getAsBoolean() {
      return packet != null;
    }

    @Override
    public void accept(IncomingPacket incomingPacket) {
      packet = incomingPacket;
    }
  }
}
