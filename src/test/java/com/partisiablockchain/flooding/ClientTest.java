package com.partisiablockchain.flooding;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ScheduledExecutorService;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

/** Test. */
public final class ClientTest {

  private final ScheduledExecutorService mock = Mockito.mock(ScheduledExecutorService.class);
  private final List<Address> connected = new ArrayList<>();
  private long kademliaPicks = 0;
  private int connectedCalls = 0;
  private final Address first = new Address("localhost", 8080);
  private final Address second = new Address("localhost", 8081);
  private Address toReturn = null;
  private final Client client =
      new Client(
          mock,
          () -> {
            kademliaPicks++;
            return toReturn;
          },
          null,
          () -> connected,
          (address, socket) -> connectedCalls++);

  public ClientTest() {}

  @Test
  public void randomInterval() {
    long l = Client.randomInterval(100, 100);
    assertThat(l).isEqualTo(100);

    assertThat(Client.randomInterval(1000, 1500)).isBetween(1000L, 1500L);
  }

  @Test
  public void connectIfUnconnected() {
    assertThat(client.connectIfUnconnected(connected, null)).isFalse();
    assertThat(connected).isEmpty();
    connected.add(second);
    assertThat(client.connectIfUnconnected(connected, first)).isTrue();
    assertThat(connected).containsExactly(second, first);
    assertThat(client.connectIfUnconnected(connected, first)).isFalse();
    assertThat(connected).containsExactly(second, first);
  }

  @Test
  public void shouldTryToPick20Times() {
    for (int i = 0; i < Client.MAX_NEIGHBOURS - 1; i++) {
      connected.add(new Address("localhost", 8080 + i));
    }

    client.checkForConnect();

    assertThat(kademliaPicks).isEqualTo(20);
  }

  @Test
  public void shouldPickOnceWhenInitiatingConnection() {
    for (int i = 0; i < Client.MAX_NEIGHBOURS - 1; i++) {
      connected.add(new Address("localhost", 8080 + i));
    }
    toReturn = new Address("localhost", 9999);

    client.checkForConnect();

    assertThat(kademliaPicks).isEqualTo(1);
  }
}
