package com.partisiablockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.secata.tools.coverage.ExceptionConverter;
import java.time.Duration;
import java.util.Locale;
import java.util.Objects;
import java.util.function.BooleanSupplier;
import java.util.function.Predicate;
import java.util.function.Supplier;
import org.assertj.core.api.Assertions;

/** Test. */
public final class ThreadedTestHelper {

  /**
   * Wait up to 10 seconds for the supplier to return true. Sleeps a bit between queries. Should be
   * used for tests that are dependent on something happening in another thread.
   *
   * <p>If the method returns successfully it means the supplier returned true in the last
   * invocation.
   */
  public static void waitForCondition(BooleanSupplier supplier) {
    waitForCondition(supplier, Duration.ofSeconds(60));
  }

  /**
   * Wait up to a specified amount of milliseconds for the supplier to return true. Sleeps a bit
   * between queries. Should be used for tests that are dependent on something happening in another
   * thread.
   *
   * <p>If the method returns successfully it means the supplier returned true in the last
   * invocation.
   */
  public static void waitForCondition(BooleanSupplier supplier, Duration waitTime) {
    waitForCondition(supplier, waitTime.toMillis(), null);
  }

  /**
   * Wait up to a specified duration for the supplier to return true. Sleeps a bit between queries.
   * Should be used for tests that are dependent on something happening in another thread.
   *
   * <p>If the method returns successfully it means the supplier returned true in the last
   * invocation.
   */
  public static void waitForCondition(BooleanSupplier supplier, long waitTime) {
    waitForCondition(supplier, waitTime, null);
  }

  /**
   * Wait up to a specified amount of seconds for the supplier to return true. Sleeps a bit between
   * queries. Should be used for tests that are dependent on something happening in another thread.
   *
   * <p>If the method returns successfully it means the supplier returned true in the last
   * invocation. If the supplier returns false the assertion's reason will be the supplied message.
   */
  public static void waitForCondition(BooleanSupplier supplier, long waitTime, String message) {
    waitForCondition(supplier::getAsBoolean, bool -> !bool, waitTime, message);
  }

  /**
   * Wait up to a specified amount of seconds for the supplier to return true. Sleeps a bit between
   * queries. Should be used for tests that are dependent on something happening in another thread.
   *
   * <p>If the method returns successfully it means the value supplied by the value updater makes
   * the shouldWait predicate return false.
   *
   * @return the value supplied by the valueUpdater
   */
  public static <T> T waitForCondition(
      Supplier<T> valueUpdater, Predicate<T> shouldWait, long waitTime, String message) {
    Predicate<T> actualWait = ((Predicate<T>) Objects::isNull).or(shouldWait);
    long timeout = System.currentTimeMillis() + waitTime;
    T value;
    do {
      ExceptionConverter.run(() -> Thread.sleep(50), "Unable to sleep");
      try {
        value = valueUpdater.get();
      } catch (Exception e) {
        value = null;
      }
    } while (System.currentTimeMillis() < timeout && actualWait.test(value));

    String description =
        Objects.requireNonNullElse(message, "Timeout")
            + " ("
            + String.format(Locale.US, "%,d", waitTime).replace(",", " ")
            + "ms)";
    Assertions.assertThat(actualWait.test(value)).describedAs(description).isFalse();

    return value;
  }

  /** Wait until the supplier does not throw an exception and returns a non null object. */
  public static <T> T waitForPresent(Supplier<T> supplier, long waitTime, String message) {
    return waitForCondition(
        supplier,
        ignored -> false,
        waitTime,
        Objects.requireNonNullElse(message, "Object was never ready"));
  }
}
