package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.serialization.SerializationResult;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.serialization.StateSerializer;

/** Utility class to handle storage transfer when routing to a new shard. */
final class StorageTransferUtil {

  private final StateSerializer incomingStorage;
  private final StateSerializer localFreshStorage;

  /**
   * Constructor to create a new storage transfer utility.
   *
   * @param incomingStorage state serializer to read the incoming data
   * @param localFreshStorage state serializer to write the incoming data to fresh storage
   */
  StorageTransferUtil(StateSerializer incomingStorage, StateSerializer localFreshStorage) {
    this.incomingStorage = incomingStorage;
    this.localFreshStorage = localFreshStorage;
  }

  /**
   * Transfers data between storages and handles references.
   *
   * @param hash a hash of the data being transferred
   * @param type the class of the data being transferred
   * @param <T> the type of Class used for type
   * @return the data read from the fresh storage
   */
  <T extends StateSerializable> T transferBetweenStorage(Hash hash, Class<T> type) {
    T accountState = incomingStorage.read(hash, type);
    SerializationResult write = localFreshStorage.write(accountState);
    return localFreshStorage.read(write.hash(), type);
  }
}
