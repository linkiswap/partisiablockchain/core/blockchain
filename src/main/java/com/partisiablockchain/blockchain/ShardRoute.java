package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.util.DataStreamLimit;
import com.secata.stream.DataStreamSerializable;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.stream.SafeListStream;

/** Unique routing to a shard. */
public final class ShardRoute implements DataStreamSerializable {

  /** Id of target shard. */
  public final String targetShard;
  /** Nonce for route. */
  public final long nonce;

  /**
   * Default constructor.
   *
   * @param targetShard id of target shard
   * @param nonce current nonce
   */
  public ShardRoute(String targetShard, long nonce) {
    this.targetShard = targetShard;
    this.nonce = nonce;
  }

  /**
   * Read shard route from stream.
   *
   * @param stream to read from
   * @return shard route read from stream
   */
  public static ShardRoute read(SafeDataInputStream stream) {
    return new ShardRoute(stream.readOptional(DataStreamLimit::readString), stream.readLong());
  }

  @Override
  public void write(SafeDataOutputStream stream) {
    stream.writeOptional(SafeListStream.primitive(SafeDataOutputStream::writeString), targetShard);
    stream.writeLong(nonce);
  }

  @Override
  public String toString() {
    return "ShardRoute{" + "targetShard='" + targetShard + '\'' + ", nonce=" + nonce + '}';
  }
}
