package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.blockchain.transaction.EventTransaction;
import com.partisiablockchain.blockchain.transaction.ExecutableEvent;
import com.partisiablockchain.blockchain.transaction.ExecutionContext;
import com.partisiablockchain.blockchain.transaction.SignedTransaction;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Queue;
import java.util.function.Predicate;

final class TransactionProcessor {

  private final ExecutionContext context;
  private final List<ExecutedTransactionEvents> results;
  private final Queue<ExecutableEvent> localEvents;
  private final Block block;
  private final MutableChainState state;

  public TransactionProcessor(
      Block block, MutableChainState nextState, String chainId, String subChainId) {
    this.state = nextState;
    this.block = block;
    this.results = new ArrayList<>();
    this.localEvents = new ArrayDeque<>();
    this.context =
        ExecutionContext.init(chainId, subChainId, block, nextState.getGovernanceVersion());
  }

  void processTransactionInSync(ExecutableEvent executableEvent) {
    process(context.executeInSync(state, executableEvent));
  }

  void processTransaction(ExecutableEvent executableEvent) {
    EventTransaction.InnerEvent innerEvent = executableEvent.getEvent().getInner();
    ExecutedTransactionEvents events;
    if (innerEvent instanceof EventTransaction.ContractInnerEvent contractEvent) {
      boolean iced = state.updateActiveContract(block, contractEvent.target());
      events = context.execute(state, executableEvent, iced);
    } else {
      events = context.execute(state, executableEvent);
    }
    process(events);
  }

  void processTransaction(SignedTransaction transaction) {
    process(context.execute(state, transaction));
  }

  private void process(ExecutedTransactionEvents result) {
    List<ExecutableEvent> ownEvents = result.ownEvents();
    localEvents.addAll(ownEvents);
    results.add(result);
  }

  public void processLocalEvents(Predicate<ExecutableEvent> isImmediatelyExecutable) {
    while (!localEvents.isEmpty() && isImmediatelyExecutable.test(localEvents.peek())) {
      ExecutableEvent localEvent = localEvents.remove();
      processTransaction(localEvent);
    }
  }

  public List<ExecutedTransactionEvents> resultingEvents() {
    return results;
  }

  public Collection<ExecutableEvent> localEvents() {
    return localEvents;
  }
}
