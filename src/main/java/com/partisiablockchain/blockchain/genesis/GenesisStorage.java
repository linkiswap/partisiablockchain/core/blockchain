package com.partisiablockchain.blockchain.genesis;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.blockchain.FinalBlock;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.serialization.StateStorage;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.coverage.WithResource;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

/** Storage that persist everything in memory to allow saving it as a single genesis file. */
public final class GenesisStorage implements StateStorage {

  private final Map<Hash, byte[]> values = new HashMap<>();

  /**
   * Initialize the blockchain storage from the supplied genesis file.
   *
   * @param file zip containing genesis data
   * @param destination the state storage that should be populated with genesis states
   * @return the genesis block
   */
  public static FinalBlock initialize(File file, StateStorage destination) {
    return WithResource.apply(
        () -> new ZipFile(file),
        zipFile -> {
          ZipEntry genesisEntry = zipFile.getEntry("genesisBlock");
          FinalBlock genesisBlock =
              WithResource.apply(
                  () -> zipFile.getInputStream(genesisEntry),
                  stream -> FinalBlock.read(new SafeDataInputStream(stream)),
                  "Unable to read genesis block");
          zipFile.stream()
              .filter(entry -> !entry.getName().equals("genesisBlock"))
              .forEach(
                  entry -> {
                    byte[] data =
                        WithResource.apply(
                            () -> zipFile.getInputStream(entry),
                            InputStream::readAllBytes,
                            "unable to read zip entry");
                    destination.write(Hash.fromString(entry.getName()), s -> s.write(data));
                  });
          return genesisBlock;
        },
        "Unable to read zip file");
  }

  /**
   * Create a genesis file with all relevant states and the supplied genesis block.
   *
   * @param destination file for the genesis data
   * @param genesisBlock the genesis block
   */
  public synchronized void persistGenesis(File destination, FinalBlock genesisBlock) {
    WithResource.accept(
        () -> new ZipOutputStream(new FileOutputStream(destination)),
        zip -> {
          zip.putNextEntry(new ZipEntry("genesisBlock"));
          genesisBlock.write(new SafeDataOutputStream(zip));
          for (Map.Entry<Hash, byte[]> entry : values.entrySet()) {
            zip.putNextEntry(new ZipEntry(entry.getKey().toString()));
            zip.write(entry.getValue());
          }
        },
        "Unable to persis genesis state");
  }

  @Override
  public synchronized boolean write(Hash hash, Consumer<SafeDataOutputStream> writer) {
    if (values.containsKey(hash)) {
      return false;
    }
    values.put(hash, SafeDataOutputStream.serialize(writer));
    return true;
  }

  @Override
  public synchronized <S> S read(Hash hash, Function<SafeDataInputStream, S> reader) {
    if (values.containsKey(hash)) {
      SafeDataInputStream bytes = SafeDataInputStream.createFromBytes(values.get(hash));
      return reader.apply(bytes);
    } else {
      return null;
    }
  }
}
