package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.binder.pub.PubBinderContract;
import com.partisiablockchain.binder.sys.SysBinderContract;
import com.partisiablockchain.binder.zk.ZkBinderContract;
import com.partisiablockchain.blockchain.contract.binder.PubBlockchainContract;
import com.partisiablockchain.blockchain.contract.binder.SysBlockchainContract;
import com.partisiablockchain.blockchain.contract.binder.ZkBlockchainContract;
import com.secata.tools.coverage.ExceptionConverter;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.jar.JarEntry;
import java.util.jar.JarInputStream;

/** Class loader for jars deployed on pbc. */
public final class JarClassLoader extends ClassLoader {

  private final Map<String, Class<?>> loadedClasses = new HashMap<>();
  private final Map<String, byte[]> definedEntries;
  private final ClassLoader defaultClassLoader;
  private final Class<?> mainClass;
  private final Set<String> systemLoadedNamespaces;

  /**
   * Create builder for jar class loader.
   *
   * @param jar as bytes
   * @return builder
   */
  public static Builder builder(byte[] jar) {
    return new Builder(jar);
  }

  private JarClassLoader(
      byte[] jar, ClassLoader defaultClassLoader, Set<String> systemLoadedNamespaces) {
    this.defaultClassLoader = defaultClassLoader;
    this.systemLoadedNamespaces = Collections.unmodifiableSet(systemLoadedNamespaces);

    // Read entries from Jar stream
    this.definedEntries =
        ExceptionConverter.call(
            () -> readEntriesToMap(new JarInputStream(new ByteArrayInputStream(jar))),
            "Unable to read jar");

    // Determine main class
    final byte[] mainClassBytes = definedEntries.get("main");
    if (mainClassBytes == null) {
      throw new IllegalArgumentException(
          "The jar file should contain a \"main\" resource to specify the main class.\n"
              + "    Jar length: "
              + jar.length
              + " bytes\n    Found: "
              + definedEntries.keySet());
    }

    final String mainClass = new String(mainClassBytes, StandardCharsets.UTF_8).trim();
    if (!definedEntries.containsKey(nameToPath(mainClass))) {
      throw new IllegalArgumentException(
          "The jar file should contain the default main class: "
              + mainClass
              + "\n    Jar length: "
              + jar.length
              + " bytes\n    Found: "
              + definedEntries.keySet());
    }

    // Load main class
    this.mainClass =
        ExceptionConverter.call(
            () -> loadClass(mainClass), "Unable to load specified main: " + mainClass);
  }

  private static Map<String, byte[]> readEntriesToMap(final JarInputStream inputStream)
      throws IOException {
    final var entries = new HashMap<String, byte[]>();
    JarEntry entry;
    while ((entry = inputStream.getNextJarEntry()) != null) {
      if (!entry.isDirectory()) {
        entries.put(entry.getName(), inputStream.readAllBytes());
      }
    }
    return Map.copyOf(entries);
  }

  @Override
  public InputStream getResourceAsStream(String name) {
    if (!definedEntries.containsKey(name)) {
      return null;
    }
    return new ByteArrayInputStream(definedEntries.get(name));
  }

  Class<?> getMainClass() {
    return mainClass;
  }

  Set<String> getSystemLoadedNamespaces() {
    return systemLoadedNamespaces;
  }

  /**
   * Reads a public binder and a public contract.
   *
   * @param binder the binder to use
   * @return the actual contract to be run on the chain
   */
  static PubBlockchainContract<?> readPubBlockchainContract(Object binder) {
    return new PubBlockchainContract<>((PubBinderContract<?>) binder);
  }

  /**
   * Reads a system binder and a system contract.
   *
   * @param binder the binder to use
   * @return the actual contract to be run on the chain
   */
  static SysBlockchainContract<?> readSysBlockchainContract(Object binder) {
    return new SysBlockchainContract<>((SysBinderContract<?>) binder);
  }

  /**
   * Reads a tace binder and a tace contract.
   *
   * @param binder the binder to use
   * @return the actual contract to be run on the chain
   */
  static ZkBlockchainContract<?> readZkBlockchainContract(Object binder) {
    return new ZkBlockchainContract<>((ZkBinderContract<?>) binder);
  }

  @Override
  public synchronized Class<?> loadClass(String name) throws ClassNotFoundException {
    boolean useSystemClassLoader = systemLoadedNamespaces.stream().anyMatch(name::startsWith);
    if (useSystemClassLoader) {
      try {
        return defaultClassLoader.loadClass(name);
      } catch (ClassNotFoundException e) {
        return fallbackLoadClass(name);
      }
    } else {
      return fallbackLoadClass(name);
    }
  }

  private synchronized Class<?> fallbackLoadClass(String name) throws ClassNotFoundException {
    String path = nameToPath(name);
    if (definedEntries.containsKey(path)) {
      if (!loadedClasses.containsKey(path)) {
        byte[] bytes = definedEntries.get(path);
        loadedClasses.put(path, defineClass(name, bytes, 0, bytes.length));
      }
      return loadedClasses.get(path);
    } else {
      return defaultClassLoader.loadClass(name);
    }
  }

  private static String nameToPath(String name) {
    return name.replace('.', '/').concat(".class");
  }

  Object instantiateBinder(Class<?> clazz, Object parameter) {
    return instantiateMainClass(clazz, parameter);
  }

  private Object instantiateMainClass(Class<?> parameterType, Object parameter) {
    return ExceptionConverter.call(
        () -> {
          Constructor<?> declaredConstructor = mainClass.getDeclaredConstructor(parameterType);
          return declaredConstructor.newInstance(parameter);
        },
        "Unable to instantiate handler: " + mainClass.getName());
  }

  Object instantiateMainClass() {
    return instantiateClass(mainClass);
  }

  static <T> T instantiateClass(Class<T> mainClass) {
    return ExceptionConverter.call(
        () -> mainClass.getDeclaredConstructor().newInstance(),
        "Unable to instantiate handler: " + mainClass.getName());
  }

  /** Builder for JarClassLoader. */
  public static final class Builder {

    private final byte[] jar;
    private final Set<String> systemClassLoadingNamespaces = new HashSet<>();
    private ClassLoader defaultClassLoader = JarClassLoader.class.getClassLoader();

    private Builder(byte[] jar) {
      this.jar = jar;
    }

    /**
     * Set default class loader.
     *
     * @param defaultClassLoader to set
     * @return builder
     */
    @SuppressWarnings("CanIgnoreReturnValueSuggester")
    public Builder defaultClassLoader(ClassLoader defaultClassLoader) {
      this.defaultClassLoader = defaultClassLoader;
      return this;
    }

    /**
     * Add namespace for loading system classes.
     *
     * @param namespace to be added
     * @return builder
     */
    @SuppressWarnings("CanIgnoreReturnValueSuggester")
    public Builder addSystemClassLoadingNamespace(String namespace) {
      this.systemClassLoadingNamespaces.add(namespace);
      return this;
    }

    /**
     * Build this builder.
     *
     * @return the created JarClassLoader
     */
    public JarClassLoader build() {
      return new JarClassLoader(jar, defaultClassLoader, systemClassLoadingNamespaces);
    }
  }
}
