package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.blockchain.contract.CoreContractState;
import com.partisiablockchain.blockchain.transaction.CallbackCreator;
import com.partisiablockchain.blockchain.transaction.InnerSystemEvent;
import com.partisiablockchain.blockchain.transaction.ReturnEnvelope;
import com.partisiablockchain.contract.CallbackContext;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.serialization.LargeByteArray;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.tree.AvlTree;
import com.secata.stream.SafeDataInputStream;
import com.secata.tools.immutable.FixedList;
import java.util.Collection;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.stream.IntStream;

/** The state of a contract. */
@SuppressWarnings("ConstantConditions")
@Immutable
public final class ContractState implements StateSerializable {

  private final CoreContractState coreContractState;
  /** Hash of the inner state object. */
  private final Hash stateHash;
  /** Byte count for the inner state object. */
  private final long byteCount;

  private final AvlTree<Hash, InvokeWithCallback> callbacks;

  @SuppressWarnings("unused")
  ContractState() {
    this.coreContractState = null;
    this.stateHash = null;
    this.byteCount = 0;
    this.callbacks = null;
  }

  private ContractState(
      CoreContractState coreContractState,
      Hash stateHash,
      long byteCount,
      AvlTree<Hash, InvokeWithCallback> callbacks) {
    this.coreContractState = coreContractState;
    this.stateHash = stateHash;
    this.byteCount = byteCount;
    this.callbacks = callbacks;
  }

  /**
   * Create a new contract state.
   *
   * @param coreContractState the core contract state
   * @return the contract state created
   */
  public static ContractState create(CoreContractState coreContractState) {
    return new ContractState(coreContractState, null, -1, AvlTree.create());
  }

  long calculateAllocatedCostForPendingCallbacks() {
    long accumulatedCost = 0;
    for (InvokeWithCallback invokedCallback : callbacks.values()) {
      for (CallbackInfo callbackInfo : invokedCallback.callbacks.values()) {
        accumulatedCost += callbackInfo.allocatedCost;
      }
    }
    return accumulatedCost;
  }

  ContractState withHashAndSize(Hash hash, long byteCount) {
    return new ContractState(coreContractState, hash, byteCount, callbacks);
  }

  ContractState withCore(CoreContractState core) {
    return new ContractState(core, stateHash, byteCount, callbacks);
  }

  CoreContractState getCore() {
    return coreContractState;
  }

  long computeStorageLength() {
    return coreContractState.computeStorageLength() + byteCount;
  }

  /**
   * Get the hash reference to the actual state of the contract. This is stored in storage with this
   * hash as the identifier.
   *
   * @return The hash identifying the actual contract state
   */
  Hash getStateHash() {
    return stateHash;
  }

  /**
   * Adds a list of callbacks to this contract, whenever the transaction with a hash is completed,
   * an event is sent to this contract with this byte array as invocation.
   *
   * @param returnEnvelope the callback that would await all the event transactions and then be
   *     called
   * @param from the sender of transaction that registers callbacks
   * @param sendingTransaction the transaction that registers callbacks
   * @param callbackInfo the list of callback info to check
   * @return the updated contract state
   */
  ContractState withCallbacks(
      ReturnEnvelope returnEnvelope,
      BlockchainAddress from,
      Hash sendingTransaction,
      CallbackInfo callbackInfo) {
    AvlTree<Hash, CallbackInfo> tree = AvlTree.create();
    tree = tree.set(callbackInfo.getCallbackIdentifier(), callbackInfo);
    InvokeWithCallback invokeWithCallback =
        new InvokeWithCallback(returnEnvelope, from, sendingTransaction, tree);
    return new ContractState(
        coreContractState,
        stateHash,
        byteCount,
        this.callbacks.set(sendingTransaction, invokeWithCallback));
  }

  /**
   * Handles a callback from an event transaction to this contract.
   *
   * @param completedEventTransaction the event transaction
   * @param success whether the transaction succeeded
   * @return result object with new contract state, rpc for callback and global callback info
   */
  CallbackResult acknowledgeCallback(
      Hash completedEventTransaction, boolean success, byte[] returnValue) {
    AvlTree<Hash, InvokeWithCallback> callbacks = this.callbacks;
    Hash sendingTransaction = findSendingTransactionForEvent(completedEventTransaction);
    InvokeWithCallback invokeWithCallback = callbacks.getValue(sendingTransaction);
    invokeWithCallback =
        invokeWithCallback.completeEvent(completedEventTransaction, success, returnValue);
    callbacks = callbacks.set(sendingTransaction, invokeWithCallback);
    ContractState contractState =
        new ContractState(coreContractState, stateHash, byteCount, callbacks);

    CallbackInfo callbackInfo = invokeWithCallback.findCallbackInfo(completedEventTransaction);
    if (callbackInfo.isEveryTransactionResolved()) {
      CallbackCreator.Callback callback =
          new CallbackCreator.Callback(
              callbackInfo.callbackIdentifier,
              invokeWithCallback.from,
              callbackInfo.allocatedCost,
              callbackInfo.callbackRpc);
      return new CallbackResult(contractState, callback);
    } else {
      return new CallbackResult(contractState, null);
    }
  }

  ContractState callbackResult(Hash callbackIdentifier, boolean success) {
    Hash sendingTransaction = findSendingTransactionForCallback(callbackIdentifier);
    InvokeWithCallback callback = callbacks.getValue(sendingTransaction);
    CallbackInfo value = callback.callbacks.getValue(callbackIdentifier);
    List<CallbackInfo.CompletedTransaction> values = value.completedTransactionTree.values();
    for (CallbackInfo.CompletedTransaction transaction : values) {
      if (ExecutionResult.Unknown.equals(transaction.executionResult)) {
        throw new IllegalStateException("Cannot set result when events are not processed");
      }
    }

    InvokeWithCallback invokeWithCallback = callback.callbackResult(callbackIdentifier, success);
    return new ContractState(
        coreContractState,
        stateHash,
        byteCount,
        callbacks.set(sendingTransaction, invokeWithCallback));
  }

  ContractState callbackUpdated(Hash oldCallbackIdentifier, CallbackInfo newCallback) {
    Hash sendingTransaction = findSendingTransactionForCallback(oldCallbackIdentifier);
    InvokeWithCallback invokeWithCallback =
        callbacks.getValue(sendingTransaction).updateCallback(newCallback);
    return new ContractState(
        coreContractState,
        stateHash,
        byteCount,
        callbacks.set(sendingTransaction, invokeWithCallback));
  }

  ContractState callbackComplete(
      Hash callbackIdentifier,
      Consumer<InnerSystemEvent.CallbackEvent> eventManager,
      byte[] returnValue) {
    Hash sendingTransaction = findSendingTransactionForCallback(callbackIdentifier);
    InvokeWithCallback invokeWithCallback = callbacks.getValue(sendingTransaction);
    if (invokeWithCallback.everyCallbackComplete()) {
      invokeWithCallback.complete(eventManager, returnValue);
      return new ContractState(
          coreContractState, stateHash, byteCount, callbacks.remove(sendingTransaction));
    } else {
      return this;
    }
  }

  Hash findSendingTransactionForEvent(Hash completedEventTransaction) {
    for (Hash hash : callbacks.keySet()) {
      for (CallbackInfo pendingTransaction : callbacks.getValue(hash).callbacks.values()) {
        if (pendingTransaction.completedTransactionTree.containsKey(completedEventTransaction)) {
          return hash;
        }
      }
    }
    throw new IllegalStateException("Unknown event identifier " + completedEventTransaction);
  }

  Hash findSendingTransactionForCallback(Hash callbackIdentifier) {
    return findCallback(callbackIdentifier, new SelectHash());
  }

  FixedList<CallbackContext.ExecutionResult> findEventsForCallback(Hash callbackIdentifier) {
    return findCallback(callbackIdentifier, new SelectExecutionResult());
  }

  <T> T findCallback(Hash callbackIdentifier, BiFunction<CallbackInfo, Hash, T> selector) {
    for (Hash hash : callbacks.keySet()) {
      InvokeWithCallback invokeWithCallback = callbacks.getValue(hash);
      CallbackInfo value = invokeWithCallback.callbacks.getValue(callbackIdentifier);
      if (value != null) {
        return selector.apply(value, hash);
      }
    }
    throw new IllegalStateException("Unknown callback identifier " + callbackIdentifier);
  }

  private static final class SelectHash implements BiFunction<CallbackInfo, Hash, Hash> {

    @Override
    public Hash apply(CallbackInfo callbackInfo, Hash hash) {
      return hash;
    }
  }

  private static final class SelectExecutionResult
      implements BiFunction<CallbackInfo, Hash, FixedList<CallbackContext.ExecutionResult>> {

    @Override
    public FixedList<CallbackContext.ExecutionResult> apply(CallbackInfo callbackInfo, Hash hash) {
      return FixedList.create(
          IntStream.range(0, callbackInfo.completedTransactionTree.size())
              .mapToObj(i -> find(callbackInfo.completedTransactionTree, i)));
    }
  }

  static CallbackContext.ExecutionResult find(
      AvlTree<Hash, CallbackInfo.CompletedTransaction> completedTransactionTree, int i) {
    for (Hash hash : completedTransactionTree.keySet()) {
      CallbackInfo.CompletedTransaction completedTransaction =
          completedTransactionTree.getValue(hash);
      if (completedTransaction.order == i) {
        return completedTransaction.asResult(hash);
      }
    }
    return null;
  }

  @Immutable
  static final class InvokeWithCallback implements StateSerializable {

    private final ReturnEnvelope returnEnvelope;
    private final BlockchainAddress from;
    private final Hash originalTransaction;
    private final AvlTree<Hash, CallbackInfo> callbacks;

    /** Creates a new CallbackInfo - for serialization. */
    @SuppressWarnings("unused")
    InvokeWithCallback() {
      returnEnvelope = null;
      from = null;
      callbacks = null;
      originalTransaction = null;
    }

    InvokeWithCallback(
        ReturnEnvelope returnEnvelope,
        BlockchainAddress from,
        Hash originalTransaction,
        AvlTree<Hash, CallbackInfo> callbacks) {
      this.returnEnvelope = returnEnvelope;
      this.from = from;
      this.originalTransaction = originalTransaction;
      this.callbacks = callbacks;
    }

    InvokeWithCallback completeEvent(Hash eventTransaction, boolean success, byte[] returnValue) {
      Hash callbackIdentifier = findIndex(eventTransaction);
      CallbackInfo callbackInfo =
          callbacks
              .getValue(callbackIdentifier)
              .completeEvent(eventTransaction, success, returnValue);
      return new InvokeWithCallback(
          returnEnvelope,
          from,
          originalTransaction,
          callbacks.set(callbackIdentifier, callbackInfo));
    }

    InvokeWithCallback callbackResult(Hash callbackIdentifier, boolean success) {
      CallbackInfo callbackInfo = callbacks.getValue(callbackIdentifier).result(success);
      return new InvokeWithCallback(
          returnEnvelope,
          from,
          originalTransaction,
          callbacks.set(callbackIdentifier, callbackInfo));
    }

    InvokeWithCallback updateCallback(CallbackInfo newCallback) {
      AvlTree<Hash, CallbackInfo> callbacks = this.callbacks;
      callbacks = callbacks.set(newCallback.callbackIdentifier, newCallback);
      return new InvokeWithCallback(returnEnvelope, from, originalTransaction, callbacks);
    }

    Hash findIndex(Hash eventTransaction) {
      List<CallbackInfo> callbacks = this.callbacks.values();
      for (CallbackInfo callback : callbacks) {
        if (callback.containsEvent(eventTransaction)) {
          return callback.callbackIdentifier;
        }
      }
      throw new IllegalStateException("Someone is returning events we have not send?");
    }

    CallbackInfo findCallbackInfo(Hash eventTransaction) {
      CallbackInfo pendingTransaction = callbacks.getValue(findIndex(eventTransaction));
      return pendingTransaction;
    }

    boolean everyCallbackComplete() {
      for (CallbackInfo pendingTransaction : callbacks.values()) {
        if (pendingTransaction.callbackResult == ExecutionResult.Unknown) {
          return false;
        }
      }
      return true;
    }

    private void complete(
        Consumer<InnerSystemEvent.CallbackEvent> eventManager, byte[] returnValue) {
      if (returnEnvelope != null) {
        Boolean totalSuccess =
            this.callbacks.values().stream()
                .map(callbackInfo -> callbackInfo.callbackResult == ExecutionResult.Success)
                .reduce(true, Boolean::logicalAnd);
        // TODO Find a way to specify format for callback return values
        eventManager.accept(
            new InnerSystemEvent.CallbackEvent(
                returnEnvelope, originalTransaction, totalSuccess, returnValue));
      }
    }
  }

  /** Information about pending callbacks. */
  @Immutable
  public static final class CallbackInfo implements StateSerializable {

    private final AvlTree<Hash, CompletedTransaction> completedTransactionTree;
    private final long allocatedCost;
    private final LargeByteArray callbackRpc;
    private final Hash callbackIdentifier;
    private final ExecutionResult callbackResult;

    @SuppressWarnings("unused")
    CallbackInfo() {
      this(null, null, null, 0, null);
    }

    private CallbackInfo(
        Hash callbackIdentifier,
        AvlTree<Hash, CompletedTransaction> completedTransactionTree,
        ExecutionResult callbackResult,
        long allocatedCost,
        LargeByteArray callbackRpc) {
      this.completedTransactionTree = completedTransactionTree;
      this.allocatedCost = allocatedCost;
      this.callbackRpc = callbackRpc;
      this.callbackIdentifier = callbackIdentifier;
      this.callbackResult = callbackResult;
    }

    /**
     * Get identifier of callback.
     *
     * @return identifying hash
     */
    public Hash getCallbackIdentifier() {
      return callbackIdentifier;
    }

    /**
     * Create a new CallbackInfo.
     *
     * @param createdEvents the events that this is a callback for
     * @param allocatedCost allocated cost to the callback invocation
     * @param callbackRpc the rpc for the callback
     * @return the created instance
     */
    public static CallbackInfo create(
        List<Hash> createdEvents, Long allocatedCost, LargeByteArray callbackRpc) {
      return new CallbackInfo(
          hash(createdEvents),
          tree(createdEvents),
          ExecutionResult.Unknown,
          allocatedCost,
          callbackRpc);
    }

    private static AvlTree<Hash, CompletedTransaction> tree(List<Hash> events) {
      AvlTree<Hash, CompletedTransaction> tree = AvlTree.create();
      for (int i = 0, eventsSize = events.size(); i < eventsSize; i++) {
        Hash hash = events.get(i);
        CompletedTransaction value =
            new CompletedTransaction(i, ExecutionResult.Unknown, new LargeByteArray(new byte[0]));
        tree = tree.set(hash, value);
      }
      return tree;
    }

    static Hash hash(Collection<Hash> hashes) {
      return Hash.create(
          stream -> {
            for (Hash hash : hashes) {
              hash.write(stream);
            }
          });
    }

    CallbackInfo completeEvent(Hash completedEvent, boolean success, byte[] returnValue) {
      CompletedTransaction value = completedTransactionTree.getValue(completedEvent);
      CompletedTransaction updated =
          new CompletedTransaction(
              value.order, translate(success), new LargeByteArray(returnValue));
      return new CallbackInfo(
          callbackIdentifier,
          completedTransactionTree.set(completedEvent, updated),
          callbackResult,
          allocatedCost,
          callbackRpc);
    }

    CallbackInfo result(boolean success) {
      ExecutionResult value = translate(success);
      return new CallbackInfo(
          callbackIdentifier, completedTransactionTree, value, allocatedCost, callbackRpc);
    }

    private ExecutionResult translate(boolean success) {
      return success ? ExecutionResult.Success : ExecutionResult.Failure;
    }

    boolean isEveryTransactionResolved() {
      for (CompletedTransaction value : completedTransactionTree.values()) {
        if (value.executionResult == ExecutionResult.Unknown) {
          return false;
        }
      }
      return true;
    }

    CallbackContext.ExecutionResult asResult(Hash hash) {
      CompletedTransaction completedTransaction = completedTransactionTree.getValue(hash);
      return CallbackContext.createResult(
          hash,
          completedTransaction.executionResult == ExecutionResult.Success,
          SafeDataInputStream.createFromBytes(completedTransaction.returnValue.getData()));
    }

    boolean containsEvent(Hash eventTransaction) {
      return completedTransactionTree.containsKey(eventTransaction);
    }

    @Immutable
    static final class CompletedTransaction implements StateSerializable {

      private final int order;
      private final ExecutionResult executionResult;
      private final LargeByteArray returnValue;

      @SuppressWarnings("unused")
      CompletedTransaction() {
        executionResult = null;
        returnValue = null;
        order = 0;
      }

      CompletedTransaction(int order, ExecutionResult executionResult, LargeByteArray returnValue) {
        this.order = order;
        this.executionResult = executionResult;
        this.returnValue = returnValue;
      }

      CallbackContext.ExecutionResult asResult(Hash hash) {
        return CallbackContext.createResult(
            hash,
            executionResult == ExecutionResult.Success,
            SafeDataInputStream.createFromBytes(returnValue.getData()));
      }
    }
  }

  static final class CallbackResult {

    public final ContractState contractState;
    public final CallbackCreator.Callback callback;

    CallbackResult(ContractState contractState, CallbackCreator.Callback callback) {
      this.callback = callback;
      this.contractState = contractState;
    }
  }
}
