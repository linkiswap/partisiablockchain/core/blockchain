package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.binder.BinderEvent;
import com.partisiablockchain.blockchain.ChainPlugin.PluginIdentifier;
import com.partisiablockchain.blockchain.ChainPlugin.PluginLoader;
import com.partisiablockchain.blockchain.ChainState.PluginSerialization;
import com.partisiablockchain.blockchain.contract.binder.BlockchainContract;
import com.partisiablockchain.contract.ContractType;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.serialization.HashHistoryStorage;
import com.partisiablockchain.serialization.LargeByteArray;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.serialization.StateSerializer;
import com.partisiablockchain.serialization.StateStorage;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

@SuppressWarnings("unchecked")
final class ChainStateCache {

  static final int DELETION_DELAY = 10;
  private final StateStorage storage;
  private final Map<Hash, PluginSerialization<?, ?, ?>> plugins;
  private final Map<BlockchainAddress, BlockchainContractWithJars> blockchainContracts;
  private final Map<BlockchainAddress, Integer> delayDeletionCounters;
  private final Map<Hash, JarClassLoader> classLoaders;
  private final Map<ContractAndBinder, JarClassLoader> contractClassLoaders;

  ChainStateCache(StateStorage storage) {
    this.storage = storage;
    this.blockchainContracts = new ConcurrentHashMap<>();
    this.delayDeletionCounters = new ConcurrentHashMap<>();
    this.plugins = new ConcurrentHashMap<>();
    this.classLoaders = new ConcurrentHashMap<>();
    this.contractClassLoaders = new ConcurrentHashMap<>();
  }

  record BlockchainContractWithJars(
      BlockchainContract<?, ?> blockchainContract, Hash binderJar, Hash contractJar) {}

  <T extends StateSerializable, U extends BinderEvent> BlockchainContract<T, U> get(
      BlockchainAddress address, Hash binderJar, Hash contractJar) {
    return (BlockchainContract<T, U>)
        blockchainContracts.compute(
                address,
                (key, value) -> {
                  if (value == null || !hasSameJars(value, binderJar, contractJar)) {
                    JarClassLoader binderLoader = getClassLoader(binderJar);
                    LargeByteArray contractData =
                        stateSerializer().read(contractJar, LargeByteArray.class);
                    Object contract;
                    Object binder;
                    if (isRawBinder(binderLoader.getMainClass())) {
                      contract = contractData.getData();
                      binder = binderLoader.instantiateBinder(byte[].class, contract);
                    } else {
                      JarClassLoader contractLoader =
                          getContractClassLoader(contractJar, binderLoader);
                      contract = contractLoader.instantiateMainClass();
                      binder =
                          binderLoader.instantiateBinder(
                              contract.getClass().getSuperclass(), contract);
                    }
                    return new BlockchainContractWithJars(
                        ChainState.createContract(ContractType.fromAddress(address), binder),
                        binderJar,
                        contractJar);
                  } else {
                    delayDeletionCounters.remove(key);
                    return blockchainContracts.get(key);
                  }
                })
            .blockchainContract;
  }

  private boolean hasSameJars(
      BlockchainContractWithJars contractWithJars, Hash binderJar, Hash contractJar) {
    return contractWithJars.binderJar.equals(binderJar)
        && contractWithJars.contractJar.equals(contractJar);
  }

  @SuppressWarnings("ReturnValueIgnored")
  private boolean isRawBinder(Class<?> type) {
    try {
      type.getDeclaredConstructor(byte[].class);
      return true;
    } catch (NoSuchMethodException e) {
      return false;
    }
  }

  private JarClassLoader getClassLoader(Hash jar) {
    return classLoaders.computeIfAbsent(jar, key -> bootClassLoader(stateSerializer(), jar, null));
  }

  private JarClassLoader getContractClassLoader(Hash contractJar, JarClassLoader binderLoader) {
    ContractAndBinder contractAndBinder = new ContractAndBinder(contractJar, binderLoader);
    return contractClassLoaders.computeIfAbsent(
        contractAndBinder, k -> bootClassLoader(stateSerializer(), contractJar, binderLoader));
  }

  private StateSerializer stateSerializer() {
    return new StateSerializer(storage, true);
  }

  private JarClassLoader bootClassLoader(
      StateSerializer serializer, Hash jar, JarClassLoader classLoader) {
    LargeByteArray largeByteArray = serializer.read(jar, LargeByteArray.class);
    JarClassLoader.Builder builder = JarClassLoader.builder(largeByteArray.getData());
    if (classLoader != null) {
      builder.defaultClassLoader(classLoader);
    }
    return builder.build();
  }

  <
          GlobalT extends StateSerializable,
          LocalT extends StateSerializable,
          PluginT extends BlockchainPlugin<GlobalT, LocalT>>
      PluginSerialization<GlobalT, LocalT, PluginT> getPlugin(Hash jarHash) {
    return (PluginSerialization<GlobalT, LocalT, PluginT>)
        plugins.computeIfAbsent(
            jarHash,
            hash ->
                new PluginSerialization<GlobalT, LocalT, PluginT>(
                    stateSerializer(),
                    stateSerializer(),
                    bootClassLoader(stateSerializer(), hash, null)));
  }

  void withContracts(Set<BlockchainAddress> toMark) {
    for (BlockchainAddress blockchainAddress : toMark) {
      delayDeletionCounters.put(blockchainAddress, 0);
    }
    for (BlockchainAddress blockchainAddress : Set.copyOf(delayDeletionCounters.keySet())) {
      Integer counter = delayDeletionCounters.get(blockchainAddress);
      if (counter >= DELETION_DELAY) {
        blockchainContracts.remove(blockchainAddress);
        delayDeletionCounters.remove(blockchainAddress);
      } else {
        delayDeletionCounters.put(blockchainAddress, counter + 1);
      }
    }
  }

  StateSerializer createStateSerializer() {
    return new StateSerializer(new HashHistoryStorage(storage), true);
  }

  StateSerializer createStateSerializerFreshStorage() {
    return new StateSerializer(new HashHistoryStorage(storage), true, true);
  }

  <L extends StateSerializable, P extends BlockchainPlugin<G, L>, G extends StateSerializable>
      PluginLoader<G, L, P> createPluginLoader() {
    return new PluginLoader<>() {
      @Override
      public PluginSerialization<G, L, P> load(Hash jar) {
        return getPlugin(jar);
      }

      @Override
      public PluginIdentifier loadId(Hash identifier) {
        return stateSerializer().read(identifier, PluginIdentifier.class);
      }

      @Override
      public Hash saveId(PluginIdentifier pluginIdentifier) {
        return stateSerializer().write(pluginIdentifier).hash();
      }
    };
  }
}
