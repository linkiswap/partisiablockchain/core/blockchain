package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.serialization.StateSerializable;
import java.util.Comparator;
import java.util.Objects;

/** The id of a shard. */
@Immutable
public final class ShardId implements Comparable<ShardId>, StateSerializable {

  /** Utility for comparing shard ids. */
  public static final Comparator<ShardId> SHARD_ID_COMPARATOR =
      Comparator.comparing(ShardId::getId, Comparator.nullsFirst(String::compareTo));

  private final String id;

  @SuppressWarnings("unused")
  private ShardId() {
    id = null;
  }

  /**
   * Default constructor.
   *
   * @param id shard id
   */
  public ShardId(String id) {
    this.id = id;
  }

  /**
   * Get shard id.
   *
   * @return shard id
   */
  public String getId() {
    return id;
  }

  @Override
  public int compareTo(ShardId other) {
    return SHARD_ID_COMPARATOR.compare(this, other);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ShardId shardId = (ShardId) o;
    return Objects.equals(id, shardId.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }
}
