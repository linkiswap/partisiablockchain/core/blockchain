package com.partisiablockchain.blockchain.contract;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.binder.BinderEvent;
import com.partisiablockchain.blockchain.MutableChainState;
import com.partisiablockchain.blockchain.contract.binder.BlockchainContract;
import com.partisiablockchain.blockchain.contract.binder.BlockchainContractContextImpl;
import com.partisiablockchain.blockchain.transaction.ExecutionContextTransaction;
import com.partisiablockchain.blockchain.transaction.Transaction;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.util.DataStreamLimit;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;

/** Create contract transaction. */
public final class CreateContractTransaction implements Transaction {

  private final BlockchainAddress address;
  private final byte[] binderJar;
  private final byte[] contractJar;
  private final byte[] abi;
  private final byte[] rpc;

  private CreateContractTransaction(
      BlockchainAddress address, byte[] binderJar, byte[] contractJar, byte[] abi, byte[] rpc) {
    this.address = address;
    this.binderJar = binderJar;
    this.contractJar = contractJar;
    this.abi = abi;
    this.rpc = rpc;
  }

  /**
   * Create a new contract transaction.
   *
   * @param address the address that the contract should be deployed at
   * @param binderJar the binding jar
   * @param contractJar the actual contract
   * @param abi the ABI for the contract
   * @param rpc the invocation for create
   * @return a new contract transaction
   */
  public static CreateContractTransaction create(
      BlockchainAddress address, byte[] binderJar, byte[] contractJar, byte[] abi, byte[] rpc) {
    return new CreateContractTransaction(address, binderJar, contractJar, nullToEmpty(abi), rpc);
  }

  private static byte[] nullToEmpty(byte[] bytes) {
    if (bytes == null) {
      return new byte[0];
    } else {
      return bytes;
    }
  }

  private static <T extends StateSerializable, EventT extends BinderEvent>
      EventResult<EventT> typedCreate(
          ExecutionContextTransaction executionContext,
          BlockchainAddress address,
          byte[] bindingJar,
          byte[] contractJar,
          byte[] abi,
          byte[] rpc) {
    MutableChainState state = executionContext.getState();
    Hash binderHash = state.saveJar(bindingJar);
    Hash contractHash = state.saveJar(contractJar);
    Hash abiHash = state.saveJar(abi);

    CoreContractState contractState =
        CoreContractState.create(binderHash, contractHash, abiHash, contractJar.length);
    BlockchainContract<T, EventT> binder = state.createContract(address, contractState);

    executionContext.getState().getBlockchainContract(address);

    BlockchainContractContextImpl contractContext = new BlockchainContractContextImpl(address);
    BlockchainResult<T, EventT> result = binder.create(executionContext, contractContext, rpc);

    state.setContractState(address, result.getState());

    long blockProductionTime = executionContext.getBlockProductionTime();
    state.contractCreated(address, blockProductionTime);
    return result.events();
  }

  @Override
  public EventResult<? extends BinderEvent> execute(ExecutionContextTransaction executionContext) {
    return typedCreate(executionContext, address, binderJar, contractJar, abi, rpc);
  }

  @Override
  public BlockchainAddress getTargetContract() {
    return address;
  }

  @Override
  public Type getType() {
    return Type.DEPLOY_CONTRACT;
  }

  @Override
  public void writeInner(SafeDataOutputStream stream) {
    address.write(stream);
    stream.writeDynamicBytes(binderJar);
    stream.writeDynamicBytes(contractJar);
    stream.writeDynamicBytes(abi);
    stream.writeDynamicBytes(rpc);
  }

  /**
   * Read the transaction from the specified stream.
   *
   * @param stream the stream to read from
   * @return the deploy transaction read from the stream
   */
  public static Transaction read(SafeDataInputStream stream) {
    BlockchainAddress address = BlockchainAddress.read(stream);
    byte[] binderJar = DataStreamLimit.readDynamicBytes(stream);
    byte[] contractJar = DataStreamLimit.readDynamicBytes(stream);
    byte[] abi = DataStreamLimit.readDynamicBytes(stream);
    byte[] rpc = DataStreamLimit.readDynamicBytes(stream);

    return create(address, binderJar, contractJar, abi, rpc);
  }
}
