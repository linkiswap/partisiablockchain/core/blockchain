package com.partisiablockchain.blockchain.contract.binder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.binder.BinderEvent;
import com.partisiablockchain.blockchain.contract.BlockchainResult;
import com.partisiablockchain.blockchain.contract.CoreContractState.ContractSerialization;
import com.partisiablockchain.blockchain.transaction.ExecutionContextTransaction;
import com.partisiablockchain.contract.CallbackContext;
import com.partisiablockchain.serialization.StateSerializable;

/**
 * Generic interface for contracts in the chain state.
 *
 * @param <T> the type of state
 */
public interface BlockchainContract<T extends StateSerializable, EventT extends BinderEvent> {

  /**
   * Create contract.
   *
   * @param executionContext context for executing a transaction
   * @param contractContext context for execution in a contract
   * @param rpc payload for invocation
   * @return initial contract state
   */
  BlockchainResult<T, EventT> create(
      ExecutionContextTransaction executionContext,
      BlockchainContractContext contractContext,
      byte[] rpc);

  /**
   * Initiate invocation against contract.
   *
   * @param executionContext context for executing a transaction
   * @param contractContext context for execution in a contract
   * @param contract contract state before invocation
   * @param rpc payload for invocation
   * @return next contract state
   */
  BlockchainResult<T, EventT> invoke(
      ExecutionContextTransaction executionContext,
      BlockchainContractContext contractContext,
      T contract,
      byte[] rpc);

  /**
   * Initiate callback for contract.
   *
   * @param executionContext context for executing a transaction
   * @param contractContext context for execution in a contract
   * @param contract contract state before callback
   * @param callbackContext context for callback
   * @param rpc payload for callback
   * @return next contract state
   */
  BlockchainResult<T, EventT> callback(
      ExecutionContextTransaction executionContext,
      BlockchainContractContext contractContext,
      T contract,
      CallbackContext callbackContext,
      byte[] rpc);

  /**
   * Get serializer for the internal state of a contract.
   *
   * @return serializer for the internal state of a contract.
   */
  ContractSerialization<T> getContractSerialization();
}
