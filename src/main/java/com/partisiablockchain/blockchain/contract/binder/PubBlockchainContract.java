package com.partisiablockchain.blockchain.contract.binder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.binder.BinderInteraction;
import com.partisiablockchain.binder.BinderResult;
import com.partisiablockchain.binder.pub.PubBinderContract;
import com.partisiablockchain.blockchain.contract.BlockchainResult;
import com.partisiablockchain.blockchain.contract.CoreContractState.ContractSerialization;
import com.partisiablockchain.blockchain.transaction.ExecutionContextTransaction;
import com.partisiablockchain.contract.CallbackContext;
import com.partisiablockchain.serialization.StateSerializable;

/**
 * Public contracts in the chain state.
 *
 * @param <T> the type of state
 */
public final class PubBlockchainContract<T extends StateSerializable>
    implements BlockchainContract<T, BinderInteraction> {

  private final PubBinderContract<T> binder;
  private final ContractSerialization<T> serialization;

  /**
   * Construct a new PubBlockchainContract.
   *
   * @param binder the binder
   */
  public PubBlockchainContract(PubBinderContract<T> binder) {
    this.binder = binder;
    Class<T> stateClazz = binder.getStateClass();
    this.serialization = new ContractSerialization<>(stateClazz);
  }

  @Override
  public BlockchainResult<T, BinderInteraction> create(
      ExecutionContextTransaction executionContext,
      BlockchainContractContext contractContext,
      byte[] rpc) {
    PubBinderContextImpl context = new PubBinderContextImpl(contractContext, executionContext);
    BinderResult<T, BinderInteraction> result = binder.create(context, rpc);
    return BlockchainResult.create(result);
  }

  @Override
  public BlockchainResult<T, BinderInteraction> invoke(
      ExecutionContextTransaction executionContext,
      BlockchainContractContext contractContext,
      T contract,
      byte[] rpc) {
    PubBinderContextImpl context = new PubBinderContextImpl(contractContext, executionContext);
    BinderResult<T, BinderInteraction> result = binder.invoke(context, contract, rpc);
    return BlockchainResult.create(result);
  }

  @Override
  public BlockchainResult<T, BinderInteraction> callback(
      ExecutionContextTransaction executionContext,
      BlockchainContractContext contractContext,
      T contract,
      CallbackContext callbackContext,
      byte[] rpc) {
    PubBinderContextImpl context = new PubBinderContextImpl(contractContext, executionContext);
    BinderResult<T, BinderInteraction> result =
        binder.callback(context, contract, callbackContext, rpc);
    return BlockchainResult.create(result);
  }

  @Override
  public ContractSerialization<T> getContractSerialization() {
    return serialization;
  }
}
