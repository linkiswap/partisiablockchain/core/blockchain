package com.partisiablockchain.blockchain.transaction;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.secata.stream.DataStreamSerializable;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;

/** The core part of any transaction in pbc, contains shared parts of the transaction. */
public final class CoreTransactionPart implements DataStreamSerializable {

  private final long nonce;
  private final long validToTime;
  private final long cost;

  /**
   * Create a new core transaction part.
   *
   * @param nonce nonce for the sender of this transaction
   * @param validToTime which human time (System.currentMillis) this transaction is valid to
   * @param cost the fee to be deducted from sender account during transaction
   */
  public CoreTransactionPart(long nonce, long validToTime, long cost) {
    this.nonce = nonce;
    this.validToTime = validToTime;
    this.cost = cost;
  }

  /**
   * Create a new core transaction part.
   *
   * @param nonce nonce for the sender of this transaction
   * @param validToTime which human time (System.currentMillis) this transaction is valid to
   * @param cost the fee to be deducted from sender account during transaction
   * @return a new core transaction part
   */
  public static CoreTransactionPart create(long nonce, long validToTime, long cost) {
    return new CoreTransactionPart(nonce, validToTime, cost);
  }

  /**
   * Get sender's nonce.
   *
   * @return sender's nonce
   */
  public long getNonce() {
    return nonce;
  }

  /**
   * Get human time this transaction is valid to.
   *
   * @return time in milliseconds
   */
  @SuppressWarnings("WeakerAccess")
  public long getValidToTime() {
    return validToTime;
  }

  /**
   * Get the fee to be deducted from sender account during transaction.
   *
   * @return fee to be deducted
   */
  @SuppressWarnings("WeakerAccess")
  public long getCost() {
    return cost;
  }

  @Override
  public void write(SafeDataOutputStream stream) {
    stream.writeLong(nonce);
    stream.writeLong(validToTime);
    stream.writeLong(cost);
  }

  @Override
  public String toString() {
    return "CoreTransactionPart{"
        + "nonce="
        + nonce
        + ", validToTime="
        + validToTime
        + ", cost="
        + cost
        + '}';
  }

  /**
   * Read a core transaction part from the specified stream.
   *
   * @param stream the stream to read from
   * @return the read core transaction part
   */
  public static CoreTransactionPart read(SafeDataInputStream stream) {
    return new CoreTransactionPart(stream.readLong(), stream.readLong(), stream.readLong());
  }
}
