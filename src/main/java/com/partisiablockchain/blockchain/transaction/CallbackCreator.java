package com.partisiablockchain.blockchain.transaction;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.serialization.LargeByteArray;

/** Interface for creating a callback. */
public interface CallbackCreator {

  /**
   * Create callback.
   *
   * @param contract target contract address for callback
   * @param callback actual callback definition
   */
  void create(BlockchainAddress contract, Callback callback);

  /** Struct representing a callback. */
  final class Callback {

    /** Callback identifier. */
    public final Hash callbackIdentifier;
    /** From. */
    public final BlockchainAddress from;
    /** Allocated cost. */
    public final Long allocatedCost;
    /** Rpc for callback. */
    public final LargeByteArray rpcForCallback;

    /**
     * Creates a new callback structure.
     *
     * @param callbackIdentifier the event transactions that has been awaited and is completed
     * @param from the sender of the callback
     * @param allocatedCost the cost for this event
     * @param rpcForCallback the saved rpc registered when completedTransaction was spawned
     */
    public Callback(
        Hash callbackIdentifier,
        BlockchainAddress from,
        Long allocatedCost,
        LargeByteArray rpcForCallback) {
      this.callbackIdentifier = callbackIdentifier;
      this.from = from;
      this.allocatedCost = allocatedCost;
      this.rpcForCallback = rpcForCallback;
    }
  }
}
