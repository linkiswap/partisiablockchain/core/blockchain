package com.partisiablockchain.blockchain.transaction;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.blockchain.AccountPluginAccessor;
import com.partisiablockchain.blockchain.TransactionCost;
import com.partisiablockchain.crypto.Hash;
import java.util.HashMap;
import java.util.Map;
import java.util.function.BiConsumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Utility class for collecting fees, in order to ensure that a transaction does not overspend.
 *
 * <p>Tracks several types of fee entries. Includes different ways of paying for gas, different
 * fees, or pre-allocating fees.
 */
public final class FeeCollector {

  private static final Logger logger = LoggerFactory.getLogger(FeeCollector.class);

  private final AccountPluginAccessor feeState;

  /** Gas provided by the calling user. */
  private final long gasPaidByUser;

  /** Gas to be paid by the contract, for example for a callback. */
  private long gasToBePaidByContract;

  /** CPU fee, taken for running computations in the contract. */
  private long feeCpu;

  /** Gas allocated for usage with events. */
  private long allocatedForEvents;

  /** Network fee, taken for transferring data over the network. */
  private final Map<Hash, Long> feeNetwork;

  private FeeCollector(AccountPluginAccessor feeState, long gasPaidByUser) {
    this.feeState = feeState;
    this.gasPaidByUser = gasPaidByUser;
    this.gasToBePaidByContract = 0;
    this.feeCpu = 0;
    this.feeNetwork = new HashMap<>();
    this.allocatedForEvents = 0;
  }

  /**
   * Create fee collector.
   *
   * @param feePayer payer of fees
   * @param gasPaidByUser amount of gas that the user will pay for the execution of the transaction
   * @return created fee collector
   */
  public static FeeCollector create(AccountPluginAccessor feePayer, long gasPaidByUser) {
    logger.debug("Creating FeeCollector with maximum {}", gasPaidByUser);
    return new FeeCollector(feePayer, gasPaidByUser);
  }

  /**
   * Add an amount of gas that will be paid by the executing contract. The total consumed gas is
   * allowed to reach {@link #gasPaidByUser} + {@link #gasToBePaidByContract}.
   *
   * @param gasToBePaidByContract the amount to add
   */
  void registerPaymentFromContract(long gasToBePaidByContract) {
    this.gasToBePaidByContract += gasToBePaidByContract;
    logger.debug("{} - Adding payment from contract {}", hashCode(), gasToBePaidByContract);
  }

  /**
   * Collect cpu fee.
   *
   * @param fee the amount to pay
   */
  void registerCpuFee(long fee) {
    this.feeCpu += fee;
    logger.debug("{} - Adding cpu fee of {} gas", hashCode(), fee);
    assertWithinMaximum();
  }

  /**
   * Convert a byte count to the network fee for that amount.
   *
   * @param networkByteCount the number of network bytes
   * @return the converted network fee in gas
   */
  public long convertNetworkFee(long networkByteCount) {
    return feeState.convertNetworkFee(networkByteCount);
  }

  /**
   * Collect network cost.
   *
   * @param identifier the transaction identifier
   * @param networkFee the network fee
   */
  public void registerNetworkFee(Hash identifier, long networkFee) {
    long oldCost = feeNetwork.getOrDefault(identifier, 0L);
    feeNetwork.put(identifier, oldCost + networkFee);

    logger.debug("{} - Adding converted network fee of {} gas", hashCode(), networkFee);
    assertWithinMaximum();
  }

  /**
   * Collect fee allocated for events and callbacks.
   *
   * @param allocatedForEvents the fee allocated for events and callbacks
   */
  void registerAllocatedCost(long allocatedForEvents) {
    this.allocatedForEvents += allocatedForEvents;
    logger.debug("{} - Adding allocated costs of {} gas", hashCode(), allocatedForEvents);
    assertWithinMaximum();
  }

  private void assertWithinMaximum() {
    if (sum() > (gasPaidByUser + gasToBePaidByContract)) {
      throw new RuntimeException("Ran out of gas: %s".formatted(this));
    }
  }

  /**
   * Determines the sum of all consumers of gas.
   *
   * <p>Formula: {@link #feeCpu} + {@link #feeCpu} + {@link #allocatedForEvents}
   *
   * @return sum of consumed gas
   */
  public long sum() {
    return feeCpu + calculateNetworkCost() + allocatedForEvents;
  }

  /**
   * Consume the collected fee as consumed and surplus gas.
   *
   * @param feeConsumer consumer of the collected fees. Argument 1 is the surplus. Argument 2 is the
   *     summed fees.
   */
  public void handleCollectedFees(BiConsumer<Long, Long> feeConsumer) {
    long usage = feeCpu + calculateNetworkCost();
    long surplus = remaining();
    long contractBalanceUpdate = surplus - gasToBePaidByContract;
    feeConsumer.accept(contractBalanceUpdate, usage);
    logger.debug(
        "{} - Handling contract balance update {} and blockchain usage {}",
        hashCode(),
        contractBalanceUpdate,
        usage);
  }

  private Long calculateNetworkCost() {
    return this.feeNetwork.values().stream().reduce(Long::sum).orElse(0L);
  }

  /**
   * Get remaining payment not yet consumed.
   *
   * @return remaining payment
   */
  public long remaining() {
    return gasPaidByUser + gasToBePaidByContract - sum();
  }

  /**
   * Get transaction cost collected.
   *
   * @return the transactions costs collected
   */
  public TransactionCost getTransactionCost() {
    return new TransactionCost(
        allocatedForEvents, feeCpu, remaining(), gasToBePaidByContract, feeNetwork);
  }

  @Override
  public String toString() {
    return "FeeCollector{"
        + "gasPaidByUser="
        + gasPaidByUser
        + ", gasToBePaidByContract="
        + gasToBePaidByContract
        + ", consumedInTotal="
        + sum()
        + ", feeCpu="
        + feeCpu
        + ", feeNetwork="
        + calculateNetworkCost()
        + ", allocatedForEvents="
        + allocatedForEvents
        + '}';
  }

  /**
   * Get the amount of gas that the calling user will pay for the execution of the transaction.
   *
   * @return The amount of gas
   */
  long getGasPaidByUser() {
    return gasPaidByUser;
  }
}
