package com.partisiablockchain.blockchain.transaction;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.blockchain.FinalBlock;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.flooding.NetworkFloodable;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;

/**
 * An event that is valid on the flooding network.
 *
 * @param block the block finalizing the event
 * @param eventIdentifier the identifier of the event to make floodable
 * @param proof event proof of the floodable event
 */
public record FloodableEventCompressed(FinalBlock block, Hash eventIdentifier, EventProof proof)
    implements NetworkFloodable {

  /**
   * Decompresses the compressed floodable event.
   *
   * @param finder executable event finder for finding the event transaction
   * @return decompressed floodable event.
   */
  public FloodableEvent decompress(ExecutableEventFinder finder) {
    EventProof.ChainIds chainIds = proof.verify(block.getBlock().getState(), eventIdentifier);
    return new FloodableEvent(
        block, finder.find(chainIds.subChainId(), eventIdentifier).getEvent(), proof, chainIds);
  }

  /**
   * Create a new floodable event.
   *
   * @param identifier the identifier of the event to make floodable
   * @param block the block finalizing the event
   * @param eventProof event proof of the floodable event
   * @return the created event
   */
  static FloodableEventCompressed create(Hash identifier, FinalBlock block, EventProof eventProof) {
    return new FloodableEventCompressed(block, identifier, eventProof);
  }

  /**
   * Read the event from stream.
   *
   * @param stream the stream to read from
   * @return the read stream
   */
  public static FloodableEventCompressed read(SafeDataInputStream stream) {
    FinalBlock block = FinalBlock.read(stream);
    Hash identifier = Hash.read(stream);
    EventProof proof = EventProof.read(stream);
    return new FloodableEventCompressed(block, identifier, proof);
  }

  @Override
  public void write(SafeDataOutputStream stream) {
    block.write(stream);
    eventIdentifier.write(stream);
    proof.write(stream);
  }
}
