package com.partisiablockchain.blockchain.transaction;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.serialization.StateSerializable;
import com.secata.tools.immutable.FixedList;

/** Represents a pending unconverted BYOC fee. */
@Immutable
public final class PendingByocFee implements StateSerializable {

  private final FixedList<BlockchainAddress> nodes;
  private final Unsigned256 amount;
  private final String symbol;

  private PendingByocFee(FixedList<BlockchainAddress> nodes, Unsigned256 amount, String symbol) {
    this.nodes = nodes;
    this.amount = amount;
    this.symbol = symbol;
  }

  /**
   * Create a new pending BYOC fee.
   *
   * @param nodes the nodes that created the transaction related to this fee
   * @param amount the fee amount
   * @param symbol the coin symbol
   * @return a new PendingByocFee.
   */
  public static PendingByocFee create(
      FixedList<BlockchainAddress> nodes, Unsigned256 amount, String symbol) {
    return new PendingByocFee(nodes, amount, symbol);
  }

  /**
   * Gets the nodes involved in the transaction related to this fee.
   *
   * @return the BYOC nodes.
   */
  public FixedList<BlockchainAddress> getNodes() {
    return nodes;
  }

  /**
   * Gets the fee amount.
   *
   * @return the fee amount.
   */
  public Unsigned256 getAmount() {
    return amount;
  }

  /**
   * Gets the BYOC coin symbol.
   *
   * @return the BYOC ocin symbol.
   */
  public String getSymbol() {
    return symbol;
  }
}
