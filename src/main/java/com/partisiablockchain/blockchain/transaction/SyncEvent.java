package com.partisiablockchain.blockchain.transaction;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.util.DataStreamLimit;
import com.secata.stream.DataStreamSerializable;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.stream.SafeListStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Event with any information that should be moved from one shard to another when changing the shard
 * configuration.
 */
public final class SyncEvent implements EventTransaction.InnerEvent {

  private final List<AccountTransfer> accounts;
  private final List<ContractTransfer> contracts;
  private final List<byte[]> stateStorage;

  /**
   * Create a sync event.
   *
   * @param accounts list of account transfer objects
   * @param contracts list of contract transfer objects
   * @param stateStorage list of serialized data
   */
  public SyncEvent(
      List<AccountTransfer> accounts, List<ContractTransfer> contracts, List<byte[]> stateStorage) {
    this.accounts = accounts;
    this.contracts = contracts;
    this.stateStorage = stateStorage;
  }

  static SyncEvent read(SafeDataInputStream stream) {
    List<AccountTransfer> accounts =
        DataStreamLimit.readDynamicList(stream, AccountTransfer.LIST_SERIALIZER);
    List<ContractTransfer> contracts =
        DataStreamLimit.readDynamicList(stream, ContractTransfer.LIST_SERIALIZER);
    List<byte[]> stateStorage = readStateStorage(stream);
    return new SyncEvent(accounts, contracts, stateStorage);
  }

  static SyncEvent unsafeRead(SafeDataInputStream stream) {
    List<AccountTransfer> accounts = unsafeReadAccountTransfers(stream);
    List<ContractTransfer> contracts = unsafeReadContractTransfers(stream);
    List<byte[]> stateStorage = unsafeReadStateStorage(stream);
    return new SyncEvent(accounts, contracts, stateStorage);
  }

  private static List<byte[]> readStateStorage(SafeDataInputStream stream) {
    int stateStorageSize = stream.readInt();
    if (stateStorageSize > DataStreamLimit.MAX_LIST_SIZE) {
      throw new IllegalArgumentException(
          "Unable to read list of size "
              + stateStorageSize
              + " from stream. Maximum allowed is 1000");
    }
    List<byte[]> stateStorage = new ArrayList<>();
    for (int i = 0; i < stateStorageSize; i++) {
      stateStorage.add(DataStreamLimit.readDynamicBytes(stream));
    }
    return stateStorage;
  }

  private static List<AccountTransfer> unsafeReadAccountTransfers(SafeDataInputStream stream) {
    int size = stream.readInt();
    return AccountTransfer.LIST_SERIALIZER.readFixed(stream, size);
  }

  private static List<ContractTransfer> unsafeReadContractTransfers(SafeDataInputStream stream) {
    int size = stream.readInt();
    return ContractTransfer.LIST_SERIALIZER.readFixed(stream, size);
  }

  private static List<byte[]> unsafeReadStateStorage(SafeDataInputStream stream) {
    int stateStorageSize = stream.readInt();
    List<byte[]> stateStorage = new ArrayList<>();
    for (int i = 0; i < stateStorageSize; i++) {
      int size = stream.readInt();
      stateStorage.add(stream.readBytes(size));
    }
    return stateStorage;
  }

  @Override
  public void writeInner(SafeDataOutputStream stream) {
    AccountTransfer.LIST_SERIALIZER.writeDynamic(stream, accounts);
    ContractTransfer.LIST_SERIALIZER.writeDynamic(stream, contracts);
    stream.writeInt(stateStorage.size());
    stateStorage.forEach(stream::writeDynamicBytes);
  }

  @Override
  public EventTransaction.EventType getEventType() {
    return EventTransaction.EventType.SYNC;
  }

  /**
   * Get account transfers.
   *
   * @return list of account transfers
   */
  public List<AccountTransfer> getAccountTransfers() {
    return accounts;
  }

  /**
   * Get contract transfers.
   *
   * @return list of contracts transfers
   */
  public List<ContractTransfer> getContractTransfers() {
    return contracts;
  }

  /**
   * Get a list of stored states.
   *
   * @return list of stored states
   */
  public List<byte[]> getStateStorage() {
    return stateStorage;
  }

  /** The current state of an account. */
  public record AccountTransfer(
      BlockchainAddress address, Hash accountStateHash, Hash pluginStateHash)
      implements DataStreamSerializable {

    static final SafeListStream<AccountTransfer> LIST_SERIALIZER =
        SafeListStream.create(AccountTransfer::read, AccountTransfer::write);

    @Override
    public void write(SafeDataOutputStream stream) {
      address.write(stream);
      accountStateHash.write(stream);
      pluginStateHash.write(stream);
    }

    static AccountTransfer read(SafeDataInputStream stream) {
      BlockchainAddress address = BlockchainAddress.read(stream);
      Hash accountStateHash = Hash.read(stream);
      Hash pluginStateHash = Hash.read(stream);
      return new AccountTransfer(address, accountStateHash, pluginStateHash);
    }
  }

  /** The current state of a contract. */
  public record ContractTransfer(
      BlockchainAddress address, Hash contractStateHash, Hash pluginStateHash)
      implements DataStreamSerializable {

    static final SafeListStream<ContractTransfer> LIST_SERIALIZER =
        SafeListStream.create(ContractTransfer::read, ContractTransfer::write);

    @Override
    public void write(SafeDataOutputStream stream) {
      address.write(stream);
      contractStateHash.write(stream);
      pluginStateHash.write(stream);
    }

    static ContractTransfer read(SafeDataInputStream stream) {
      BlockchainAddress address = BlockchainAddress.read(stream);
      Hash contractStateHash = Hash.read(stream);
      Hash pluginStateHash = Hash.read(stream);
      return new ContractTransfer(address, contractStateHash, pluginStateHash);
    }
  }
}
