package com.partisiablockchain.blockchain.transaction;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;

/**
 * Takes event from the binder result and converts these to actual events by forwarding these to
 * {@link EventCollector}.
 */
public final class EventSender {

  private final BlockchainAddress sender;
  private final BlockchainAddress contract;

  /**
   * Creates a new event handler.
   *
   * @param sender the sender of this execution context.
   * @param contract the contract
   */
  public EventSender(BlockchainAddress sender, BlockchainAddress contract) {
    this.sender = sender;
    this.contract = contract;
  }

  BlockchainAddress selectSender(boolean originalSender) {
    if (originalSender) {
      return sender;
    } else {
      return contract;
    }
  }
}
