package com.partisiablockchain.flooding;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.blockchain.FinalBlock;
import com.partisiablockchain.blockchain.transaction.SignedTransaction;
import com.secata.stream.DataStreamSerializable;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.stream.SafeListStream;
import java.util.List;

/** Response for a request for a block send via the flooding network. */
public final class BlockResponseSemiCompressed implements DataStreamSerializable {

  private final long senderBlockTime;
  private final List<BlockPairSemiCompressed> blocks;
  private final String chainId;

  /**
   * Creates a new block response.
   *
   * @param senderBlockTime sender's block time - where is sender in time
   * @param blocks the blocks in the response
   * @param chainId the chain id for serialization of transactions
   */
  public BlockResponseSemiCompressed(
      long senderBlockTime, List<BlockPairSemiCompressed> blocks, String chainId) {
    this.senderBlockTime = senderBlockTime;
    this.blocks = blocks;
    this.chainId = chainId;
  }

  /**
   * Read a {@link BlockResponseSemiCompressed} from stream.
   *
   * @param stream the stream to read from
   * @param chainId the id of the running blockchain
   * @return the block response read from the stream
   */
  public static BlockResponseSemiCompressed read(String chainId, SafeDataInputStream stream) {
    long senderBlockTime = stream.readLong();
    int size = stream.readUnsignedByte();
    List<BlockPairSemiCompressed> blocks = getSerializer(chainId).readFixed(stream, size);
    return new BlockResponseSemiCompressed(senderBlockTime, blocks, chainId);
  }

  /**
   * Get sender's block time.
   *
   * @return sender's block time
   */
  public long getSenderBlockTime() {
    return senderBlockTime;
  }

  /**
   * Get the blocks in the response.
   *
   * @return list of block pairs
   */
  public List<BlockPairSemiCompressed> getBlocks() {
    return blocks;
  }

  @Override
  public void write(SafeDataOutputStream stream) {
    stream.writeLong(senderBlockTime);
    stream.writeByte(blocks.size());
    getSerializer(chainId).writeFixed(stream, blocks);
  }

  private static SafeListStream<BlockPairSemiCompressed> getSerializer(String chainId) {
    return SafeListStream.create(
        stream -> BlockPairSemiCompressed.read(stream, chainId), BlockPairSemiCompressed::write);
  }

  /** A block with the entailed transactions. */
  public static final class BlockPairSemiCompressed implements DataStreamSerializable {

    private final FinalBlock block;
    private final List<SignedTransaction> transactions;
    private final List<SemiCompressedEvent> semiCompressedEvents;
    private final String chainId;

    /**
     * Creates a new block pair.
     *
     * @param block the final block
     * @param transactions the entailed transactions
     * @param semiCompressedEvents the entailed events
     * @param chainId the chain id for serialization of transactions
     */
    public BlockPairSemiCompressed(
        FinalBlock block,
        List<SignedTransaction> transactions,
        List<SemiCompressedEvent> semiCompressedEvents,
        String chainId) {
      this.block = block;
      this.transactions = transactions;
      this.semiCompressedEvents = semiCompressedEvents;
      this.chainId = chainId;
    }

    /**
     * Read a block pair from the supplied stream.
     *
     * @param stream the stream to read from
     * @param chainId the id of the running blockchain
     * @return the read block pair
     */
    static BlockPairSemiCompressed read(SafeDataInputStream stream, String chainId) {
      FinalBlock block = FinalBlock.read(stream);

      int size = block.getBlock().getTransactions().size();
      List<SignedTransaction> transactions = transactionSerializer(chainId).readFixed(stream, size);
      List<SemiCompressedEvent> events = SemiCompressedEvent.LIST_SERIALIZER.readDynamic(stream);
      return new BlockPairSemiCompressed(block, transactions, events, chainId);
    }

    @Override
    public void write(SafeDataOutputStream stream) {
      block.write(stream);
      transactionSerializer(chainId).writeFixed(stream, transactions);
      SemiCompressedEvent.LIST_SERIALIZER.writeDynamic(stream, semiCompressedEvents);
    }

    private static SafeListStream<SignedTransaction> transactionSerializer(String chainId) {
      return SafeListStream.create(
          stream -> SignedTransaction.read(chainId, stream), SignedTransaction::write);
    }

    /**
     * Get the final block.
     *
     * @return a final block
     */
    public FinalBlock getBlock() {
      return block;
    }

    /**
     * Get the entailed transactions.
     *
     * @return list of signed transactions
     */
    public List<SignedTransaction> getTransactions() {
      return transactions;
    }

    /**
     * Get the entailed events.
     *
     * @return list of executable events
     */
    public List<SemiCompressedEvent> getSemiCompressedEvents() {
      return semiCompressedEvents;
    }
  }
}
