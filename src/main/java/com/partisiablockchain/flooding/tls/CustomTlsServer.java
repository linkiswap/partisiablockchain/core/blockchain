package com.partisiablockchain.flooding.tls;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.crypto.TlsUtils;
import java.io.IOException;
import java.util.List;
import java.util.Vector;
import org.bouncycastle.tls.Certificate;
import org.bouncycastle.tls.CertificateRequest;
import org.bouncycastle.tls.CipherSuite;
import org.bouncycastle.tls.ClientCertificateType;
import org.bouncycastle.tls.DefaultTlsServer;
import org.bouncycastle.tls.ProtocolVersion;
import org.bouncycastle.tls.SignatureAndHashAlgorithm;
import org.bouncycastle.tls.TlsCredentialedSigner;
import org.bouncycastle.tls.crypto.impl.bc.BcTlsCrypto;

/** The TLS server used when making a handshake. */
public final class CustomTlsServer extends DefaultTlsServer {

  private final BcTlsCrypto tlsCrypto;
  private final KeyPair signer;
  private final Certificate selfSignedCertificate;

  /**
   * Create a TLS server.
   *
   * @param tlsCrypto the TLS crypto used by the default server.
   * @param signer the key pair used to sign the certificate.
   * @param selfSignedCertificate the self signed certificate.
   */
  public CustomTlsServer(BcTlsCrypto tlsCrypto, KeyPair signer, Certificate selfSignedCertificate) {
    super(tlsCrypto);
    this.tlsCrypto = tlsCrypto;
    this.signer = signer;
    this.selfSignedCertificate = selfSignedCertificate;
  }

  @SuppressWarnings("JdkObsolete")
  @Override
  public CertificateRequest getCertificateRequest() {
    SignatureAndHashAlgorithm signatureAndHashAlgorithm = TlsUtils.getSignatureAndHashAlgorithm();
    return new CertificateRequest(
        new short[] {ClientCertificateType.ecdsa_sign},
        new Vector<>(List.of(signatureAndHashAlgorithm)),
        new Vector<>());
  }

  @Override
  public void notifyClientCertificate(Certificate clientCertificate) throws IOException {
    TlsUtils.validateCertificate(clientCertificate);
  }

  @Override
  protected TlsCredentialedSigner getECDSASignerCredentials() {
    return TlsUtils.getTlsSignerCredentials(signer, tlsCrypto, context, selfSignedCertificate);
  }

  @Override
  public ProtocolVersion[] getProtocolVersions() {
    return new ProtocolVersion[] {ProtocolVersion.TLSv12};
  }

  @Override
  protected int[] getSupportedCipherSuites() {
    return new int[] {CipherSuite.TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256};
  }
}
