package com.partisiablockchain.flooding;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.flooding.tls.CustomTlsClient;
import com.partisiablockchain.flooding.tls.CustomTlsServer;
import com.partisiablockchain.util.DataStreamLimit;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.coverage.ExceptionConverter;
import com.secata.tools.coverage.ExceptionLogger;
import com.secata.tools.thread.ThreadedLoop;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.security.SecureRandom;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.function.BiConsumer;
import org.bouncycastle.tls.Certificate;
import org.bouncycastle.tls.TlsClientProtocol;
import org.bouncycastle.tls.TlsPeer;
import org.bouncycastle.tls.TlsProtocol;
import org.bouncycastle.tls.TlsServerProtocol;
import org.bouncycastle.tls.crypto.impl.bc.BcTlsCrypto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** A connection to another party in the flooding network. */
public final class Connection implements Closeable {

  private static final Logger logger = LoggerFactory.getLogger(Connection.class);

  static final int MAX_OUTBOUND_QUEUE_SIZE = 100_000;

  private final LinkedBlockingQueue<Packet<?>> outbound;
  private final ThreadedLoop senderThread;
  private final ThreadedLoop receiverThread;
  private final Closeable input;
  private final Closeable output;
  private final TlsProtocol tlsProtocol;

  Connection(
      final BiConsumer<Connection, Packet<?>> consumer,
      InputStream input,
      OutputStream output,
      String chainId,
      TlsProtocol tlsProtocol) {
    this(
        consumer,
        input,
        output,
        new SafeDataInputStream(input),
        new SafeDataOutputStream(output),
        chainId,
        tlsProtocol);
  }

  private Connection(
      final BiConsumer<Connection, Packet<?>> consumer,
      Closeable input,
      Closeable output,
      SafeDataInputStream inputStream,
      SafeDataOutputStream outputStream,
      String chainId,
      TlsProtocol tlsProtocol) {
    this.input = input;
    this.output = output;
    this.outbound = new LinkedBlockingQueue<>(MAX_OUTBOUND_QUEUE_SIZE);
    this.tlsProtocol = tlsProtocol;

    receiverThread =
        ThreadedLoop.create(
                () -> {
                  int type = inputStream.readUnsignedByte();
                  Packet.Type<?> packetType = Packet.Type.parseInt(type);
                  Packet<?> packet = packetType.parse(chainId, inputStream);
                  consumer.accept(this, packet);
                },
                "Receiver")
            .onException(this::close)
            .logger(logger::trace, "Connection problem ")
            .start();

    senderThread =
        ThreadedLoop.create(() -> this.outbound.take().send(outputStream), "Sender")
            .onException(this::close)
            .logger(logger::trace, "Connection problem ")
            .start();
  }

  static Connection createServerConnection(
      BiConsumer<Connection, Packet<?>> incoming,
      Socket socket,
      String chainId,
      KeyPair signer,
      Certificate selfSignedCertificate)
      throws IOException {
    InputStream input = ExceptionConverter.call(socket::getInputStream, "Unable to get input");
    OutputStream output = ExceptionConverter.call(socket::getOutputStream, "Unable to get output");

    BcTlsCrypto tlsCrypto = new BcTlsCrypto(new SecureRandom());
    CustomTlsServer tlsServer = new CustomTlsServer(tlsCrypto, signer, selfSignedCertificate);
    TlsServerProtocol tlsServerProtocol = new TlsServerProtocol(input, output);

    return createConnection(
        incoming, chainId, tlsServerProtocol, tlsServer, TlsServerProtocol::accept);
  }

  static Connection createClientConnection(
      BiConsumer<Connection, Packet<?>> incoming,
      Socket socket,
      String chainId,
      KeyPair signer,
      Certificate selfSignedCertificate)
      throws IOException {
    InputStream input = ExceptionConverter.call(socket::getInputStream, "Unable to get input");
    OutputStream output = ExceptionConverter.call(socket::getOutputStream, "Unable to get output");

    BcTlsCrypto tlsCrypto = new BcTlsCrypto(new SecureRandom());
    CustomTlsClient tlsClient = new CustomTlsClient(tlsCrypto, signer, selfSignedCertificate);
    TlsClientProtocol tlsClientProtocol = new TlsClientProtocol(input, output);

    return createConnection(
        incoming, chainId, tlsClientProtocol, tlsClient, TlsClientProtocol::connect);
  }

  static <P extends TlsProtocol, T extends TlsPeer> Connection createConnection(
      BiConsumer<Connection, Packet<?>> incoming,
      String chainId,
      P tlsProtocol,
      T tlsPeer,
      Handshake<P, T> handshake)
      throws IOException {
    Connection conn = null;
    try {
      handshake.handshake(tlsProtocol, tlsPeer);
      new SafeDataOutputStream(tlsProtocol.getOutputStream()).writeString(chainId);
      String receivedChainId =
          DataStreamLimit.readString(new SafeDataInputStream(tlsProtocol.getInputStream()));
      if (!chainId.equals(receivedChainId)) {
        throw new RuntimeException(
            "Received invalid chainId. expected='"
                + chainId
                + "', received='"
                + receivedChainId
                + "'");
      } else {
        conn =
            new Connection(
                incoming,
                tlsProtocol.getInputStream(),
                tlsProtocol.getOutputStream(),
                chainId,
                tlsProtocol);
      }
    } finally {
      if (conn == null) {
        ExceptionLogger.handle(
            logger::debug, tlsProtocol::close, "Error while closing TLS protocol");
      }
    }
    return conn;
  }

  interface Handshake<P extends TlsProtocol, T extends TlsPeer> {

    void handshake(P protocol, T peer) throws IOException;
  }

  /**
   * Send packet on network.
   *
   * @param packet to be sent
   */
  public void send(Packet<?> packet) {
    if (!this.outbound.offer(packet) && isAlive()) {
      logger.info("Closing connection since the queue is full");
      close();
    }
  }

  boolean isAlive() {
    return senderThread.isRunning();
  }

  @Override
  public void close() {
    senderThread.stop();
    receiverThread.stop();

    ExceptionLogger.handle(logger::debug, input::close, "Error while closing socket");
    ExceptionLogger.handle(logger::debug, output::close, "Error while closing socket");
    ExceptionLogger.handle(logger::debug, tlsProtocol::close, "Error while closing TLS protocol");
  }
}
