package com.partisiablockchain.flooding;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.secata.tools.coverage.ExceptionConverter;
import com.secata.tools.coverage.ExceptionLogger;
import com.secata.tools.thread.ExecutorFactory;
import java.io.Closeable;
import java.net.Socket;
import java.util.List;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.function.BiConsumer;
import java.util.function.Supplier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

final class Client implements Closeable {

  private static final Logger logger = LoggerFactory.getLogger(Client.class);
  static final int MAX_NEIGHBOURS = 5;

  private final ScheduledExecutorService scheduler;
  private final Supplier<Address> kademlia;
  private final Address persistentConnection;
  private final Supplier<List<Address>> connectedAccounts;
  private final BiConsumer<Address, Socket> connectedCallback;
  private boolean closed;

  /** Creates a client for a network node. */
  Client(
      Supplier<Address> kademlia,
      Address persistentConnection,
      Supplier<List<Address>> connectedAccounts,
      BiConsumer<Address, Socket> connectedCallback) {
    this(
        ExecutorFactory.newScheduled("Connector", 1),
        kademlia,
        persistentConnection,
        connectedAccounts,
        connectedCallback);
  }

  /** Creates a client for a network node. */
  Client(
      ScheduledExecutorService scheduler,
      Supplier<Address> kademlia,
      Address persistentConnection,
      Supplier<List<Address>> connectedAccounts,
      BiConsumer<Address, Socket> connectedCallback) {
    this.scheduler = scheduler;
    this.kademlia = kademlia;
    this.persistentConnection = persistentConnection;
    this.connectedAccounts = connectedAccounts;
    this.connectedCallback = connectedCallback;
    initialize();
  }

  @SuppressWarnings("FutureReturnValueIgnored")
  private void initialize() {
    scheduler.scheduleWithFixedDelay(
        () ->
            ExceptionLogger.handle(
                logger::debug, this::checkForConnect, "Error while checking connections"),
        0,
        randomInterval(1000, 1500),
        TimeUnit.MILLISECONDS);
  }

  void checkForConnect() {
    List<Address> connectedAccounts = this.connectedAccounts.get();

    connectIfUnconnected(connectedAccounts, persistentConnection);

    int neededCount = MAX_NEIGHBOURS - connectedAccounts.size();
    for (int i = 0; i < neededCount; i++) {
      initializeConnection(connectedAccounts);
    }
  }

  private void initializeConnection(List<Address> connectedAccounts) {
    int picks = 0;
    boolean done = false;
    while (!done && picks < 20) {
      Address address = kademlia.get();
      done = connectIfUnconnected(connectedAccounts, address);
      picks++;
    }
  }

  boolean connectIfUnconnected(List<Address> connectedAddresses, Address toConnect) {
    if (toConnect == null || connectedAddresses.contains(toConnect)) {
      return false;
    }
    connectedAddresses.add(toConnect);
    connect(toConnect);
    return true;
  }

  @SuppressWarnings("FutureReturnValueIgnored")
  private void connect(Address toConnect) {
    scheduler.schedule(
        () -> allocateSocket(toConnect), randomInterval(0, 100), TimeUnit.MILLISECONDS);
  }

  @SuppressWarnings("AddressSelection")
  private void allocateSocket(Address toConnect) {
    Socket socket =
        ExceptionConverter.call(
            // Intentional suppressed, TLS connection is established by Connection class.
            // It just doesn't use Java SSL sockets.
            // nosemgrep
            () -> new Socket(toConnect.getHost(), toConnect.getPort()), "Unable to connect");
    connectedCallback.accept(toConnect, socket);
  }

  @Override
  public void close() {
    closed = true;
    scheduler.shutdownNow();
  }

  boolean isClosed() {
    return closed;
  }

  static long randomInterval(long min, long max) {
    return (long) (min + Math.random() * (max - min));
  }
}
