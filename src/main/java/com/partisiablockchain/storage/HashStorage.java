package com.partisiablockchain.storage;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.Hash;
import com.secata.stream.DataStreamSerializable;
import java.util.stream.Stream;

/**
 * Stores data of type T based on a hash.
 *
 * @param <T> the type to store.
 */
public interface HashStorage<T extends DataStreamSerializable> {

  /**
   * Read all items stored in this storage.
   *
   * @return a stream of all items in this storage
   */
  Stream<T> readEvery();

  /**
   * Read a item from this storage.
   *
   * @param hash the id of the item to read
   * @return the read item or null if not present
   */
  T read(Hash hash);

  /**
   * Writes to a file with the given hash.
   *
   * @param hash the hash to store in the file system
   * @param writer the writer
   */
  void write(Hash hash, T writer);

  /**
   * Is this storage empty.
   *
   * @return true if no items are stored in this storage
   */
  boolean isEmpty();

  /**
   * Does the storage contain the specified item.
   *
   * @param hash the id of the item
   * @return true if the item exists in the storage
   */
  boolean has(Hash hash);

  /** Clear the storage. Should only be called when no read and write are in process. */
  void clear();

  /**
   * Remove a hash from the storage. Only allowed for mutable stores.
   *
   * @param hash the hash to remove
   */
  void remove(Hash hash);
}
