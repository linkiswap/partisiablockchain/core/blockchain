package com.partisiablockchain.storage;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.secata.tools.coverage.ExceptionConverter;
import com.secata.tools.coverage.ExceptionLogger;
import java.io.File;
import java.io.RandomAccessFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** Index from block time to blocks. */
public final class BlockTimeStorage implements AutoCloseable {

  private static final Logger logger = LoggerFactory.getLogger(BlockTimeStorage.class);

  private final RandomAccessFile target;
  private long latestBlockTime;

  /**
   * Create a new block time storage.
   *
   * @param file the file to save current value to
   * @param minimumValue a minimal value to return if the value stored in file is less than
   */
  public BlockTimeStorage(File file, long minimumValue) {
    target =
        ExceptionConverter.call(
            () -> new RandomAccessFile(file, "rw"), "Unable to open block time file");
    // Make sure we only produce blocks that are newer than what we know
    latestBlockTime =
        ExceptionConverter.call(
            () -> {
              long value = -1;
              if (target.length() == 8) {
                value = target.readLong();
                // Make sure we only produce blocks that are newer than what we know
              }
              return value;
            },
            "Unable to read current state");
    storeLatestBlockTime(minimumValue);
  }

  /**
   * Get latest blocktime.
   *
   * @return latest blocktime
   */
  public long getLatestBlockTime() {
    return latestBlockTime;
  }

  /**
   * Store the supplied block time in this storage.
   *
   * @param latestBlockTime the update block time to store
   */
  @SuppressWarnings("WeakerAccess")
  public void storeLatestBlockTime(long latestBlockTime) {
    if (latestBlockTime > this.latestBlockTime) {
      this.latestBlockTime = latestBlockTime;
      ExceptionConverter.run(
          () -> {
            target.seek(0);
            target.writeLong(latestBlockTime);
          },
          "Unable to store latest produced block");
    }
  }

  @Override
  public void close() {
    ExceptionLogger.handle(logger::warn, target::close, "Unable to close storage");
  }
}
