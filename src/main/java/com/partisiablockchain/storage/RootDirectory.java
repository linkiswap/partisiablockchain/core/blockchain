package com.partisiablockchain.storage;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.secata.stream.DataStreamSerializable;
import com.secata.stream.SafeDataInputStream;
import com.secata.tools.coverage.ExceptionConverter;
import java.io.File;
import java.util.function.Function;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Is a root directory for storing files and directories below the current. Is an augmented version
 * of a file that presents pbc friendly methods.
 */
public final class RootDirectory {

  private static final Logger logger = LoggerFactory.getLogger(RootDirectory.class);

  private final File root;
  private final StorageCreator storageCreator;

  /**
   * Creates a new root directory.
   *
   * @param root the root of the files created below this root directory
   * @param storageCreator responsible for creating storage (allows for advanced db)
   */
  public RootDirectory(File root, StorageCreator storageCreator) {
    this.root = root;
    this.storageCreator = storageCreator;
    ensureExistence(root);
  }

  File createSubFile(String fileName) {
    // The following lines ensures this file is a sub directory.
    // nosemgrep
    File subFile = new File(root, fileName);
    if (ExceptionConverter.call(
        () -> subFile.getCanonicalPath().startsWith(root.getCanonicalPath()))) {
      return subFile;
    } else {
      throw new IllegalArgumentException("Filename is not legal: " + fileName);
    }
  }

  /**
   * Creates a sub directory to this directory.
   *
   * @param directory the name of the sub directory
   * @return the new directory
   */
  @SuppressWarnings("WeakerAccess")
  public RootDirectory createSub(String directory) {
    File file = createSubFile(directory);
    return new RootDirectory(file, storageCreator);
  }

  /**
   * Create a file in this directory.
   *
   * @param fileName the name of the file
   * @return the new file
   */
  public File createFile(String fileName) {
    return createSubFile(fileName);
  }

  /**
   * Create a storage of key,value pair based on hash as key and safe streams fore storage.
   *
   * @param directory the name of the sub directory to store the underlying key value store
   * @param reader function to read the value type
   * @param <T> the type of items stored
   * @return the new HashStorage
   */
  public <T extends DataStreamSerializable> HashStorage<T> createHashStorage(
      String directory, Function<SafeDataInputStream, T> reader) {
    return createHashStorage(directory, reader, true);
  }

  /**
   * Create a storage of key,value pair based on hash as key and safe streams fore storage.
   *
   * @param directory the name of the sub directory to store the underlying key value store
   * @param reader function to read the value type
   * @param immutable true if the values stored are immutable
   * @param <T> the type of items stored
   * @return the new HashStorage
   */
  public <T extends DataStreamSerializable> HashStorage<T> createHashStorage(
      String directory, Function<SafeDataInputStream, T> reader, boolean immutable) {
    return new HashStorageImpl<>(storageCreator.create(createSub(directory), immutable), reader);
  }

  /**
   * Create a storage of key,value pair based on hash as key and safe streams fore storage.
   *
   * @param directory the name of the sub directory to store the underlying key value store
   * @return the new HashStorage
   */
  @SuppressWarnings("WeakerAccess")
  public StateStorageCached createStateStorage(String directory) {
    return new StateStorageCached(storageCreator.create(createSub(directory), true));
  }

  /**
   * Create a storage for contract variables.
   *
   * @param directory name of sub-directory to store into
   * @return created storage
   */
  @SuppressWarnings("WeakerAccess")
  public ContractVariableStorage createVariableStorage(String directory) {
    return new ContractVariableStorageImpl(storageCreator.create(createSub(directory), false));
  }

  String getPath() {
    return root.getAbsolutePath();
  }

  /**
   * Checks to see if the underlying directory is empty.
   *
   * @return true if empty
   * @throws IllegalStateException if the path is illegal
   */
  public boolean isEmpty() {
    File[] files = root.listFiles();
    if (files != null) {
      return files.length == 0;
    } else {
      throw new IllegalStateException("Cannot work in an illegal path: " + root);
    }
  }

  private void ensureExistence(File file) {
    boolean mkdirs = file.mkdirs();
    logger.debug("Root directory={}, creation status={}", file, mkdirs);
  }

  /**
   * Injection interface for storage creation. Allows different underlying implementations of the
   * storage.
   */
  @FunctionalInterface
  public interface StorageCreator {

    /**
     * Create storage.
     *
     * @param rootDirectory root directory to hold storage
     * @param immutable will shortcut writing existing entries.
     * @return created storage
     */
    Storage create(RootDirectory rootDirectory, boolean immutable);
  }
}
